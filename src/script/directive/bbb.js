/**
 * Created by Administrator on 2017/3/25.
 */
angular.module('HTRD_AutoConsole.A').directive('bbb',[function() {
	var $scope, $node;

	var link = function(s, e) {
    var page = 1;
    var pageList = [];

    var activePage = function(item) {
      console.log(item);
      // $emit('');
    };

		var parseDOM = function() {
			$scope = s;
			$node = $(e[0]);
		};

		var bindListener = function() {
      $scope.activePage = activePage;
		};

		var initPlugins = function() {
      console.log('initPlugins');
      $scope.$emit('toparents',{name:'child-parents'});
      $scope.$on('tolist',function(evt,data){
        $scope.page = Math.ceil(data.total / data.pageSize);

        for (var i = 0;i < $scope.page;i++) {

          var model = {
            index: (i + 1)
          };

          if (data.pageNo === (i + 1)) {
            model.active = true;
          };

          pageList.push(model);
        };

        $scope.pageList = pageList;

        console.log($scope.pageList);
      })
		};

		parseDOM();
		bindListener();
		initPlugins();
	};

	return {
		'replace': true,
		'restrict' : 'AEC',
		'scope': {},
		'link': link,
		'templateUrl': 'view/directive/util/bbb.html'
	};
}]);
