// 左侧菜单
angular.module('HTRD_AutoConsole.A').directive('menu',[function() {
  var link = function(s, e) {
      var $scope, $node;

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {

      };

      var initPlugins = function() {

      };

      parseDOM();
      bindListener();
      initPlugins();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'list': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/menu.html'
  };
}]);
