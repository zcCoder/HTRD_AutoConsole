// 左侧菜单
angular.module('HTRD_AutoConsole.A').directive('selectMenu',[function() {
  var link = function(s, e) {
      var $scope, $node;

      var activeMenu = function(item) {
        console.log(11111)
        $scope.defaultText = item.title;
        $scope.menuCallback && $scope.menuCallback(item);
      };

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
        $scope.activeMenu = activeMenu;
      };

      var initPlugins = function() {
        // $node.find(".dropdown-menu").on('click', function (e) {
        //    e.stopPropagation();
        //  });
      };

      parseDOM();
      bindListener();
      initPlugins();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'list': '=',
        'defaultText': '=',
        'menuCallback': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/selectMenu.html'
  };
}]);
