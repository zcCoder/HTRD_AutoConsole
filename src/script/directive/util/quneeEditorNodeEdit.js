// 左侧菜单 -
angular.module('HTRD_AutoConsole.A').directive('quneeEditorNodeEdit',['processEditServ','ManagerListServ',
function(processEditServ,ManagerListServ) {
  var link = function(s, e) {
      var $scope, $node;
      var nodeId ;

      //选择流程
      var activeProcess = function (item) {
        $scope.pipelineName = item.name;
  				$scope.quoteProcess = item;
          console.log($scope.quoteProcess)
  				// $scope.$emit("processProperty",{
  				// 	processSelect: item
  				// })
  		};

  		var getProcessListByName = function (item) {
  			ManagerListServ.getProcessList({
  							search: item
  			})
  				.then(function (ret) {

  						if (ret.status === 200) {
  								$scope.$broadcast('changeSelectFilterList', {
                    data: ret.results.data,
                    id: 'processSelectId'
                  });
  						} else {

  						}

  				});
  		};

      var machineCallback = function(item) {
        console.log('machineCallback', item.id);
      };

      var scriptCallback = function(item) {
        console.log('scriptCallback', item.id);
      };

      var resultCallback = function(item) {
        console.log('resultCallback', item);
      };

      var operationCallback = function(item) {
        console.log('operationCallback', item);
      };

      var saveNode = function(){
        $scope.nodeBaseInfo.id = nodeId;
        $scope.$emit('nodeShow',{
          nodeInfoState : false
        });
        //发请求$scope.nodeBaseInfo
        console.log('node的内容',$scope.nodeBaseInfo);
      }
      //机器
      var machineList = function(params){
        console.log(params);
        processEditServ.getMachineList(params)
        .then(function(result){
          $scope.machineData = result.results.map($scope.setTreeData);
        })
      };
      //脚本
      var scriptList = function(params){
        processEditServ.getScriptList(params)
        .then(function(result){
          $scope.scriptData = result.results.map($scope.setTreeData);
        })
      };
      //操作
      var operationList = function(params){
        processEditServ.getoperationList(params)
        .then(function(result){
          $scope.scriptData = result.results.map($scope.setTreeData);
        })
      };

      //用户
      var userList = function(params){
        processEditServ.getUserList(params)
        .then(function(result){
          $scope.userData = result.results.map($scope.setTreeData);
        })
      };

      //节点属性
      var nodeInfo = function(params){
        // processEditServ.getNodeInfo(params)
        // .then(function(result){
        //   console.log('nodeInfo');
        // })
        console.log('发送请求获取nodeInfo');
        $scope.machineSelected = '10.10.202.106';
        $scope.scriptSelected = '打开计算器';
        $scope.pipelineName = 'sssss';
        $scope.operationList = '';
      };

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
        $scope.resultCallback = resultCallback;
        $scope.saveNode = saveNode;
        //下拉菜单返回的id
        $scope.machineCallback = machineCallback;
        $scope.scriptCallback = scriptCallback;
        $scope.getProcessListByName = getProcessListByName;
        $scope.activeProcess = activeProcess;
        $scope.operationList = operationList;
      };

      var initPlugins = function() {
        $scope.nodeBaseInfo = {};
        $scope.userText = '请选择用户';
        $scope.processSelectId = 'processSelectId'
        $scope.$on('node_info',function(e,data){

          if(data.nodeData && data.nodeData.infoData){
            $scope.nodeInfo = data.nodeData.infoData.ref_type;
            $scope.nodeBaseInfo.type = data.nodeData.name;
            nodeId = data.nodeData.id;
            nodeInfo(nodeId);
          }else{
            $scope.nodeInfo = data.nodeData;
            $scope.nodeBaseInfo = {};
          };

          if(typeof($scope.nodeInfo) == 'string'){
            var nodeArr = ["节点","基本节点", "用户确认节点", "引用流程", "开始节点", "结束节点", "关机节点",  "重启节点"];
            $scope.nodeInfo = nodeArr.indexOf($scope.nodeInfo);
          };

          switch ($scope.nodeInfo) {
            case 1:
              $scope.nodeType = "任务脚本";
              $scope.machineShow = true;
              $scope.scriptShow = true;
              $scope.operationShow = true;
              $scope.nameShow = $scope.timeShow = $scope.processShow = false;
              //machineList(nodeId);
              //scriptList(nodeId);
            break;

            case 2:
              $scope.nodeType = "用户确认";
              $scope.nameShow = true;
              $scope.timeShow = true;
              $scope.operationShow = $scope.machineShow = $scope.scriptShow = $scope.processShow = false;
              //userList(nodeId);
              break;

            case 3:
              $scope.nodeType = "引用流程";
              $scope.processShow = true;
              $scope.timeShow = true;
              $scope.operationShow = $scope.nameShow = $scope.machineShow = $scope.scriptShow = false;
              //processList(nodeId);
              break;

            case 4:
              $scope.nodeType = "开始";
              $scope.operationShow = $scope.machineShow = $scope.processShow = $scope.timeShow = $scope.nameShow = $scope.scriptShow = false;
              break;

            case 5:
              $scope.nodeType = "结束";
              $scope.operationShow = $scope.machineShow = $scope.processShow = $scope.timeShow = $scope.nameShow = $scope.scriptShow = false;
              break;

              case 6:
                $scope.nodeType = "关机";
                $scope.machineShow = true;
                $scope.operationShow = $scope.processShow = $scope.timeShow = $scope.nameShow = $scope.scriptShow = false;
                //machineList(nodeId);
                break;

              case 7:
                $scope.nodeType = "重启";
                $scope.machineShow = true;
                $scope.operationShow = $scope.processShow = $scope.timeShow = $scope.nameShow = $scope.scriptShow = false;
                //machineList(nodeId);
                break;
            default:
          };
        });
        $scope.$on('nodeInfoList',function(e,data){
          switch (data.tag) {
            case 'process':
              $scope.nodeBaseInfo.process = data.quoteProcessSelect;
              break;
            default:
          }
          console.log('nodeInfoList',$scope.nodeBaseInfo)
        });
        //userlist的数据
        $scope.list = [
          {
            id: 01,
            title: 'chuxiaolin'
          },
          {
            id: 02,
            title: 'duoduohua'
          },
          {
            id: 03,
            title: 'wanglinin'
          },
          {
            id: 04,
            title: 'zhangllin'
          },
          {
            id: 05,
            title: 'langpinpin'
          }
        ];

        $scope.machineOptions = [
          {
            'id': 1,
            'title': '10.10.202.101'
          },
          {
            'id': 2,
            'title': '10.10.202.102'
          },
          {
            'id': 3,
            'title': '10.10.202.103'
          },
          {
            'id': 4,
            'title': '10.10.202.104'
          },
          {
            'id': 5,
            'title': '10.10.202.105'
          },
          {
            'id': 6,
            'title': '10.10.202.106'
          },
          {
            'id': 7,
            'title': '10.10.202.107'
          }
        ];

        $scope.scriptOptions = [
          {
            'id': 1,
            'title': '打开计算器'
          },
          {
            'id': 2,
            'title': '打开记事本'
          },
          {
            'id': 3,
            'title': '复制文件'
          },
          {
            'id': 4,
            'title': '关闭计算器'
          },
          {
            'id': 5,
            'title': '关闭记事本'
          },
          {
            'id': 6,
            'title': '下载文件'
          },
          {
            'id': 7,
            'title': '启win进程'
          },
          {
            'id': 8,
            'title': '切换杀进程'
          },
          {
            'id': 9,
            'title': '应急切换杀全进程'
          },
          {
            'id': 10,
            'title': '系统日志检查'
          }
        ];

        $scope.operationOptions = [
          {
            'id': 1,
            'title': '启动脚本'
          },
          {
            'id': 2,
            'title': '关闭脚本'
          },
          {
            'id': 3,
            'title': '重启脚本'
          }
        ];

        //user-menu返回的id
        $scope.$on('userList',function(e,data){
          console.log(data,nodeId);
          $scope.nodeBaseInfo.userIds = data.userIds;
        })

        $scope.$on('timeOut',function(e,data){
          console.log('time_out',data)
        })
      };

      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'nodeInfo': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/quneeEditorNodeEdit.html'
  };
}]);
