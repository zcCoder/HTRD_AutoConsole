angular.module('HTRD_AutoConsole.A').directive('breadcrumbs', [function() {
  var $scope, $node;
  var link = function(s, e) {

    var parseDom = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {
      
    };

    var initPlugins = function() {
      console.log($scope.data);
    };

    parseDom();
    bindListener();
    initPlugins();
  };

  return {
    'restrict': 'AE',
    'replace': true,
    'templateUrl': 'view/directive/util/breadcrumbs.html',
    'scope': {
      'data': '='
    },
    'link': link,
  };
}]);