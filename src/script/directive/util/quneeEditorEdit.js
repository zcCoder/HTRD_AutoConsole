// 拓扑图编辑
angular.module('HTRD_AutoConsole.A').directive('quneeEditerEdit',['$window', '$timeout', '$interval', '$stateParams', '$rootScope','ManagerProcessTreeServ','ManagerProcessQuneeServ','DialogServ', 'SocketServ', 'AlertServ', 'ParseMapServ', 'DateParseServ',
function($window, $timeout, $interval, $stateParams, $rootScope, ManagerProcessTreeServ,ManagerProcessQuneeServ,DialogServ, SocketServ, AlertServ, ParseMapServ, DateParseServ) {
  var link = function(s, e) {
      var $scope, $node;

      var nameInput = function(){
        $scope.nameInputShow = false;
      }

      var classifyInput = function(){
        $scope.classifyInputShow = false;
      }

      // 保存
      var processSave = function () {
        // processEditServ.saveProcessInfo($scope.processInfo)
        // .then(funcion(result){
        //   if(result.status === 200){
        //     AlertServ.showSuccess('保存成功');
        //   }
        // })
        console.log($scope.processInfo);
        AlertServ.showSuccess('保存成功');
      };

      //放弃
      var processAbandon = function(){
         strapModal = DialogServ.modal({
          'scope': $scope,
           'templateUrl': 'view/console/modal/abandonProcess.html'
         })
         strapModal.$promise.then(strapModal.show);
       }

      var giveUpBtn = function(){
       strapModal.hide();
       AlertServ.showSuccess('放弃成功');
     };

      var showProcessEdit = function() {
        $scope.processInfoState = !!!$scope.processInfoState;
        if($scope.processInfoState){
          $scope.nodeInfoState = false;
        }
        // 流程属性编辑
        $scope.$broadcast('qunee.process.edit', {
          processInfoState: $scope.processInfoState
        });
      };

      // 流程id
      var processId = 0;

      // 执行id
      var execId;

      // 如果状态为数字 则转换
      var quneeStateMap = {
        0: 'init', // 默认(1级分类)
        1: 'check', // 检测中(1级分类)
        2: 'exec', // 执行中(1级分类)
        3: 'checkError', // 检测异常(2级分类)
        4: 'checkSuccess', // 检测成功(2级分类)
        5: 'execSuccess' // 执行成功
      };

      // 执行中状态
      var runningMap = {
        0: 'pause',
        1: 'running'
      };

      // 执行中节点状态
      var runningNodeMap = {
        0: 'error',
        1: 'overtime',
        2: 'runningSuccess'
      };

      var quneeRefTypeMap = {
        1: 'basicNode',
        2: 'userConfirmNode',
        3: 'groupNode',
        4: 'startNode',
        5: 'endNode',
        6: 'closeNode',
        7: 'restartNode'
      };

      var resetNodes = function() {
        var refId;
        $scope.nodeList = quneeParseNodeJSON($scope.pipeline);

        // 先获取全部node
        selfJson = JSON.parse($scope.getChartJSON());
        selfJson.datas.map(function(selfItem, index) {

          // 更新普通节点
          if (selfItem._className === 'Q.Node') {

            if (!!!selfItem.json.properties.data.id) {
              refId = selfItem.json.properties.data.$ref;
              selfItem.json.properties.data = selfJson.refs[refId];
            };

            selfItem.json.properties.data.infoData.status = 'init';
            getNodeData(selfItem.json.properties.data, selfItem.json);
          };
        });

        updateJSON(selfJson);
      };

      var dupRemove = function(arr, isNotReverse) {
        var result = [];
        var ids = [];

        if (!!!isNotReverse) {
          // 相同id的节点最终显示后面的状态
          arr.reverse();
        };

        arr.map(function(item) {

          // 不存在才加入结果集
          if (ids.indexOf(item.id) === -1) {
            result.push(item);
            ids.push(item.id);
          };
        });

        return result;
      };

      // 转换json字符串 , 组装完整的json数据
      var quneeParseNodeJSON = function(nodeList, nodeStatusList) {
        var nodeJSON;

        if (typeof nodeList === 'string') {
          nodeJSON = JSON.parse(nodeList);
        } else {
          nodeJSON = nodeList;
        };

        // 判断节点状态列表内是否有数据 没有则代表初始化状态
        if (!!nodeStatusList) {

          for (var key in nodeJSON) {

            if (key === 'groups') {
              nodeStatusList.map(function(item) {
                nodeJSON[key].map(function(group) {

                  // 循环组内的节点状态
                  group.nodes.map(function(node) {
                    node.infoData.status = 'init';
                    node.infoData.type = quneeRefTypeMap[node.infoData.ref_type];
                  });
                });
              });
            } else {
              nodeStatusList.map(function(item) {
                nodeJSON[key].map(function(node) {
                  node.infoData.status = 'init';
                  node.infoData.type = quneeRefTypeMap[node.infoData.ref_type];
                });
              });
            }
          };
        } else {

          for (var key in nodeJSON) {
            nodeJSON[key].map(function(node) {
              node.infoData.status = 'init';
              node.infoData.type = quneeRefTypeMap[node.infoData.ref_type];
            });
          };
        };
        return nodeJSON;
      };

      var getQuneeProcessInfo = function() {
        ManagerProcessQuneeServ.getManagerProcessQuneeInfo({
          id: processId
        })
        .then(function(ret) {
          if (ret.status === 200) {
            console.log('流程info',ret);
            $scope.processInfo.name = ret.results.name;
            $scope.defaultText = ret.results.group_name;
            $scope.processInfo.group_name = $scope.defaultText;
          }
        });
      };

      var getUserList = function() {
        $scope.userList = [
          {
            id: 1,
            title: '张健'
          }
        ];

        // ManagerProcessQuneeServ.getUserList()
        // .then(function(ret) {
        //
        //   if (ret.status === 200) {
        //     $scope.userList = [];
        //     ret.results.data.map(function(item, index) {
        //       $scope.userList.push({
        //         id: item.id,
        //         title: item.username
        //       });
        //     });
        //   }
        // });
      };

      // var activeUser = function(item) {
      //   console.log('activeUser',item);
      //   $scope.userConfirmModel.user_id = item.id;
      // };

      // 获取当前流程节点json数据以及状态
      var getQuneeStateAndData = function() {
        ManagerProcessQuneeServ.getManagerProcessQuneeStateAndData({
          id: processId
        })
        .then(function(ret) {
          var quneeState;

          if (ret.status === 200) {

            console.log('ret'+ret+JSON.parse(ret.results.pipeline))

            // 模拟点击执行后 数据返回的状态为 2
            // var pl_ex_id = ParseMapServ.getItem(processId, 'process');
            var pl_ex_id = execId;

            // 获取1级状态 0 基本 1 检测中 2 执行中
            quneeState = ret.results.status;
            // ManagerProcessQuneeServ.getNodeJsonTest()
            // .then(function(result) {
              // 模拟读nodeJson
              // ret.results.pipeline = result;
            switch (quneeState) {

              // 如果是默认状态 则判断chekced内的状态
              case 0:
                // var pl_ex_id = ParseMapServ.getItem(processId, 'process') || ret.results.exec_pipeline_id;
                var pl_ex_id = ret.results.exec_pipeline_id;

                // 判断是否检测过
                if (!!ret.results.checked) {
                  $scope.init = true;

                  $scope.pipeline = ret.results.pipeline;

                  // 保存在全局的节点列表
                  $scope.nodeList = quneeParseNodeJSON(ret.results.pipeline, ret.results.checked.check_nodes);
                  $scope.initEditor();
                  $scope.quneeState = 'init';

                  if (pl_ex_id) {
                    // $scope.quitState = ParseMapServ.getItem(processId, 'quitState');
                    // $scope.quneeState = 'init';
                    $scope.quitState = 'manualQuit';
                    $scope.quneeState = 'init'
                  };

                } else {
                  $scope.pipeline = ret.results.pipeline;

                  // 保存在全局的节点列表中
                  $scope.nodeList = quneeParseNodeJSON(ret.results.pipeline);

                  $scope.initEditor();
                  $scope.quneeState = 'init';
                };
                break;

              // 如果是检测中 则状态为检测中
              case 1:
                var groupNodes = [];
                var selfJson;

                if (typeof ret.results.status === 'number') {
                  $scope.quneeState = quneeStateMap[ret.results.status];
                } else {
                  $scope.quneeState = ret.results.status;
                };

                $scope.pipeline = ret.results.pipeline;
                $scope.nodeList = quneeParseNodeJSON(ret.results.pipeline, ret.results.checked.check_nodes);
                $scope.initEditor(function() {

                  // 先获取全部node
                  selfJson = JSON.parse($scope.getChartJSON());

                  // 先渲染socket推送过来的节点
                  selfJson && selfJson.datas.map(function(selfItem, index) {

                    if (selfItem._className === 'Q.Node' && !!selfItem.json.properties.data.$ref) {
                      groupNodes.push({
                        index: index,
                        item: selfItem,
                        ref: selfItem.json.properties.data.$ref
                      });
                    };
                  });
                  var resukt_nodeList = dupRemove($scope.changeNodeList);
                  resukt_nodeList.map(function(node, index) {

                    // 更新节点数据 只会改变单个节点
                    selfJson.datas.map(function(selfItem, index) {

                      // 更新普通节点
                      if (selfItem._className === 'Q.Node') {
                        var refId;

                        // 判断id是否存在 不存在则为组
                        if (!!!selfItem.json.properties.data.id) {
                          refId = selfItem.json.properties.data.$ref;
                          selfItem.json.properties.data = selfJson.refs[refId];
                        };

                        if (selfItem.json.properties.data.id === node.id) {
                          selfItem.json.properties.data.infoData.status = node.status;
                          updateNode = selfItem.json.properties.data;
                          getNodeData(selfItem.json.properties.data, selfItem.json);
                        };
                      };
                    });
                  });
                  updateJSON(selfJson);
                  $scope.quneeState = 'check';
                });
                break;

              // 如果是执行中 调用其他接口获取执行的具体状态
              case 2:
                var groupNodes = [];
                var selfJson;
                execId = ret.results.exec_pipeline_id;
                $scope.pipeline = ret.results.pipeline;
                $scope.nodeList = quneeParseNodeJSON(ret.results.pipeline, ret.results.checked.check_nodes);

                // 初始化节点流程图
                $scope.initEditor(function() {

                  // 先获取全部node
                  selfJson = JSON.parse($scope.getChartJSON());

                  // 先渲染socket推送过来的节点
                  selfJson && selfJson.datas.map(function(selfItem, index) {

                    if (selfItem._className === 'Q.Node' && !!selfItem.json.properties.data.$ref) {
                      groupNodes.push({
                        index: index,
                        item: selfItem,
                        ref: selfItem.json.properties.data.$ref
                      });
                    };
                  });
                  var resukt_nodeList = dupRemove($scope.changeNodeList);
                  resukt_nodeList.map(function(node, index) {
                    selfJson.datas.map(function(selfItem, index) {

                      // 更新普通节点
                      if (selfItem._className === 'Q.Node') {

                        var refId;

                        // 判断id是否存在 不存在则为组
                        if (!!!selfItem.json.properties.data.id) {
                          refId = selfItem.json.properties.data.$ref;
                          selfItem.json.properties.data = selfJson.refs[refId];
                        };

                        if (selfItem.json.properties.data.id === node.id) {
                          selfItem.json.properties.data.infoData.status = node.status;
                          updateNode = selfItem.json.properties.data;
                          getNodeData(selfItem.json.properties.data, selfItem.json);
                        };
                      };
                    });
                  });
                  updateJSON(selfJson);
                  //$scope.getQuneeExecState(ret.results.exec_pipeline_id);
                });
                break;
              default:
            };
            // });
          } else {

          };
        });
      };

      // 获取执行的具体状态
      // var getQuneeExecState = function(execId) {
      //   ManagerProcessQuneeServ.getManagerProcessQuneeExecState({
      //     id: execId
      //   })
      //   .then(function(ret) {
      //
      //     if (ret.status === 200) {
      //
      //       if (ret.results.result_status === 13) {
      //         $scope.lastState = 'pause';
      //         $scope.quneeState = 'pause';
      //       }
      //       else if (ret.results.result_status === 6){
      //
      //         if ($scope.lastState !== 'check') {
      //           $scope.quneeState = 'pause';
      //         };
      //
      //         $scope.lastState = 'pause';
      //       }
      //       else if (ret.results.result_status === 7){
      //
      //         // 执行中 但节点错误
      //         $scope.quneeState = 'pause';
      //         $scope.lastState = 'parseError';
      //       };
      //
      //       // 模拟保存上次执行后的实际状态 执行中 / 暂停
      //       var lastState = ParseMapServ.getItem(processId, 'lastState');
      //       var result_exec_nodes = [];
      //       $scope.quitState = ParseMapServ.getItem(processId, 'quitState');
      //
      //       if (lastState) {
      //         ret.results.status = lastState;
      //         $scope.quneeState = ret.results.status;
      //       } else {
      //
      //         if (typeof ret.results.status === 'number') {
      //           $scope.quneeState = runningMap[ret.results.status] || 'running';
      //         } else {
      //           $scope.quneeState = ret.results.status;
      //         };
      //       };
      //
      //       ParseMapServ.setItem(processId, ret.results.pl_ex_id, 'process');
      //
      //       // 去重获取最终结果
      //       var resukt_nodeList = dupRemove($scope.changeNodeList);
      //
      //       // 去除socket中存在的节点 不在重复渲染
      //       ret.results.exec_nodes.map(function(item, index) {
      //
      //         resukt_nodeList.map(function(socketItem) {
      //
      //           if (item.node_id === socketItem.id) {
      //             // 删除http中socket存在的节点
      //             ret.results.exec_nodes.splice(index, 1);
      //           };
      //         });
      //       });
      //
      //       // [0,5] + [1,2,3,4] = [0,1,2,3,4,5]
      //       result_exec_nodes = ret.results.exec_nodes.concat(resukt_nodeList);
      //
      //       if (!!result_exec_nodes && result_exec_nodes.length > 0) {
      //         var groupNodes = [];
      //         var selfJson = JSON.parse($scope.getChartJSON());
      //
      //         selfJson.datas.map(function(selfItem, index) {
      //
      //           if (selfItem._className === 'Q.Node' && !!selfItem.json.properties.data.$ref) {
      //             groupNodes.push({
      //               index: index,
      //               item: selfItem,
      //               ref: selfItem.json.properties.data.$ref
      //             });
      //           };
      //         });
      //
      //         result_exec_nodes.map(function(node, index) {
      //           selfJson.datas.map(function(selfItem, index) {
      //
      //             // 更新普通节点
      //             if (selfItem._className === 'Q.Node') {
      //
      //               var refId;
      //
      //               // 判断id是否存在 不存在则为组
      //               if (!!!selfItem.json.properties.data.id) {
      //                 refId = selfItem.json.properties.data.$ref;
      //                 selfItem.json.properties.data = selfJson.refs[refId];
      //               };
      //
      //               if (selfItem.json.properties.data.id === node.node_id) {
      //                 selfItem.json.properties.data.infoData.status = node.status;
      //                 updateNode = selfItem.json.properties.data;
      //                 getNodeData(selfItem.json.properties.data, selfItem.json);
      //               };
      //             };
      //           });
      //         });
      //         updateJSON(selfJson);
      //       };
      //     } else {
      //
      //     };
      //   });
      // };

      // 获取流程节点
      // var getQuneeState = function() {
      //   ManagerProcessQuneeServ.getManagerProcessQuneeState()
      //   .then(function(result) {
      //     console.log('流程节点',result);
      //     if (result.status === 200) {
      //       $scope.quneeState = result.results.state;
      //     };
      //   });
      // };

      // 推送消息
      var onQuneeInfo = function(evt, data) {
        $scope.$apply(function() {
          $scope.processInfoState = !$scope.processInfoState;
          $scope.nodeInfoState = false;
        });
      };

      var getNodeData = function(data, node, isGroup) {
        var type, status, info;

        if (isGroup) {
          type = quneeRefTypeMap[data.infoData.ref_type];
          status = 'init';
          info = $scope.statusNodeMap[type][status];
          data.image = info.icon;
          data.menuList = info.menuList;
          data.status = status;
          data.type = type;
        } else {
          type = data.infoData.type || quneeRefTypeMap[data.infoData.ref_type];
          status = 'init';
          info = $scope.statusNodeMap[type][status];

          if (info) {
            node.image = info.icon;
            node.menuList = info.menuList;
          };
        };
      };

      // 创建折线
      // from 起点
      // to终点
      // pathSegmentList 中间坐标
      var createEdge = function(graph, from, to, pathSegmentList) {
        var edge = graph.createEdge('edge', from, to);
        pathSegmentList && pathSegmentList.map(function(item) {
          edge.addPathSegment([item.x, item.y]);
        });
        return edge;
      };

      // 创建组节点
      var createGroup = function (groupData, expanded, graph) {
        var graph = graph || $window.editor.graph;
        expanded = expanded !== false;
        var group = graph.createGroup(groupData.name);
        group.expanded = expanded;
        var groupHandle = new Q.LabelUI(expanded ? "-" : "+");
        groupHandle.backgroundColor = "#2898E0";
        groupHandle.color = "#FFF";
        groupHandle.padding = new Q.Insets(0, 4);
        groupHandle.borderRadius = 0;
        groupHandle.position = Q.Position.RIGHT_TOP;
        groupHandle.anchorPosition = Q.Position.LEFT_TOP;
        groupHandle.type = "GroupHandle";
        groupHandle.reverseExpanded = function(evt){
            var g = this.parent.data;
            g.expanded = !g.expanded;
        }
        group.addUI(groupHandle, {
            property : "expanded",
            callback: function(value, ui){
              ui.data = value ? "-" : "+";
            }
        });
        return group;
      };

      // 解析json
      var translateToQuneeElements = function(json, graph){
          var map = {};
          var graph = graph || $window.editor.graph;

          // 创建普通节点
          if (json.nodes) {
              Q.forEach(json.nodes, function(data){
                  var node = graph.createNode(data.name, data.x || 0, data.y || 0, data.infoData.ref_type);
                  getNodeData(data, node);
                  node.set("data", data);
                  map[data.id] = node;
              });
          };

          // 创建组
          if (json.groups) {
              Q.forEach(json.groups, function(data){
                  var group = createGroup(data, true, graph);
                  getNodeData(data, group);
                  data.nodes.map(function(item){

                    // 子组
                    var innerNode = graph.createNode(item.name, item.x, item.y);
                    getNodeData(item, innerNode);
                    innerNode.set("data", item);
                    map[item.id] = innerNode;
                    group.addChild(innerNode);
                  });

                  group.set("data", data);
                  map[data.id] = group;
              });
          };

          // 创建连线
          if (json.edges) {
              Q.forEach(json.edges, function(data){
                  var from = map[data.from];
                  var to = map[data.to];

                  if(!from || !to){
                      return;
                  };

                  // data.pathSegmentList = [
                  //   {
                  //     x: 100,
                  //     y: 200
                  //   }
                  // ];

                  var edge = createEdge(graph, from, to, data.infoData.pathList);
                  edge.set("data", data);
              }, graph);
          };
      };


      // 显示右侧信息
      var onShowNodeInfo = function(evt) {
        var data = evt.getData();
        var nodeId;

        console.log(data)

        if(data && data._n0w.data){

          $timeout(function(){
            $scope.$broadcast('node_info',{
              nodeData : data._n0w.data
            })
          },100)

        }else{

          $timeout(function(){
            $scope.$broadcast('node_info',{
              nodeData : data.name
            })
          },100)

        };

        // 是否点击到空白处
        if (!!!data) {
          $scope.$apply(function() {
            $scope.processInfoState = false;
            $scope.nodeInfoState = false;
          });
        } else {

          if (data instanceof Q.Group) {
              var target = $window.editor.graph.hitTest(evt);

              if (target && target.type == "GroupHandle") {
                  target.reverseExpanded();
              } else {
                // nodeId = data._n0w.data.id;
                // $scope.nodeInfo = {
                //   id: nodeId
                // };

                $scope.$apply(function() {
                  $scope.processInfoState = false;
                  $scope.nodeInfoState = true;
                });
              }
          } else {
            // nodeId = data._n0w.data.id;
            // $scope.nodeInfo = {
            //   id: nodeId
            // };

            $scope.$apply(function() {
              $scope.processInfoState = false;
              $scope.nodeInfoState = true;
              //$scope.nodeInfoState = !!!$scope.nodeInfoState;
            });
          }
        };
      };

      var initQuneeEvent = function() {
        editor.graph.onclick = onShowNodeInfo;
      };

      // 获取加载完成的json
      var getChartJSON = function() {

        if (!!$window.editor) {
          return editor.graph.exportJSON(true, {space: '  '});
        } else {
          return null;
        };
      };

      var initEditor = function(callback) {
        $window.isEdit = true;
        $node.find('.editor').graphEditor({
          callback: function(editor){
            $('#search_input').css('margin-top','2px');
            $('.input-group').css('margin-top','2px')
            new GridBackground(editor.graph);
            // editor.graph.delayedRendering = true;
             if($scope.processId){
               translateToQuneeElements($scope.nodeList, editor.graph);
               editor.graph.moveToCenter();
               $timeout(function () {
                 editor.graph.zoomToOverview();
               }, 100);
             }

            $window.editor = editor;
            var selfJson = JSON.parse($scope.getChartJSON());
            initQuneeEvent();
            callback && callback();
          }
        });
      };

      // 局部刷新流程图
      var updateJSON = function(data) {
        editor.submitJSON(data);
      };

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
        $window.$quneeNode = $node;
      };

      var bindListener = function() {
        $scope.nameInput = nameInput;
        $scope.classifyInput = classifyInput;
        //保存按钮
        $scope.processSave = processSave;
        //显示流程属性
        $scope.showProcessEdit = showProcessEdit;
        //放弃btn
        $scope.processAbandon =processAbandon;
        $scope.setTreeData = ManagerProcessTreeServ.setTreeData;
        //流程属性的内容
        $scope.getQuneeProcessInfo = getQuneeProcessInfo;
        //用户list
        $scope.getUserList = getUserList;
        $scope.createGroup = createGroup;
        //解析json
        $scope.translateToQuneeElements = translateToQuneeElements;
        //初始渲染图
        $scope.initEditor = initEditor;
        $scope.updateJSON = updateJSON;
        //获取加载完成的json
        $scope.getChartJSON = getChartJSON;
        //点击显示右侧的节点属性
        $scope.initQuneeEvent = initQuneeEvent;
        //$scope.getQuneeState = getQuneeState;
        //获取json
        $scope.getQuneeStateAndData = getQuneeStateAndData;
        $scope.giveUpBtn = giveUpBtn;
      };

      var initPlugins = function() {
        //初始化流程属性
        $scope.processInfo = {};
        $scope.nodeInfoState = false;
        $scope.processInfoState = false;
        $scope.nameInputShow = true;
        $scope.classifyInputShow = true;
        //流程属性的
        $scope.defaultText = "请选择...";
        $scope.changeNodeList = [];
        // 保存当前流程id
        processId = $stateParams.id;
        $scope.processId = processId;
        if(!!!$scope.processId){
          $scope.initEditor();
        }else{
          // 初始化socket链接
          //SocketServ.initSocket(processId);

          // 只监听跳转路由
          // $rootScope.$on('$stateChangeStart', function() {
          //
          //   // 如果状态不为终止 执行成功 的话 则清除执行id
          //   if ($scope.quneeState === 'manualQuit' || $scope.quneeState === 'execSuccess') {
          //     ParseMapServ.removeItem(processId, 'quitState');
          //     ParseMapServ.removeItem(processId, 'process');
          //   };
          // });
          // 用户接管表单参数
          $scope.userEditorTakeOverModel = {
            check: 1,
            resultQuery: true,
            remark: ''
          };

          // 所有节点状态字典
          ManagerProcessQuneeServ.getManagerProcessQuneeStateMap()
          .then(function(result) {
            console.log('所有节点状态字典',result)
            $scope.statusNodeMap = result;
            $window.statusNodeMap = result;
            getQuneeStateAndData();
          });
          $scope.getUserList();
          $scope.getQuneeProcessInfo();
          $node.find('.htrd-c-button-group').hide();
        }

        //分类tree的请求
        ManagerProcessTreeServ.getProcessTree()
        .then(function(result) {
          console.log('tree'+result)
          $scope.treeData = result.results.map($scope.setTreeData);
        });


        //接受分类tree的id
        $scope.$on('processEdit',function(evt,data) {
          $scope.processInfo.group_name = data.classify;
          console.log($scope.processInfo+"分类的tree")
        });

        //流程属性前置触发条件id
  	    $scope.$on('processProperty',function(evt,data) {
  	      $scope.processInfo.processSelect = data.processSelect.id;
  				console.log($scope.processInfo.processSelect+"前置触发条件idId")
  	    });

        //流程属性: 流程执行结果
        $scope.$on('processResult',function(evt,data) {
          $scope.processInfo.processResult = data.processResult;
          console.log($scope.processInfo)
        });

        //节点属性: 保存后隐藏
        $scope.$on('nodeShow',function(evt,data) {
          $scope.nodeInfoState = data.nodeInfoState;
          console.log($scope.nodeInfoState)
        });

      };

      parseDOM();
      bindListener();
      initPlugins();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {},
      'link': link,
      'templateUrl': 'view/directive/util/quneeEditorEdit.html'
  };
}]);
