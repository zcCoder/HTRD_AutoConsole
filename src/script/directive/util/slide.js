// 滑动
angular.module('HTRD_AutoConsole.A').directive('slide',[function() {
	var link = function(s, e) {
		var $scope, $node;

    var switchActive = function() {
      $scope.active = !$scope.active;
      $scope.callback && $scope.callback($scope.active);
    };

		var parseDOM = function() {
			$scope = s;
			$node = $(e[0]);
		};

		var bindListener = function() {
      $scope.switchActive = switchActive;
		};

		var initPlugins = function() {
			$scope.active = $scope.defaultActive || false;
		};

		var init = function() {
			parseDOM();
			bindListener();
			initPlugins();
		};

		init();
	};
	return {
		'replace': true,
		'restrict' : 'AEC',
		'scope': {
			'defaultActive': '=',
			'callback': '='
		},
		'link': link,
		'templateUrl': 'view/directive/util/slide.html'
	};
}]);
