// tree
angular.module('HTRD_AutoConsole.A').directive('treeItem', ['ManagerProcessTreeServ', function(ManagerProcessTreeServ) {
  var link = function(s, e) {
    var $scope, $node;
    var isInit = true;
    var initTree = function() {
      $scope.$watch('treeData', function() {
        $scope.treeData && $node.jstree({
          'core': {
            'check_callback': true,
            'data': $scope.treeData
          },
          'plugins': [
            'dnd', 'search', 'unique',
            'state', 'types', 'wholerow'
          ]
        });
      });

      // 默认选中根节点
      $node.on('loaded.jstree', function(e, data) {
        var inst = data.instance;
        var obj = inst.get_node(e.target.firstChild.firstChild.lastChild);
        inst.select_node(obj);
      });

      $node.on('changed.jstree', function (e, data) {
          //console.log(data.node.children);
          // if ($scope.selected[0] !== data.selected[0]) {
          //   $scope.selected = data.selected;
          //   $scope.callBack && $scope.callBack(data);
          // }

        if ($scope.selected[0] !== data.selected[0]) {
          $scope.selected = data.selected;

          if (!$scope.isInitLoad) {
            $scope.callBack && $scope.callBack(data);
          } else {
            $scope.isInitLoad = false;
          };
        };
      });

      // 监听展开文件夹事件
      $node.on('open_node.jstree', function(e, data) {
        // 通过未加载属性进行ajax展开tree
        if (data.node.li_attr.notload) {
          // 隐藏占位文件夹
          $('#' + data.node.children[0]).hide();
          // 加载tree
          ManagerProcessTreeServ.getProcessTreeById({
            // 'format': 'json',
            'id': data.node.id
          }).then(function(res) {
            // 删除占位文件夹
            $node.jstree().delete_node([data.node.children[0]]);

            // 移除未加载属性
            delete data.node.li_attr.notload;

            // 整理数据,后端的数据不符合tree生成
            var treeArr = res.results.children.map(ManagerProcessTreeServ.setTreeData);
            // // 由于create_node的限制,一次只能创建一个节点
            treeArr.forEach(function(item) {
              $node.jstree().create_node(data.node.id, item, 'last');
            });
          });
        }
      });
    };

    var parseDOM = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {
      $scope.selected = '';
      $scope.initTree = initTree;
    };

    var initPlugins = function() {
      $scope.initTree();
    };

    var init = function() {
      parseDOM();
      bindListener();
      initPlugins();
    };

    init();
  };
  return {
    'replace': true,
    'restrict': 'AEC',
    'scope': {
      'treeData': '=',
      'callBack': '=',
      'isInitLoad': '='
    },
    'link': link,
    'templateUrl': 'view/directive/util/treeItem.html'
  };
}]);
