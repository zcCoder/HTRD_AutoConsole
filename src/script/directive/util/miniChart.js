// 迷你图表
angular.module('HTRD_AutoConsole.A').directive('miniChart',[function() {
  var link = function(s, e) {

      var $scope, $node;

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {

      };

      var initPlugins = function() {
        $scope.$watch('data', function() {

          if (!!$scope.data) {
            var chartNode = $node.find('.mainChart')[0];

            // 基于准备好的dom，初始化echarts图表
            var chart = echarts.init(chartNode);

            var option = {
                grid: {
                  x: 30,
                  y: 20,
                  x2: 10,
                  y2: 20
                },
                tooltip: {
                    show: true
                },
                legend: {
                    data: $scope.data.title
                },
                xAxis : [
                    {
                        type : 'category',
                        data : $scope.data.xAxis
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name": $scope.data.title[0],
                        "type": "line",
                        "data": $scope.data.series
                    }
                ]
            };

            chart.setOption(option);
          };
        });
      };

      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };
  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'data': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/miniChart.html'
  };
}]);
