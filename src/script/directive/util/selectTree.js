// 下拉框带tree
angular.module('HTRD_AutoConsole.A').directive('selectTree',['$state', 'ManagerProcessTreeServ',  function($state, ManagerProcessTreeServ) {
  var link = function(s, e) {
    var $scope, $node;
    var activeTree = function(item) {
      if (item.node && $scope.panelShow) {
        $scope.panelShow = false;
        //分类的id  item.node.id;
        switch ($scope.tag) {

          case 'classify':
            $scope.$emit("processEdit",{
              classify : item.node.id
            })
            break;

          case 'process':
            $scope.$emit("nodeInfoList",{
              quoteProcessSelect : item.node.id,
              tag: 'process'
            })
            break;

          default:
        };

        $scope.$apply(function() {
          $scope.defaultText = item.node.text;
          $scope.activeId = item.node.id;
        });
      };
    };

    // var initTreeData = function() {
    //   ManagerProcessTreeServ.getProcessTree()
    //   .then(function(result) {
    //     $scope.treeData = result.results.map($scope.setTreeData);
    //   });
    // };

    var togglePanel = function() {
      $scope.panelShow = !!!$scope.panelShow;
    };

    var parseDOM = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {
      //$scope.initTreeData = initTreeData;
      $scope.togglePanel = togglePanel;
      //$scope.setTreeData = ManagerProcessTreeServ.setTreeData;
      $scope.activeTree = activeTree;
    };

    var initPlugins = function() {
      $scope.isInitLoad = true;
      //$scope.initTreeData();
    };

    var init = function() {
      parseDOM();
      bindListener();
      initPlugins();
    };

    init();
  };
  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'defaultText':'=',
        'treeData':'=',
        'tag':'='
      },
      'link': link,
      'templateUrl': 'view/directive/util/selectTree.html'
  };
}]);
