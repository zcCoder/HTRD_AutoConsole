// 时间
angular.module('HTRD_AutoConsole.A').directive('tree',[function() {
  var link = function(s, e) {
      var $scope, $node;
      var getStateClass = function(tabIndex, index) {
        var classArr = [
          ['glyphicon-pause', 'glyphicon-exclamation-sign', 'glyphicon-play'],
          ['glyphicon-ok-sign', 'glyphicon-user', 'glyphicon-off']
        ];
        return classArr[tabIndex][index];
      }
      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
      };

      var initPlugins = function() {
        $scope.tabs = ['执行中的流程', '已结束的流程'];
        $scope.tabs.active = '0';
        $scope.map = ['running', 'end'];
        $scope.getStateClass = getStateClass;
      };

      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };
  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'treeData': '=',
        'processState': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/tree.html'
  };
}]);
