/**
 * Created by Administrator on 2017/3/24.
 */
angular.module('HTRD_AutoConsole.A').directive('log1',function () {
	var $scope, $node;
	var link = function () {

		var parseDOM = function() {
			$scope = s;
			$node = $(e[0]);
		};

		var bindListener = function() {

		};

		var initPlugins = function() {

			// 打印传递进来的数据
			console.log($scope.logInfo1);
		};

		parseDOM();
		bindListener();
		initPlugins();
	};
	return {
		'replace': true,
		'restrict' : 'AEC',
		'scope': {
			// 传递进来的数据
			'logInfo1': '='
		},
		'link': link,
		'templateUrl': 'view/directive/util/log1.html'
	}
})