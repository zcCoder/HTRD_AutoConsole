// 左侧菜单 -
angular.module('HTRD_AutoConsole.A').directive('quneeEditorProcessInfo',[function() {
  var link = function(s, e) {
      var $scope, $node;

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {};

      var initPlugins = function() {};

      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'processInfo': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/quneeEditorProcessInfo.html'
  };
}]);
