// 左侧菜单
angular.module('HTRD_AutoConsole.A').directive('multipleSelect',[function() {
  var link = function(s, e) {
      var $scope, $node;

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {

      };

      var initPlugins = function() {
        $node.find("select").multipleSelect({
          filter: true
        });

      };

      parseDOM();
      bindListener();
      initPlugins();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'list': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/multipleSelect.html'
  };
}]);
