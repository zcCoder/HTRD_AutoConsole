// 左侧菜单
angular.module('HTRD_AutoConsole.A').directive('message',['MessagePanelServ','DialogServ','AlertServ','$state',function(MessagePanelServ,DialogServ,AlertServ,$state) {
  var link = function(s, e) {
      var $scope, $node, strapModal;

      //清空消息;弹出框
      var clearMessage = function () {
        strapModal = DialogServ.modal({
          'scope': $scope,
          'templateUrl': 'view/console/modal/testModal.html'
        });
        strapModal.$promise.then(strapModal.show);
      };

      //弹出框的确定按钮
      var clearMessageSure = function () {
        strapModal.hide();
        $scope.$emit("clearMsg",{
          messagePanelState: false,
          messageCount: 0,
          messageData: []
        });
      };

      //点击查看
      var messageLook = function (item) {

        MessagePanelServ.getManagerOperation(item)
          .then(function(resuit){

            if(resuit.status === 200){

              if($scope.count > 99){
                    $scope.count -= 1;
                    $scope.messageData.splice(item.$index, 1);
                }else{
                    $scope.messageData.splice(item.$index, 1);
                    $scope.messageCount -= 1;
                };

                 $scope.$emit("countReduce",{
                  messageCount: $scope.messageCount,
                });

                //根据返回的is_exec 确定跳转的流程/执行id
                if (!item.is_exec) {

                  $state.go("console.base.manager.process",{id: item.pipeline_id});

                } else {

                  $state.go("console.base.manager.historyProcess",{id: item.ex_pl_id, processId: item.pipeline_id});

                }
            };
          })
      };

      //点击忽略
      var messageIgnore = function (item) {

        event.stopPropagation();

        MessagePanelServ.getManagerOperation(item)
          .then(function(resuit){

            if(resuit.status === 200){

              AlertServ.showSuccess('忽略成功');

              if($scope.count > 99){
                  $scope.count -= 1;
                  $scope.messageData.splice(item.$index, 1);
                }else{
                  $scope.messageData.splice(item.$index, 1);
                   $scope.messageCount -= 1;
                };

                $scope.$emit("countReduce",{
                  messageCount: $scope.messageCount,
                  messagePanelState: true,
                });

            }else{
              AlertServ.showSuccess('忽略失败');
            };
          })
      };

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
        $scope.clearMessage = clearMessage;
        $scope.clearMessageSure =clearMessageSure;
        $scope.messageLook =messageLook;
        $scope.messageIgnore = messageIgnore
      };

      var initPlugins = function() {
        $scope.$on("messageCount",function (evt,data) {
          $scope.messageCount = data.messageCount;
          $scope.count = data.count;
        });
        //console.log("消息中心"+$scope.messageData)
      };

      parseDOM();
      bindListener();
      initPlugins();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        "messageData": '=',
        "messageCount": '='
      },
      'link': link,
      'templateUrl': 'view/directive/message/messagePanel.html'
  };
}]);
