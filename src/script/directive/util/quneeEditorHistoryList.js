// 左侧菜单 -
angular.module('HTRD_AutoConsole.A').directive('quneeEditorHistoryList',['$state', function($state) {
  var link = function(s, e) {
      var $scope, $node;


      var activeItem = function(item) {
        console.log('processId', $scope.processId);
        console.log('execId', item.id);
        $state.go('console.base.manager.historyProcess', {
          id: item.id,
          processId: $scope.processId
        });
      };

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
        $scope.activeItem = activeItem;
      };

      var initPlugins = function() {
      };

      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'historyList': '=',
        'processId': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/quneeEditorHistoryList.html'
  };
}]);
