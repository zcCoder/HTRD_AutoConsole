// 面包屑
angular.module('HTRD_AutoConsole.A').directive('crumbs',['$state', function($state) {
  var link = function(s, e) {
    var $scope, $node;

    var activeIndex = function(item) {
      console.log('item', item);

      if (!!item.url) {
        $state.go(item.url);
      };
    };

    var parseDOM = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {
      $scope.activeIndex = activeIndex;
    };

    var initPlugins = function() {

      // $scope.config = {
      //   index: {
      //     title: '运维管理',
      //     url: 'console.base.home.index',
      //     icon: 'htrd-c-icon crumbs home'
      //   },
      //
      //   items: [
      //     {
      //       title: '流程列表',
      //       icon: 'htrd-c-icon crumbs right',
      //       url: ''
      //     },
      //
      //     {
      //       title: '流程详情',
      //       icon: 'htrd-c-icon crumbs right'
      //     }
      //   ]
      // }
    };

    var init = function() {
      parseDOM();
      bindListener();
      initPlugins();
    };

    init();
  };
  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'config': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/crumbs.html'
  };
}]);
