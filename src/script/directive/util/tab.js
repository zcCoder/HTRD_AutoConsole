// 选项卡
angular.module('HTRD_AutoConsole.A').directive('tab',[function() {
  var $scope, $node;
  var link = function(s, e) {

      var switchTab = function(item) {
        $scope.activeIndex = item.index;
        $scope.callback($scope.activeIndex);
      };

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
        $scope.switchTab = switchTab;
      };

      var initPlugins = function() {
        $scope.activeIndex = 1;
        $scope.tabList = [
          {
            index: 1,
            name: '基本属性'
          },

          {
            index: 2,
            name: '性能监控'
          }
          // ,
          //
          // {
          //   index: 3,
          //   name: '远程桌面'
          // },
        ];

        $scope.$watch('nodeInfo', function(n, o, scope) {
          $scope.activeIndex = 1;
          $scope.tabList[$scope.activeIndex - 1].active = true;
        });
      };

      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };
  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'nodeInfo': '=',
        'callback': '=',
				'type': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/tab.html'
  };
}]);
