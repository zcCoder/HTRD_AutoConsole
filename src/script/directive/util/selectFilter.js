// 下拉框带查询
angular.module('HTRD_AutoConsole.A').directive('selectFilter',['$state', '$timeout', function($state, $timeout, ManagerProcessTreeServ) {
  var link = function(s, e) {
    var $scope, $node;

    // 时时监听输入的内容 传递给控制器 发送请求
    var changeText = function() {
      console.log('changeText', $scope.text);
      $scope.callback && $scope.callback($scope.text);
    };

    var activeItem = function(item,$event) {
      $scope.selFlowTarget=$event.target;
      $scope.text = item.name;
      $scope.activeCallback && $scope.activeCallback(item);
      $scope.list = [];
    };

    // 焦点事件
    var changeFocus = function() {
      $timeout(function () {
        e.find("#listItem").stop(true,true).show();
      }, 300);

    };

    var changeBlur = function() {
      $timeout(function () {
        e.find("#listItem").stop(true,true).hide();
      }, 300)
    };

    var parseDOM = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {
      $scope.changeText = changeText;
      $scope.activeItem = activeItem;
      $scope.changeBlur=changeBlur;
      $scope.changeFocus=changeFocus;
    };

    var initPlugins = function() {
      $scope.text = $scope.text || '';
      // 接收外面查询出来的数据
      $scope.$on('changeSelectFilterList', function(evt, data) {
        console.log('data.id', data.id);
        console.log('$scope.id', $scope.id);

        if (data.id === $scope.id) {
          $scope.list = data.data;
        };
      });
    };

    var init = function() {
      parseDOM();
      bindListener();
      initPlugins();
    };

    init();
  };
  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'callback': '=',
        'activeCallback': '=',
        'text': '=',
        'id': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/selectFilter.html'
  };
}]);
