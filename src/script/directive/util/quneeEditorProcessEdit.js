// 流程属性编辑
angular.module('HTRD_AutoConsole.A').directive('quneeEditorProcessEdit',['ManagerListServ',function(ManagerListServ) {
	var link = function(s, e) {
		var $scope, $node;

		//选择流程
		var activeProcess = function (item) {
			$scope.pipelineName = item.name;
				console.log('aaaaa')
				$scope.$emit("processProperty",{
					processSelect: item
				})
		};

		var getProcessListName = function (item) {
			ManagerListServ.getProcessList({
				search: item
			})
			.then(function (ret) {

				if (ret.status === 200) {
					$scope.$broadcast('changeSelectFilterList', {
						data: ret.results.data,
						id: 'processSelectId'
					});
				};
			});
		};

		var parseDOM = function() {
			$scope = s;
			$node = $(e[0]);
		};

		var bindListener = function() {
			$scope.activeProcess = activeProcess;
			$scope.getProcessListName = getProcessListName;
		};


		var initPlugins = function() {
			$scope.processResultText = '请选择流程执行结果';
			$scope.processSelectId = 'processSelectId'
			$scope.processResultList = [
				{
					id: 1,
					title: '执行成功'
				},

				{
					id: 2,
					title: '执行失败'
				}
			];

		};

		var init = function() {
			parseDOM();
			bindListener();
			initPlugins();
		};

		init();
	};

	return {
		'replace': true,
		'restrict' : 'AEC',
		'scope': {
			'processInfo':'=',
			'treeData':'=',
		},
		'link': link,
		'templateUrl': 'view/directive/util/quneeEditorProcessEdit.html'
	};
}]);
