// 左侧菜单 -
angular.module('HTRD_AutoConsole.A').directive('quneeEditorNodeInfo',['ManagerProcessServ',
function(ManagerProcessServ) {
  var link = function(s, e) {
      var $scope, $node;

      var quneeRefTypeMapStr = {
        1: '基本节点',
        2: '用户确认节点',
        3: '引用流程节点',
        4: '开始节点',
        5: '结束节点',
        6: '关机节点',
        7: '重启节点'
      };

      var getNodeCharts = function() {
        getNodeChartByCPU();
        getNodeChartByMemory();
      };

      var getNodeChartByCPU = function() {
        ManagerProcessServ.getNodeChartByCPU()
        .then(function(ret) {

          if (ret.state === 0) {
            $scope.chartDataByCPU = {
              xAxis: [],
              series: []
            };

            // 格式化数据
            ret.data.map(function(item, index) {
              var xAxiItem = new Date(parseInt(item.clock * 1000)).toTimeString().split(' ')[0];
              var serieItem = item.value;
              $scope.chartDataByCPU.xAxis.push(xAxiItem);
              $scope.chartDataByCPU.series.push(serieItem);
            });

            $scope.chartDataByCPU.title = ['CPU'];
          };
        });
      };

      var getNodeChartByMemory = function() {
        ManagerProcessServ.getNodeChartByMemory()
        .then(function(ret) {

          if (ret.state === 0) {
            $scope.chartDataByMemory= {
              xAxis: [],
              series: []
            };

            // 格式化数据
            ret.data.map(function(item, index) {
              var xAxiItem = new Date(parseInt(item.clock * 1000)).toTimeString().split(' ')[0];
              var serieItem = item.value;
              $scope.chartDataByMemory.xAxis.push(xAxiItem);
              $scope.chartDataByMemory.series.push(serieItem);
            });

            $scope.chartDataByMemory.title = ['内存'];
          };
        });
      };

      var getNodeInfo = function() {
        ManagerProcessServ.getNodeInfo({
          id: $scope.nodeInfo.id
        })
        .then(function(ret) {

          if (ret.status === 200) {
            $scope.nodeBaseInfo = ret.results;
            $scope.nodeBaseInfo.type = quneeRefTypeMapStr[$scope.nodeBaseInfo.ref_type];
          } else {
            $scope.nodeBaseInfo = {};
          };
        });
      };

      var callback = function(index) {
        $scope.activeIndex = index;
      };

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
        $scope.callback = callback;
      };

      var initPlugins = function() {
        $scope.activeIndex = 1;
        $scope.$watch('nodeInfo', function(a, b) {

          // nodeId change 后 加载多个图表 和 基本信息
          getNodeInfo();
          getNodeCharts();
        });
      };

      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'nodeInfo': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/quneeEditorNodeInfo.html'
  };
}]);
