// 左侧菜单
angular.module('HTRD_AutoConsole.A').directive('dropdownmenu',[function() {
  var link = function(s, e) {
    var $scope, $node;
    var activeMenu = function(item) {
      //console.log(item+'drop');
      $scope.defaultText = item.title || item.name || item;
      $scope.defaultId = item.id || 0;
      $scope.callBack && $scope.callBack(item);
    };

    var parseDOM = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {
      $scope.activeMenu = activeMenu;
    };

    var initPlugins = function() {
      $scope.defaultText = $scope.defaultText || '请选择';
    };

    parseDOM();
    bindListener();
    initPlugins();
  };

  return {
    'replace': true,
    'restrict' : 'AEC',
    'scope': {
      'list': '=',
      'callBack': '=',
      'defaultText': '=',
      'defaultId': '='
    },
    'link': link,
    'templateUrl': 'view/directive/util/dropdownMenu.html'
  };
}]);
