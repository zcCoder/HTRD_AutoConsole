// 时间
angular.module('HTRD_AutoConsole.A').directive('dateTimePicker',[function() {
  var $scope, $node;
  var link = function(s, e) {

     /*var changeData = function(){
        if ((!!$scope.startDate && !!!$scope.endDate) || (!!!$scope.endDate && !!$scope.startDate)) {
          $scope.$emit("showBtn",{
            isClick : true;
          })
        } else{
          $scope.$emit("showBtn",{
             isClick : false;
          })
        }
      }*/
       /*当前时间格式*/
       var date = new Date();
        var endDate = function (){
                    Y = date.getFullYear() + '-';
                    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
                    D = (date.getDate() < 10 ? '0'+(date.getDate()) : date.getDate()) + ' ';
                    h = (date.getHours() < 10 ? '0'+(date.getHours()) : date.getHours()) + ':';
                    m = (date.getMinutes() < 10 ? '0'+(date.getMinutes()) : date.getMinutes());
            return Y+M+D+h+m;
        };

        /*倒退一月时间格式*/
        var startDate = function(){
          var time = Date.parse(date) - 30*24*60*60*1000;
                nowTime = new Date(time);
                    Y = nowTime.getFullYear() + '-';
                    M = (nowTime.getMonth() +1< 10 ? '0'+(nowTime.getMonth()+1) : nowTime.getMonth()+1) + '-';
                    D = (nowTime.getDate() < 10 ? '0'+(nowTime.getDate()) : nowTime.getDate()) + ' ';
                    h = (nowTime.getHours() < 10 ? '0'+(nowTime.getHours()) : nowTime.getHours()) + ':';
                    m =(nowTime.getMinutes() < 10 ? '0'+(nowTime.getMinutes()) : nowTime.getMinutes());
            return Y+M+D+h+m;
        };

        /*时间格式转时间戳*/
        var getTime = function(dateStr){
          var newstr = dateStr.replace(/-/g,'/'); 
          var date =  new Date(newstr); 
          var time_str = date.getTime();
          return time_str;
        };

      /*时间戳比较,改变查询的状态*/
      var changeData = function(e){
        console.log(e)

        /*时间戳格式*/
        var startTime = getTime($scope.min_date);
        var endTime = getTime($scope.max_date);
        if(startTime>endTime){
          $scope.$emit("showBtn",{
            isClick : true,
            min_date:$scope.min_date,
            max_date:$scope.max_date
          })
        }else{
          $scope.$emit("showBtn",{
            isClick : false,
            min_date:$scope.min_date,
            max_date:$scope.max_date
          })
        }
      }

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {
        $scope.changeData =changeData;
      };

      var initPlugins = function() {
        $scope.max_date= endDate();
        $scope.min_date = startDate(); 

        $scope.$emit('toDate',{
        min_date: $scope.min_date,
        max_date: $scope.max_date
      });

    	   $node.find('input').datetimepicker({
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
      };


      var init = function() {
        parseDOM();
        bindListener();
        initPlugins();
      };

      init();
  };
  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {},
      'link': link,
      'templateUrl': 'view/directive/util/dateTimePicker.html'
  };
}]);
