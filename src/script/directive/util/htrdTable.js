// 选项卡
angular.module('HTRD_AutoConsole.A').directive('htrdtable',[function() {
  var $scope, $node;
  var link = function(s, e) {

    var parseDOM = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {

    };

    var initPlugins = function() {

      $scope.$watch("data",function () {

      })
    };

    var init = function() {
      parseDOM();
      bindListener();
      initPlugins();
    };

    init();
  };
  return {
    'replace': true,
    'restrict' : 'AEC',
    'scope': {
      'data': '='
    },
    'link': link,
    'templateUrl': 'view/directive/util/htrdTable.html'
  };
}]);
