angular.module('HTRD_AutoConsole.A').directive('homeList',['$state','StatusMapServ',
function($state,StatusMapServ) {
  var link = function(s, e) {
    var $scope, $node;
    var title;

    var statusMap = {
      1:{
        text:"检测异常"
      },
      2:{
        text:"执行超时"
      },
      3:{
        text:"执行错误"
      }
    };

    var tag = [
       {
         text: "今日待执行",
         url:'console.base.home.waitlist'
       },
      {
         text:"今日执行中",
         url:'console.base.home.runlist'
       },
       {
         text:"今日执行告警",
         url:'console.base.home.warnlist'
       },
       {
         text:"今日已结束",
         url:'console.base.home.endlist'
       },
       {
         text:"今日执行成功",
         url:'console.base.home.successlist'
       },
       {
         text:"今日手工接管",
         url:'console.base.home.handlist'
       }
   ];

   //查看流程的list
   var getLook = function (item) {

     if($scope.index === "0" || $scope.index === "1" || $scope.index === "2"){
       $state.go('console.base.manager.process',{id:item.pipeline});
     }else{
      console.log(item.id+'---'+item.pipeline)
       $state.go('console.base.manager.historyProcess',{
         id: item.id,
         processId: item.pipeline
       });
     }
   };

   var getMoreList = function(index){
      $state.go(tag[index].url)
   };

    var parseDom = function() {
      $scope = s;
      $node = $(e[0]);
    };

    var bindListener = function() {
      $scope.getLook = getLook;
      $scope.getMoreList = getMoreList
    };

    var initPlugins = function() {

      title = tag[$scope.index].text;

      $scope.title = title;

      //今日待执行的显示;
      switch($scope.index)
        {
           case '1':
           case '3':
            $scope.data.data.map(function(item){
              var model = StatusMapServ.getStatusData(item.result_status);
              item.result_status = model.text.substring(0,4);
           });
           break;

          case '2':
             $scope.data.data.map(function(item){
                item.pipeline_name = item.result_info;
             });
            break;

           case '4':
            $scope.data.data.map(function(item){
              var model = StatusMapServ.getStatusData(item.result_status);
              item.result_status =' ';
           });
           break;


           case '5':
            $scope.data.data.map(function(item){
              var model = StatusMapServ.getStatusData(item.result_status);
              item.result_status = model.text.replace('手工接管'||'手工接管后的执行结果：', '');
              if(item.result_status.indexOf('手工接管后的执行结果：')){
                item.result_status = model.text.replace('手工接管后的执行结果：', '');
              }else if(item.result_status.indexOf('手工接管')){
                item.result_status = model.text.replace('手工接管', '');
              }
           });
           break;
        };

  };

    var init = function() {
      parseDom();
      bindListener();
      initPlugins();
    };

    init();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'data': '=',
        'index': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/homeList.html'
  };
}]);
