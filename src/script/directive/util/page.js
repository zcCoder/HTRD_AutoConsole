// 分页
angular.module('HTRD_AutoConsole.A').directive('page',[function() {
	var link = function(s, e) {
		var $scope, $node;

		var toTopPage = function() {
			$scope.toIndexPage(1);
		};

		var toLastPage = function() {
			$scope.toIndexPage($scope.pageConfig.pageCount);
		};

		var toNextPage = function() {
			$scope.toIndexPage($scope.pageConfig.pageNo + 1);
		};

		var toPrevPage = function() {
			$scope.toIndexPage($scope.pageConfig.pageNo - 1);
		};

		var queryPage = function() {

			if (!typeof $scope.queryPageValue === 'number') {
				return false;
			}
			else if ($scope.queryPageValue >= 1 && $scope.queryPageValue <= $scope.pageConfig.pageCount) {
				$scope.toIndexPage($scope.queryPageValue);
			};
		};

		var toIndexPage = function(index, isInit) {

			if (index < 1) {
				index = 1;
			}
			else if (index > $scope.pageConfig.pageCount) {
				index = $scope.pageConfig.pageCount;
			};

			$scope.pageConfig.pageList.map(function(item) {

				if (item.index === index) {
					item.active = true;
				} else {
					item.active = false;
				};
			});

			// 判断是否是在首页 / 尾页
			$scope.pageConfig.isTop = ($scope.pageConfig.pageNo == 1);
			$scope.pageConfig.isLast = ($scope.pageConfig.pageNo == $scope.pageConfig.pageCount);


			if (!!!isInit) {
				$scope.$emit('toPage', {
					page: index
				});
			};
		};

		var createPageList = function(start, end, isPrev) {
			var pageListSize = 0;
			var pageList = [];

			if (isPrev) {

				// 如果总页数 小于5页
				if ($scope.pageConfig.pageCount < end) {
					end = $scope.pageConfig.pageCount;
				}
			};

			pageListSize = end - start;

			for (var i = start;i < end;i++) {
				pageList.push({
					index: (i + 1)
				});
			};

			$scope.pageConfig.pageList = pageList;
		};

		var parseDOM = function() {
			$scope = s;
			$node = $(e[0]);
		};

		var bindListener = function() {
			$scope.toTopPage = toTopPage;
			$scope.toLastPage = toLastPage;
			$scope.toNextPage = toNextPage;
			$scope.toPrevPage = toPrevPage;
			$scope.toIndexPage = toIndexPage;
			$scope.createPageList = createPageList;
			$scope.queryPage = queryPage;
		};

		var initPlugins = function() {
			$scope.pageConfig = {};

			// 接收数据传入
			$scope.$on('toList', function (evt, data) {
				data.limit = data.limit || 20;

				// 保存分页基本配置
				// pageNo 当前第几页
				// pageSize 每页大小 默认20
				// pageCount 共多少页
				// totalCount 共多少条
				$scope.pageConfig = {
					pageNo: data.page,
					pageList: data.list,
					pageSize: data.limit || 20,
					pageCount: Math.ceil(data.count / data.limit),
					totalCount: data.count
				};

				$scope.queryPageValue = $scope.pageConfig.pageNo;

				// 判断当前pageNo
				// 显示不同的index 数量

				// 总页数大于1
				if ($scope.pageConfig.pageCount > 1) {

					// 当前页数 小于5
					if ($scope.pageConfig.pageNo <= 4) {
						createPageList(0, 5, true);
						$scope.toIndexPage($scope.pageConfig.pageNo, true);
					}

					else if ($scope.pageConfig.pageNo > 4 && $scope.pageConfig.pageNo < 6) {
						createPageList(0, $scope.pageConfig.pageNo + 1, true);
						$scope.toIndexPage($scope.pageConfig.pageNo, true);
					}

					else if ($scope.pageConfig.pageNo == 6) {
						createPageList($scope.pageConfig.pageNo - 4, $scope.pageConfig.pageNo + 2, true);
						$scope.toIndexPage($scope.pageConfig.pageNo, true);
					}

					else if ($scope.pageConfig.pageNo > 6) {
						createPageList($scope.pageConfig.pageNo - 5, $scope.pageConfig.pageNo + 1, true);
						$scope.toIndexPage($scope.pageConfig.pageNo, true);
					};
				}
			});

		};

		var init = function() {
			parseDOM();
			bindListener();
			initPlugins();
		};

		init();
	};
	return {
		'replace': true,
		'restrict' : 'AEC',
		'scope': {
			'data': '='
		},
		'link': link,
		'templateUrl': 'view/directive/util/page.html'
	};
}]);
