// 拓扑图编辑
angular.module('HTRD_AutoConsole.A').directive('multiSelect',['$window', '$timeout',  function($window, $timeout) {
  var link = function(s, e) {
      var $scope, $node, allList, queryList;

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };

      var bindListener = function() {

      };


      var initPlugins = function() {
        allList = $scope.allData && $scope.allData.map(function(item, index) {
          item.value = item.name;
          delete item.name;
          return item;
        });

        queryList = $scope.queryData && $scope.queryData.map(function(item,index){
          item.value = item.name;
          delete item.name;
          return item;
        });

        allList && allList.map(function(allItem, allIndex) {
          queryList && queryList.map(function(item, index) {

            if (allItem.id === item.id) {
              allItem.select = true;
            };
          });
        });



        $node.find('#pre-selected-options').multiSelect({
          selectableHeader: "<input type='text' class='search-input form-contro' autocomplete='off' placeholder='请输入名称'>",
          selectionHeader: "<input type='text' class='search-input form-contro' autocomplete='off' placeholder='请输入名称'>",
          selectableFooter: "<div id='select-all'>全选</div>",
          selectionFooter: "<div id='deselect-all'>全不选</div>",
          // optionList: $scope.data,
          optionList: allList,
          afterInit: function(ms){
            var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
              .on('keydown', function(e){
                if (e.which === 40){
                  that.$selectableUl.focus();
                  return false;
                }
            });


            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
              .on('keydown', function(e){
                if (e.which == 40){
                  that.$selectionUl.focus();
                  return false;
                }
              });
          },

          afterSelect: function (values){
            //console.log('left -> right values', values);

            if(Array.isArray(values)){

              if(values.length > 1){
                queryList = values;
              }else{
                queryList.push(values[0])
              }
              //console.log('queryList',queryList)
              $scope.$emit('selectValue',{
                selectData:queryList,
                tag: $scope.tag
              });
            }
          },

          afterDeselect: function(values){
            //console.log('right -> left values', values);
            if(Array.isArray(values)){

              if(values.length > 1){
                queryList = [];
              }else{
                var index = queryList.indexOf(values[0])
                queryList.splice(index,1)
              }
             //console.log('queryList',queryList)
            };

            $scope.$emit('selectValue',{
              selectData:queryList,
              tag: $scope.tag
            })
          }
        });

        $node.find('#select-all').click(function(){
          $node.find('#pre-selected-options').multiSelect('select_all');
              return false;
        });
        $node.find('#deselect-all').click(function(){
          $node.find('#pre-selected-options').multiSelect('deselect_all');
              return false;
        });
      };
      parseDOM();
      bindListener();
      initPlugins();
  };


  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {
        'title': '=',
        'allData': '=',
        'queryData': '=',
        'tag': '='
      },
      'link': link,
      'templateUrl': 'view/directive/util/multiSelect.html'
  };
}]);
