// 左侧菜单
angular.module('HTRD_AutoConsole.A').directive('timePicker',['$timeout',function($timeout) {
  var link = function(s, e) {
      var $scope, $node;

      var parseDOM = function() {
        $scope = s;
        $node = $(e[0]);
      };


      var bindListener = function() {

      };

      var initPlugins = function() {

        $node.find('#timepicker1').timepicker(
          {
            showMeridian: false,
            explicitMode: true,
            showSeconds: true,
            disableMousewheel: true
          }
        );

        var time_out = $("#timepicker1").val();
        $node.find('#timepicker1').on('changeTime.timepicker',function(e){
          $('#timeDisplay').text(e.time.value);
          time_out = e.time.value;

          $scope.$emit('timeOut',{
            timeOut: time_out
          });
        });

        $timeout(function(){
          $scope.$emit('timeOut',{
            timeOut: time_out
          })
        },1000)
      };

      parseDOM();
      bindListener();
      initPlugins();
  };

  return {
      'replace': true,
      'restrict' : 'AEC',
      'scope': {

      },
      'link': link,
      'templateUrl': 'view/directive/util/timePicker.html'
  };
}]);
