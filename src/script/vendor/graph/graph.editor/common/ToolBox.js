!function (Q, $) {
    var createElement = function (className, parent, tag, html) {
        var element = document.createElement(tag || 'div');
        element.className = className;
        $(element).html(html);
        if (parent) {
            parent.appendChild(element);
        }
        return element;
    }

    var forEach = function (object, call, scope) {
        if (Array.isArray(object)) {
            return object.forEach(function (v) {
                call.call(this, v);
            }, scope);
        }
        for (var name in object) {
            call.call(scope, object[name], name);
        }
    }
    var defaultImageStyles = {
        fillColor: '#EEE',
        lineWidth: 1,
        strokeStyle: '#2898E0',
        padding: {left: 1, top: 1, right: 5, bottom: 5},
        shadowColor: '#888',
        shadowOffsetX: 2,
        shadowOffsetY: 2,
        shadowBlur: 3
    }
    var nodeImageStyles = {};
    nodeImageStyles[Q.Styles.RENDER_COLOR] = 'renderColor';
    nodeImageStyles[Q.Styles.RENDER_COLOR_BLEND_MODE] = 'renderColorBlendMode';
    nodeImageStyles[Q.Styles.SHAPE_FILL_COLOR] = 'fillColor';
    nodeImageStyles[Q.Styles.SHAPE_STROKE_STYLE] = 'strokeStyle';
    nodeImageStyles[Q.Styles.SHAPE_LINE_DASH] = 'borderLineDash';
    nodeImageStyles[Q.Styles.SHAPE_LINE_DASH_OFFSET] = 'borderLineDashOffset';
    //nodeImageStyles[Q.Styles.SHAPE_FILL_GRADIENT] = 'fillGradient';
    nodeImageStyles[Q.Styles.SHAPE_OUTLINE] = 'outline';
    nodeImageStyles[Q.Styles.SHAPE_OUTLINE_STYLE] = 'outlineStyle';
    nodeImageStyles[Q.Styles.LINE_CAP] = 'lineGap';
    nodeImageStyles[Q.Styles.LINE_JOIN] = 'lineJoin';
    nodeImageStyles[Q.Styles.BACKGROUND_COLOR] = 'backgroundColor';
    nodeImageStyles[Q.Styles.BACKGROUND_GRADIENT] = 'backgroundGradient';
    nodeImageStyles[Q.Styles.BORDER] = 'border';
    nodeImageStyles[Q.Styles.BORDER_COLOR] = 'borderColor';
    nodeImageStyles[Q.Styles.BORDER_LINE_DASH] = 'borderLineDash';
    nodeImageStyles[Q.Styles.BORDER_LINE_DASH_OFFSET] = 'borderLineDashOffset';
    //Styles.IMAGE_BACKGROUND_COLOR = "image.background.color";
    //Styles.IMAGE_BACKGROUND_GRADIENT = "image.background.gradient";
    //Styles.IMAGE_BORDER = "image.border.width";
    //Styles.IMAGE_BORDER_STYLE = Styles.IMAGE_BORDER_COLOR = "image.border.style";
    //Styles.IMAGE_BORDER_LINE_DASH = "image.border.line.dash";
    //Styles.IMAGE_BORDER_LINE_DASH_OFFSET = "image.border.line.dash.offset";
    //Styles.IMAGE_RADIUS = Styles.IMAGE_BORDER_RADIUS = "image.radius";
    //Styles.IMAGE_PADDING = "image.padding";

    //var imageUI = new Q.ImageUI();
    //var imageProperties = {};
    //for(var name in imageUI){
    //    if(name[0] == '_' || name.indexOf('$invalidate') == 0 || imageUI[name] instanceof Function){
    //        continue;
    //    }
    //    if(name[0] == '$'){
    //        name = name.substring(1);
    //    }
    //    imageProperties[name] = imageUI[name];
    //}
    //Q.log(JSON.stringify(imageProperties, null, '\t'));

    function mixStyles(styles) {
        if (!styles) {
            return defaultImageStyles;
        }
        var result = {};
        for (var name in defaultImageStyles) {
            result[name] = defaultImageStyles[name];
        }
        for (var name in styles) {
            var propertyName = nodeImageStyles[name];
            if (propertyName) {
                result[propertyName] = styles[name];
            }
        }
        return result;
    }

    var onGroupTitleClick = function (evt) {
        var parent = evt.target.parentNode;
        while (parent && !$(parent).hasClass('group')) {
            parent = parent.parentNode;
        }
        closeGroup(parent);
    }
    function closeGroup(parent, open){
        if (!parent) {
            return;
        }
        if(open === undefined){
            open = $(parent).hasClass('group--closed');
        }
        if (open) {
            $(parent).removeClass('group--closed');
        } else {
            $(parent).addClass('group--closed');
        }
    }

    function isImage(image) {
        return Q.isString(image) || image.draw instanceof Function;
    }

    var ToolBox = function (graph, html, groups) {
        this.graph = graph;
        this.html = html;
        this.init(groups);
    }
    ToolBox.prototype = {
        loadButton: null,
        imageWidth: 40,
        imageHeight: 40,
        loadImageBoxes: function (groups) {
            if (Q.isArray(groups)) {
                forEach(groups, function (group) {
                    this.loadImageBox(group);
                }, this);
                return;
            }
            this.loadImageBox(groups);
        },
        loadImageBox: function (json, insert) {
            if (Q.isString(json)) {
                json = JSON.parse(json);
            }
            if (insert) {
                var firstGroup = this.html.getElementsByClassName('group').item(0);
                if (firstGroup) {
                    this.html.insertBefore(this._createGroup(json, json.prefix), firstGroup);
                    return;
                }
            }
            return this.html.appendChild(this._createGroup(json, json.prefix));
        },
        //加载图元库文件
        loadImageBoxFile: function (files) {
            if (!files[0]) {
                return;
            }
            Q.readerSingleFile(files[0], 'json', function (json) {
                if (json) {
                    this.loadImageBox(json, true);
                }
            }.bind(this));
        },
        // 初始化拖拽节点列表
        hideButtonBar: function(show){
            this.buttonBar.style.display = show ? '' : 'none';
        },
        init: function (groups) {
            var toolbox = this.html, graph = this.graph;

            Q.appendClass(toolbox, 'graph-editor__toolbox');
            var buttonBar = this.buttonBar = createElement('graph-editor__toolbox-buttonBar', toolbox);
            var button = this.loadButton = Q.createButton({
                type: 'file',
                name: getI18NString('Load Images...'),
                iconClass: 'q-icon toolbar-add',
                action: this.loadImageBoxFile.bind(this)
            }, this);
            buttonBar.appendChild(button);
            this.hideButtonBar();

            // 常用节点
            var commonNodes = [];

            // 基本节点
            var basicNodes = [];

            // 其他节点
            var otherNodes = [];

            var stateMap = {
                "basicNode": {
                    "title": "基本节点",
                    "type": "common",
                    "init": {
                        "icon": "img/nodes/basicNode/init.png",
                        "menuList": []
                    },
                    "check": {
                        "icon": "img/nodes/basicNode/check.png",
                        "menuList": []
                    },
                    "checkError": {
                        "icon": "img/nodes/basicNode/checkAbnormal.png",
                        "menuList": []
                    },
                    "checkSuccess": {
                        "icon": "img/nodes/basicNode/checkSuccess.png",
                        "menuList": []
                    },
                    "waitExecute": {
                        "icon": "img/nodes/basicNode/init.png",
                        "menuList": []
                    },
                    "running": {
                        "icon": "img/nodes/basicNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "runSuccess": {
                        "icon": "img/nodes/basicNode/runSuccess.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "nodeOvertime": {
                        "icon": "img/nodes/basicNode/nodeOvertime.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    },
                    "error": {
                        "icon": "img/nodes/basicNode/nodeError.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    }
                },
                // "logicNode": {
                //     "type": "common",
                //     "title": "逻辑节点",
                //     "init": {
                //         "icon": "img/nodes/logicNode/init.png",
                //         "menuList": []
                //     },
                //     "waitExecute": {
                //         "icon": "img/nodes/logicNode/init.png",
                //         "menuList": []
                //     },
                //     "running": {
                //         "icon": "img/nodes/logicNode/running.png",
                //         "menuList": [
                //             {
                //                 "title": "重做本节点",
                //                 "action": "qunee.restartSelfNode",
                //                 "disabled": true
                //             },
                //             {
                //                 "title": "手工接管",
                //                 "action": "qunee.manualNozzleNode",
                //                 "disabled": true
                //             }
                //         ]
                //     },
                //     "runSuccess": {
                //         "icon": "img/nodes/logicNode/runSuccess.png",
                //         "menuList": [
                //             {
                //                 "title": "重做本节点",
                //                 "action": "qunee.restartSelfNode",
                //                 "disabled": true
                //             },
                //             {
                //                 "title": "手工接管",
                //                 "action": "qunee.manualNozzleNode",
                //                 "disabled": true
                //             }
                //         ]
                //     },
                //     "nodeOvertime": {
                //         "icon": "img/nodes/logicNode/nodeOvertime.png",
                //         "menuList": [
                //             {
                //                 "title": "重做本节点",
                //                 "action": "qunee.restartSelfNode",
                //                 "disabled": false
                //             },
                //             {
                //                 "title": "手工接管",
                //                 "action": "qunee.manualNozzleNode",
                //                 "disabled": false
                //             }
                //         ]
                //     },
                //     "error": {
                //         "icon": "img/nodes/logicNode/nodeError.png",
                //         "menuList": [
                //             {
                //                 "title": "重做本节点",
                //                 "action": "qunee.restartSelfNode",
                //                 "disabled": false
                //             },
                //             {
                //                 "title": "手工接管",
                //                 "action": "qunee.manualNozzleNode",
                //                 "disabled": false
                //             }
                //         ]
                //     }
                // },
                "userConfirmNode": {
                    "type": "basic",
                    "title": "用户确认节点",
                    "init": {
                        "icon": "img/nodes/userConfirmNode/init.png",
                        "menuList": []
                    },
                    "waitExecute": {
                        "icon": "img/nodes/userConfirmNode/init.png",
                        "menuList": []
                    },
                    "running": {
                        "icon": "img/nodes/userConfirmNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    },
                    "runSuccess": {
                        "icon": "img/nodes/userConfirmNode/runSuccess.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "nodeOvertime": {
                        "icon": "img/nodes/userConfirmNode/nodeOvertime.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    },
                    "error": {
                        "icon": "img/nodes/userConfirmNode/nodeError.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    },
                    "waitUserConfirm": {
                        "icon": "img/nodes/userConfirmNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    }
                },
                "groupNode": {
                    "type": "common",
                    "title": "引用流程",
                    "init": {
                        "icon": "img/nodes/groupNode/init.png",
                        "menuList": []
                    },
                    "waitExecute": {
                        "icon": "img/nodes/groupNode/init.png",
                        "menuList": []
                    },
                    "running": {
                        "icon": "img/nodes/groupNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "runSuccess": {
                        "icon": "img/nodes/groupNode/runSuccess.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "nodeOvertime": {
                        "icon": "img/nodes/groupNode/nodeOvertime.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    },
                    "error": {
                        "icon": "img/nodes/groupNode/nodeError.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    }
                },
                "startNode": {
                    "type": "other",
                    "title": "开始节点",
                    "init": {
                        "icon": "img/nodes/startNode/init.png",
                        "menuList": []
                    },
                    "running": {
                        "icon": "img/nodes/startNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "runSuccess": {
                        "icon": "img/nodes/startNode/runSuccess.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "nodeOvertime": {
                        "icon": "img/nodes/startNode/nodeOvertime.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "error": {
                        "icon": "img/nodes/startNode/nodeError.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    }
                },
                "endNode": {
                    "type": "other",
                    "title": "结束节点",
                    "init": {
                        "icon": "img/nodes/endNode/init.png",
                        "menuList": []
                    },
                    "waitExecute": {
                        "icon": "img/nodes/endNode/init.png",
                        "menuList": []
                    },
                    "running": {
                        "icon": "img/nodes/endNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "runSuccess": {
                        "icon": "img/nodes/endNode/runSuccess.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "nodeOvertime": {
                        "icon": "img/nodes/endNode/nodeOvertime.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "error": {
                        "icon": "img/nodes/endNode/nodeError.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    }
                },
                "closeNode": {
                    "type": "basic",
                    "title": "关机节点",
                    "init": {
                        "icon": "img/nodes/closeNode/init.png",
                        "menuList": []
                    },
                    "check": {
                        "icon": "img/nodes/closeNode/check.png",
                        "menuList": []
                    },
                    "checkError": {
                        "icon": "img/nodes/closeNode/checkAbnormal.png",
                        "menuList": []
                    },
                    "checkSuccess": {
                        "icon": "img/nodes/closeNode/checkSuccess.png",
                        "menuList": []
                    },
                    "waitExecute": {
                        "icon": "img/nodes/closeNode/init.png",
                        "menuList": []
                    },
                    "running": {
                        "icon": "img/nodes/closeNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "runSuccess": {
                        "icon": "img/nodes/closeNode/runSuccess.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "nodeOvertime": {
                        "icon": "img/nodes/closeNode/nodeOvertime.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    },
                    "error": {
                        "icon": "img/nodes/closeNode/nodeError.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    }
                },
                "restartNode": {
                    "type": "basic",
                    "title": "重启节点",
                    "init": {
                        "icon": "img/nodes/restartNode/init.png",
                        "menuList": []
                    },
                    "check": {
                        "icon": "img/nodes/restartNode/check.png",
                        "menuList": []
                    },
                    "checkError": {
                        "icon": "img/nodes/restartNode/checkAbnormal.png",
                        "menuList": []
                    },
                    "checkSuccess": {
                        "icon": "img/nodes/restartNode/checkSuccess.png",
                        "menuList": []
                    },
                    "waitExecute": {
                        "icon": "img/nodes/restartNode/init.png",
                        "menuList": []
                    },
                    "running": {
                        "icon": "img/nodes/restartNode/running.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "runSuccess": {
                        "icon": "img/nodes/restartNode/runSuccess.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": true
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": true
                            }
                        ]
                    },
                    "nodeOvertime": {
                        "icon": "img/nodes/restartNode/nodeOvertime.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    },
                    "error": {
                        "icon": "img/nodes/restartNode/nodeError.png",
                        "menuList": [
                            {
                                "title": "重做本节点",
                                "action": "qunee.restartSelfNode",
                                "disabled": false
                            },
                            {
                                "title": "手工接管",
                                "action": "qunee.manualNozzleNode",
                                "disabled": false
                            }
                        ]
                    }
                }
            };

            for (var key in stateMap) {
              var image = stateMap[key].init.icon;
              var label = key;
              var title = stateMap[key].title;
              var type = stateMap[key].type;

              switch (type) {
                case 'common':
                  commonNodes.push({
                    title: title,
                    label: title,
                    image: image
                  });
                  break;

                case 'basic':
                  basicNodes.push({
                    title: title,
                    label: title,
                    image: image
                  });
                  break;

                case 'other':
                  otherNodes.push({
                    title: title,
                    label: title,
                    image: image
                  });
                  break;
              }
            };

            console.log(commonNodes);
            console.log(basicNodes);
            console.log(otherNodes);

            var innerGroups = [
              {
                name: 'common.nodes',
                displayName: '常用节点',
                images: commonNodes
              },

              {
                name: 'basic.nodes',
                displayName: '基本节点',
                images: basicNodes
              },

              {
                name: 'other.nodes',
                displayName: '其他节点',
                images: otherNodes
              }
            ];

            this.loadImageBoxes(innerGroups);
            if (groups) {
                this.loadImageBoxes(groups);
            }
            Q.Shapes.getShape(Q.Consts.SHAPE_CIRCLE, 100, 100)
        },
        _index: 0,
        _getGroup: function(name){
            return this._groups[name];
            //return $(this.html).find('#' + name);
        },
        hideDefaultGroups: function(){
            this.hideGroup('basic.nodes');
            this.hideGroup('register.images');
            this.hideGroup('default.shapes');
        },
        hideGroup: function(name){
            var group = this._getGroup(name);
            if(group){
                group.style.display = 'none';
            }
        },
        showGroup: function(name){
            var group = this._getGroup(name);
            if(group){
                group.style.display = '';
            }
        },
        _groups: {},
        _createGroup: function (groupInfo) {
            var name = groupInfo.name;
            var root = groupInfo.root;
            var images = groupInfo.images;
            var close = groupInfo.close;
            var displayName = groupInfo.displayName || name;

            var group = createElement('group htrd-c-qunee-edit');
            group.id = name;
            this._groups[name] = group;

            var title = createElement('group__title', group);
            title.onclick = onGroupTitleClick;
            createElement(null, title, 'span', displayName);
            createElement('q-icon group-expand toolbar-expand', title, 'span');
            var items = createElement('group__items', group);
            var clearDiv = document.createElement('div');
            clearDiv.style.clear = 'both';
            group.appendChild(clearDiv);
            if(close){
                closeGroup(group);
            }

            if (!images) {
                return group;
            }

            //var images = [{
            //    type: '图元类型',
            //    label: '图元文本',
            //    image: '图元图片',
            //    imageName: '图片名称',
            //    styles: '图元样式',
            //    properties: '图元属性',
            //    clientProperties: '图元client属性',
            //    html: '拖拽html内容'
            //}, 'a.png', {draw: function(g){}}];
            //var group = {
            //    name: '分组名称',
            //    root: '根目录',
            //    imageWidth: '',
            //    imageHeight: '',
            //    size: 'q-icon size',
            //    images: images//'拖拽图片信息'
            //}

            var imageWidth = groupInfo.imageWidth || this.imageWidth;
            var imageHeight = groupInfo.imageHeight || this.imageHeight;

            forEach(images, function (imageInfo, name) {
                if (name == '_classPath' || name == '_className') {
                    return;
                }
                var image;
                if (isImage(imageInfo)) {
                    image = imageInfo;
                } else {
                    image = imageInfo.image;
                }
                var imageDiv, tooltip;
                var title;
                if (image) {
                    var imageName;
                    if (Q.isString(image)) {
                        imageName = image;
                        if (!Q.hasImage(image) && root) {
                            imageName = image = root + image;
                        }
                    } else {
                        imageName = imageInfo.imageName || imageInfo.name || name || 'drawable-' + this._index++;
                    }
                    if (!Q.hasImage(imageName)) {
                        Q.registerImage(imageName, image);
                    }
                    imageDiv = Q.createCanvas(imageWidth, imageHeight, true);
                    Q.drawImage(imageName, imageDiv, mixStyles(imageInfo.styles));
                    if (isImage(imageInfo)) {
                        imageInfo = {image: imageName};
                    } else {
                        imageInfo.image = imageName;
                    }
                    if(groupInfo.size){
                        if(!imageInfo.properties){
                            imageInfo.properties = {}
                        }
                        if(!imageInfo.properties.size){
                            imageInfo.properties.size = groupInfo.size;
                        }
                    }

                    tooltip = imageName;
                } else if (imageInfo.html) {
                    var imageDiv = document.createElement('div');
                    imageDiv.style.width = imageWidth + 'px';
                    imageDiv.style.height = imageHeight + 'px';
                    imageDiv.style.lineHeight = imageHeight + 'px';
                    imageDiv.style.overflow = 'hidden';
                    imageDiv.innerHTML = imageInfo.html;
                } else {
                    return;
                }
                title = imageInfo.title;
                tooltip = imageInfo.tooltip || imageInfo.label || tooltip || name;
                imageDiv.setAttribute('title', tooltip);
                var item = createElement('group__item group-item', items);
                var title = createElement('icon-title', null, 'div', title);
                Q.appendDNDInfo(imageDiv, imageInfo);
                item.appendChild(imageDiv);
                item.appendChild(title);
            }, this)
            return group;
        }
    }

    Q.ToolBox = ToolBox;

}(Q, jQuery)
