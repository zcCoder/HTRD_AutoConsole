/**
 * Created by Administrator on 2017/4/13.
 */
angular.module('HTRD_AutoConsole.A')
	.controller('HomeHandListCtrl', ['$scope', '$state', '$window','HomeServ','StatusMapServ','DateParseServ','$timeout',function($scope, $state, $window,HomeServ,StatusMapServ,DateParseServ,$timeout) {
		console.log('HomeHandListCtrl');

		//面包屑
	    	var breadcrumbsData = [
		      {
		        'iconClass': 'icon-home',
		        'text': '首页',
		        'link': 'console.base.home.index'
		      },
		      {
		        'text': '手工接管'
		      }
	    	];

		var homeHandListConfig = {
			limit: 20,
			page: 1
		};

		var toManagerProcess = function (item) {
			$state.go('console.base.manager.historyProcess',{
				id: item.id,
				processId: item.pipeline
			});
		};

		var getHomeHandList;
		//手工接管的排序
		$scope.handList = {};
		$scope.handList.endTimeSort = "sorting_desc";
		var changeHomeHandStartTimeSort = function (handList) {

			//清除其他header样式
			$scope.handList.endTimeSort = "";
			$scope.handList.processSort = "";
			$scope.handList.nodeSort = "";
			$scope.handList.executorSort = "";
			$scope.handList.terminatorSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.handList.startTimeSort){
				$scope.handList.startTimeSort = "sorting_asc";

				//配置传参升序排列
				$scope.homeHandListConfig.sort = "started_at";
				getHomeHandList()

			}else if($scope.handList.startTimeSort === 'sorting_asc'){

				$scope.handList.startTimeSort = "sorting_desc";
				$scope.homeHandListConfig.sort = "-started_at";
				getHomeHandList()

			}else if($scope.handList.startTimeSort === 'sorting_desc'){

				$scope.handList.startTimeSort = "sorting_asc";
				$scope.homeHandListConfig.sort = "started_at";
				getHomeHandList()

			}
		};
		var changeHomeHandEndTimeSort = function (handList) {

			$scope.handList.startTimeSort = "";
			$scope.handList.processSort = "";
			$scope.handList.nodeSort = "";
			$scope.handList.executorSort = "";
			$scope.handList.terminatorSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.handList.endTimeSort){
				$scope.handList.endTimeSort = "sorting_asc" ;
				$scope.homeHandListConfig.sort = "ended_at";
				getHomeHandList()
			}else if($scope.handList.endTimeSort === 'sorting_asc'){
				$scope.handList.endTimeSort = "sorting_desc";
				$scope.homeHandListConfig.sort = "-ended_at";
				getHomeHandList()
			}else if($scope.handList.endTimeSort === 'sorting_desc'){
				$scope.handList.endTimeSort= "sorting_asc";
				$scope.homeHandListConfig.sort = "ended_at";
				getHomeHandList()
			}
		};
		var changeHomeHandProcessSort = function (handList) {

			//清除其他header样式
			$scope.handList.startTimeSort = "";
			$scope.handList.endTimeSort = "";
			$scope.handList.nodeSort = "";
			$scope.handList.executorSort = "";
			$scope.handList.terminatorSort = "";

			if(!!!$scope.handList.processSort){
				$scope.handList.processSort = "sorting_asc" ;
				$scope.homeHandListConfig.sort = "id";
				getHomeHandList()
			}else if($scope.handList.processSort === 'sorting_asc'){
				$scope.handList.processSort = "sorting_desc";
				$scope.homeHandListConfig.sort = "-id";
				getHomeHandList()
			}else if($scope.handList.processSort === 'sorting_desc'){
				$scope.handList.processSort = "sorting_asc";
				$scope.homeHandListConfig.sort = "id";
				getHomeHandList()
			}
		};
		var changeHomeHandNodeSort = function (handList) {
			//清除其他header样式
			$scope.handList.startTimeSort = "";
			$scope.handList.endTimeSort = "";
			$scope.handList.processSort = "";
			$scope.handList.executorSort = "";
			$scope.handList.terminatorSort = "";

			if(!!!$scope.handList.nodeSort){
				$scope.handList.nodeSort = "sorting_asc" ;
				$scope.homeHandListConfig.sort = "id";
				getHomeHandList()
			}else if($scope.handList.nodeSort === 'sorting_asc'){
				$scope.handList.nodeSort = "sorting_desc";
				$scope.homeHandListConfig.sort = "-id";
				getHomeHandList()
			}else if($scope.handList.nodeSort === 'sorting_desc'){
				$scope.handList.nodeSort = "sorting_asc";
				$scope.homeHandListConfig.sort = "id";
				getHomeHandList()
			}
		};
		var changeHomeHandExecutorSort = function (handList) {

			$scope.handList.startTimeSort = "";
			$scope.handList.endTimeSort = "";
			$scope.handList.processSort = "";
			$scope.handList.nodeSort = "";
			$scope.handList.terminatorSort = "";

			if(!!!$scope.handList.executorSort){
				$scope.handList.executorSort = "sorting_asc" ;
				$scope.homeHandListConfig.sort = "id";
				getHomeHandList()
			}else if($scope.handList.executorSort === 'sorting_asc'){
				$scope.handList.executorSort = "sorting_desc";
				$scope.homeHandListConfig.sort = "id";
				getHomeHandList()
			}else if($scope.handList.executorSort === 'sorting_desc'){
				$scope.handList.executorSort = "sorting_asc";
				$scope.homeHandListConfig.sort = "id";
				getHomeHandList()
			}
		};

		var bindListener = function() {

			$scope.changeHomeHandStartTimeSort = changeHomeHandStartTimeSort;
			$scope.changeHomeHandEndTimeSort = changeHomeHandEndTimeSort;
			//$scope.changeHomeHandProcessSort = changeHomeHandProcessSort;
			//$scope.changeHomeHandNodeSort = changeHomeHandNodeSort;
			//$scope.changeHomeHandExecutorSort = changeHomeHandExecutorSort;

			$scope.toManagerProcess = toManagerProcess
		};

		var initPlugins = function() {
			$scope.breadcrumbsData = breadcrumbsData;
			//执行(分页)的配置
			$scope.homeHandListConfig = homeHandListConfig;

			getHomeHandList = function () {
				HomeServ.getHandOver(homeHandListConfig)
					.then(function(data) {
						if(data.status === 200){
							$scope.handOver = data;
							$scope.handOver.result = $scope.handOver.results.data;
							$scope.handOver.result.map(function(item){

								var started_at = DateParseServ.dateParse(item.started_at);
								var ended_at = DateParseServ.dateParse(item.ended_at)
								var model = StatusMapServ.getStatusData(item.result_status);
								item.result_status = model.text;
								item.started_at = started_at;
								item.ended_at = ended_at;

							})
							$scope.$broadcast("toList",{
								handOver: data.results.data,
								count: data.results.count,
								limit:  $scope.homeHandListConfig.limit,
								page: $scope.homeHandListConfig.page || data.results.page
							});
						}
					});
			};
			$timeout(function () {
				getHomeHandList()
			},1000);
			$scope.$on('toPage',function (evt,data) {
				$scope.homeHandListConfig.page = data.page;
				getHomeHandList()
			});
		};

		var init = function() {
			bindListener();
			initPlugins()
		};

		init();
	}]);
