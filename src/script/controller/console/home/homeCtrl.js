angular.module('HTRD_AutoConsole.A').controller('HomeCtrl', ['$scope', '$state', '$window', 'HomeServ',function($scope, $state, $window,HomeServ) {
  console.log('HomeCtrl');

      var homeConfig = {
            limit: 5
      };

      var bindListener = function() {

      };

      var initPlugins = function() {

            $scope.homeConfig = homeConfig;

            $scope.waitRun = {
              count : 0
            }

            // 今日待执行
           /* HomeServ.getWaitRun()
            .then(function(result) {
            if (result.status === 200) {
                    $scope.waitRun = result.results
                  };
            });*/


            //执行中
            HomeServ.getRuning(homeConfig)
              .then(function(result) {
                if(result.status === 200){
                  $scope.runing = result.results;
                }
              });

            //执行告警
            HomeServ.getWarnRun(homeConfig)
              .then(function(result) {
                if(result.status === 200){
                  $scope.warnRun = result.results
                }
              });


            //已结束
            HomeServ.getEndRun(homeConfig)
            .then(function(result) {
              if(result.status === 200){
                $scope.endRun = result.results;
              }
            });

            //执行成功
            HomeServ.getSuccessRun(homeConfig)
            .then(function(result) {
              if(result.status === 200){
                $scope.successRun = result.results;
              }
            });

            //手工接管
            HomeServ.getHandOver(homeConfig)
            .then(function(result) {
              if (result.status === 200) {
                $scope.handOver = result.results;
              }
            });
        };

      var init = function() {
        bindListener();
        initPlugins()
      };

      init();
}]);
