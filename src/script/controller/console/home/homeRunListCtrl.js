/**
 * Created by Administrator on 2017/4/12.
 */
angular.module('HTRD_AutoConsole.A')
	.controller('HomeRunListCtrl', ['$scope', '$state', '$window','HomeServ','StatusMapServ','DateParseServ','$timeout',function($scope, $state, $window,HomeServ,StatusMapServ,DateParseServ,$timeout) {
		console.log('HomeRunListCtrl');

		//面包屑
	    	var breadcrumbsData = [
		      {
		        'iconClass': 'icon-home',
		        'text': '首页',
		        'link': 'console.base.home.index'
		      },
		      {
		        'text': '执行中'
		      }
	    	];

		var homeRunListConfig = {
			page: 1,
			limit: 20
		};
		
		var toManagerProcess = function (item) {
			$state.go("console.base.manager.process",{id: item.pipeline});
		};

		var getHomeRuningList;
		//执行中的排序
		$scope.runingList = {};
		$scope.runingList.timeSort = "sorting_desc";
		var changeHomeRuningProcessSort = function (runingList) {

			//清除其他header样式
			$scope.runingList.nodeSort = "";
			$scope.runingList.createrSort = "";
			$scope.runingList.modifierSort = "";
			$scope.runingList.auditorSort = "";
			$scope.runingList.timeSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.runingList.processSort){
				$scope.runingList.processSort = "sorting_asc";

				//配置传参升序排列
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()

			}else if($scope.runingList.processSort === 'sorting_asc'){

				$scope.runingList.processSort = "sorting_desc";
				$scope.homeRunListConfig.sort = "-id";
				getHomeRuningList()

			}else if($scope.runingList.processSort === 'sorting_desc'){

				$scope.runingList.processSort = "sorting_asc";
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()

			}
		};
		var changeHomeRuningNodeSort = function (runingList) {

			$scope.runingList.processSort = "";
			$scope.runingList.createrSort = "";
			$scope.runingList.modifierSort = "";
			$scope.runingList.auditorSort = "";
			$scope.runingList.timeSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.runingList.nodeSort){
				$scope.runingList.nodeSort = "sorting_asc" ;
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}else if($scope.runingList.nodeSort === 'sorting_asc'){
				$scope.runingList.nodeSort = "sorting_desc";
				$scope.homeRunListConfig.sort = "-id";
				getHomeRuningList()
			}else if($scope.runingList.nodeSort === 'sorting_desc'){
				$scope.runingList.nodeSort = "sorting_asc";
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}
		};
		var changeHomeRuningCreaterSort = function (runingList) {

			//清除其他header样式
			$scope.runingList.processSort = "";
			$scope.runingList.nodeSort = "";
			$scope.runingList.modifierSort = "";
			$scope.runingList.auditorSort = "";
			$scope.runingList.timeSort = "";

			if(!!!$scope.runingList.createrSort){
				$scope.runingList.createrSort = "sorting_asc" ;
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList(homeRunListConfig)
			}else if($scope.runingList.createrSort === 'sorting_asc'){
				$scope.runingList.createrSort = "sorting_desc";
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList(homeRunListConfig)
			}else if($scope.runingList.createrSort === 'sorting_desc'){
				$scope.runingList.createrSort = "sorting_asc";
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}
		};
		var changeHomeRuningModifierSort = function (runingList) {

			$scope.runingList.processSort = "";
			$scope.runingList.nodeSort = "";
			$scope.runingList.createrSort = "";
			$scope.runingList.auditorSort = "";
			$scope.runingList.timeSort = "";

			if(!!!$scope.runingList.modifierSort){
				$scope.runingList.modifierSort = "sorting_asc" ;
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}else if($scope.runingList.modifierSort === 'sorting_asc'){
				$scope.runingList.modifierSort = "sorting_desc";
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}else if($scope.runingList.modifierSort === 'sorting_desc'){
				$scope.runingList.modifierSort = "sorting_asc";
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}
		};
		var changeHomeRuningAuditorSort = function (runingList) {
			//清除其他header样式
			$scope.runingList.processSort = "";
			$scope.runingList.nodeSort = "";
			$scope.runingList.createrSort = "";
			$scope.runingList.modifierSort = "";
			$scope.runingList.timeSort = "";

			if(!!!$scope.runingList.auditorSort){
				$scope.runingList.auditorSort = "sorting_asc" ;
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}else if($scope.runingList.auditorSort === 'sorting_asc'){
				$scope.runingList.auditorSort = "sorting_desc";
				$scope.homeRunListConfig.sort = "-id";
				getHomeRuningList()
			}else if($scope.runingList.auditorSort === 'sorting_desc'){
				$scope.runingList.auditorSort = "sorting_asc";
				$scope.homeRunListConfig.sort = "id";
				getHomeRuningList()
			}
		};
		var changeHomeRuningTimeSort = function (runingList) {
			//清除其他header样式
			$scope.runingList.processSort = "";
			$scope.runingList.nodeSort = "";
			$scope.runingList.createrSort = "";
			$scope.runingList.modifierSort = "";
			$scope.runingList.auditorSort = "";

			if(!!!$scope.runingList.timeSort){
				$scope.runingList.timeSort = "sorting_asc" ;
				$scope.homeRunListConfig.sort = "started_at";
				getHomeRuningList()
			}else if($scope.runingList.timeSort === 'sorting_asc'){
				$scope.runingList.timeSort = "sorting_desc";
				$scope.homeRunListConfig.sort = "-started_at";
				getHomeRuningList()
			}else if($scope.runingList.timeSort === 'sorting_desc'){
				$scope.runingList.timeSort = "sorting_asc";
				$scope.homeRunListConfig.sort = "started_at";
				getHomeRuningList()
			}
		};

		var bindListener = function() {

			//$scope.changeHomeRuningProcessSort = changeHomeRuningProcessSort;
			//$scope.changeHomeRuningNodeSort = changeHomeRuningNodeSort;
			//$scope.changeHomeRuningCreaterSort = changeHomeRuningCreaterSort;
			//$scope.changeHomeRuningModifierSort = changeHomeRuningModifierSort;
			//$scope.changeHomeRuningAuditorSort = changeHomeRuningAuditorSort;
			$scope.changeHomeRuningTimeSort = changeHomeRuningTimeSort;

			$scope.toManagerProcess = toManagerProcess
		};

		var initPlugins = function() {
			$scope.breadcrumbsData = breadcrumbsData;
			//执行(分页)的配置
			$scope.homeRunListConfig = homeRunListConfig;

			///执行中
			getHomeRuningList = function () {
				console.log(homeRunListConfig)
				HomeServ.getRuning(homeRunListConfig)
					.then(function(data) {
						if(data.status === 200){
							$scope.runing = data;
							$scope.runing.result = $scope.runing.results.data;
							$scope.runing.result.map(function(item){
								var model = StatusMapServ.getStatusData(item.result_status);
								var started_at = DateParseServ.dateParse(item.started_at)
								item.result_status = model.text.substring(0,4);
								item.started_at = started_at;
							})
							$scope.$broadcast("toList",{
								runing: data.results.data,
								count: data.results.count,
								limit:  $scope.homeRunListConfig.limit,
								page: $scope.homeRunListConfig.page || data.results.page
							})
						}
					});
			};
			$timeout(function () {
				getHomeRuningList()
			},1000);
			$scope.$on('toPage',function (evt,data) {
				$scope.homeRunListConfig.page = data.page;
				getHomeRuningList()
			});
		};

		var init = function() {
			bindListener();
			initPlugins()
		};

		init();
	}]);
