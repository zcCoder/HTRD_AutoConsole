/**
 * Created by Administrator on 2017/4/12.
 */
angular.module('HTRD_AutoConsole.A')
	.controller('HomeWaitListCtrl', ['$scope', '$state', '$window','HomeServ','$timeout',function($scope, $state, $window,HomeServ,$timeout) {
		console.log('HomeWaitListCtrl');

		//初始化
		var getHomeWaitList;
		
		//今日待执行的排序
		//$scope.waitList = {};

		//今日执行获取更多数据的配置
		var homeWaitListConfig = {
			page: 1,
			limit: 20
		};

		var toManagerProcess = function (item) {
			$state.go("console.base.manager.process",{id: item.pipeline});
		};
		
		//$scope.waitList.processSort = "sorting_desc";
		/*var changeHomeWaitProcessSort = function (waitList) {

			//清除其他header样式
			$scope.waitList.nodeSort = "";
			$scope.waitList.createrSort = "";
			$scope.waitList.modifierSort = "";
			$scope.waitList.auditorSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.waitList.processSort){

				$scope.waitList.processSort = "sorting_asc";
				//配置传参升序排列
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()

			}else if($scope.waitList.processSort === 'sorting_asc'){

				$scope.waitList.processSort = "sorting_desc";
				$scope.homeWaitListConfig.sort = "-id";
				getHomeWaitList()

			}else if($scope.waitList.processSort === 'sorting_desc'){

				$scope.waitList.processSort = "sorting_asc";
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}
		};
		var changeHomeWaitNodeSort = function (waitList) {

			$scope.waitList.processSort = "";
			$scope.waitList.createrSort = "";
			$scope.waitList.modifierSort = "";
			$scope.waitList.auditorSort = "";

			if(!!!$scope.waitList.nodeSort){
				//默认为空 点击按top排序
				$scope.waitList.nodeSort = "sorting_asc" ;
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}else if($scope.waitList.nodeSort === 'sorting_asc'){
				$scope.waitList.nodeSort = "sorting_desc";
				$scope.homeWaitListConfig.sort = "-id";
				getHomeWaitList()
			}else if($scope.waitList.nodeSort === 'sorting_desc'){
				$scope.waitList.nodeSort = "sorting_asc";
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}
		};
		var changeHomeWaitCreaterSort = function (waitList) {

			$scope.waitList.processSort = "";
			$scope.waitList.nodeSort = "";
			$scope.waitList.modifierSort = "";
			$scope.waitList.auditorSort = "";

			if(!!!$scope.waitList.createrSort){
				$scope.waitList.createrSort = "sorting_asc" ;
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}else if($scope.waitList.createrSort === 'sorting_asc'){
				$scope.waitList.createrSort = "sorting_desc";
				$scope.homeWaitListConfig.sort = "-id";
				getHomeWaitList()
			}else if($scope.waitList.createrSort === 'sorting_desc'){
				$scope.waitList.createrSort = "sorting_asc";
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}
		};
		var changeHomeWaitModifierSort = function (waitList) {

			$scope.waitList.processSort = "";
			$scope.waitList.nodeSort = "";
			$scope.waitList.createrSort = "";
			$scope.waitList.auditorSort = "";

			if(!!!$scope.waitList.modifierSort){
				$scope.waitList.modifierSort = "sorting_asc" ;
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}else if($scope.waitList.modifierSort === 'sorting_asc'){
				$scope.waitList.modifierSort = "sorting_desc";
				$scope.homeWaitListConfig.sort = "-id";
				getHomeWaitList()
			}else if($scope.waitList.modifierSort === 'sorting_desc'){
				$scope.waitList.modifierSort = "sorting_asc";
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}
		};
		var changeHomeWaitAuditorSort = function (waitList)  {
			//清除其他header样式
			$scope.waitList.processSort = "";
			$scope.waitList.nodeSort = "";
			$scope.waitList.createrSort = "";
			$scope.waitList.modifierSort = "";

			if(!!!$scope.waitList.auditorSort){
				$scope.waitList.auditorSort = "sorting_asc" ;
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}else if($scope.waitList.auditorSort === 'sorting_asc'){
				$scope.waitList.auditorSort = "sorting_desc";
				$scope.homeWaitListConfig.sort = "-id";
				getHomeWaitList()
			}else if($scope.waitList.auditorSort === 'sorting_desc'){
				$scope.waitList.auditorSort = "sorting_asc";
				$scope.homeWaitListConfig.sort = "id";
				getHomeWaitList()
			}
		};*/

		var bindListener = function() {

			//流程列表排序/待执行
			/*$scope.changeHomeWaitProcessSort = changeHomeWaitProcessSort;
			$scope.changeHomeWaitNodeSort = changeHomeWaitNodeSort;
			$scope.changeHomeWaitCreaterSort = changeHomeWaitCreaterSort;
			$scope.changeHomeWaitModifierSort = changeHomeWaitModifierSort;
			$scope.changeHomeWaitAuditorSort = changeHomeWaitAuditorSort;*/

			$scope.toManagerProcess = toManagerProcess
		};

		var initPlugins = function() {

			//执行(分页)的配置
			$scope.homeWaitListConfig = homeWaitListConfig;

			//今日待执行
			getHomeWaitList = function () {
				HomeServ.getWaitRun(homeWaitListConfig)
					.then(function(data) {
						if(data.status === 200){
							$scope.waitRun = data;
							$scope.waitRun.result = $scope.waitRun.results.data;
							$scope.$broadcast("toList",{
								waitRun: data.results.data,
								count: data.results.count,
								limit: $scope.homeWaitListConfig.limit,
								page: $scope.homeWaitListConfig.page || data.results.page
							});
						}
					});
			};
			$timeout(function () {
				getHomeWaitList()
			},1000);
			//page指令接受到的数据
			$scope.$on('toPage',function (evt,data) {
				$scope.homeWaitListConfig.page = data.page;
				getHomeWaitList()
			});
		};

		var init = function() {
			bindListener();
			initPlugins()
		};

		init();
	}]);
