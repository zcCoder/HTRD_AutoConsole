/**
 * Created by Administrator on 2017/4/13.
 */

angular.module('HTRD_AutoConsole.A')
	.controller('HomeEndCtrl', ['$scope', '$state', '$window','HomeServ','StatusMapServ','DateParseServ','$timeout',function($scope, $state, $window,HomeServ,StatusMapServ,DateParseServ,$timeout) {
		console.log('HomeEndCtrl');
		//面包屑
	    var breadcrumbsData = [
		      {
		        'iconClass': 'icon-home',
		        'text': '首页',
		        'link': 'console.base.home.index'
		      },
		      {
		        'text': '已结束'
		      }
	    	];

		var homeEndListConfig = {
			page: 1,
			limit: 20
		};

		var toManagerProcess = function (item) {
			$state.go('console.base.manager.historyProcess',{
				id: item.id,
				processId: item.pipeline
			});
		};

		var getHomeEndList;
		//已结束的排序
		$scope.endList = {};
		$scope.endList.endTimeSort = "sorting_desc";
		var changeHomeEndStartTimeSort = function (endList) {

			//清除其他header样式
			$scope.endList.endTimeSort = "";
			$scope.endList.processSort = "";
			$scope.endList.nodeSort = "";
			$scope.endList.executorSort = "";
			$scope.endList.terminatorSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.endList.startTimeSort){

				$scope.endList.startTimeSort = "sorting_asc";
				//配置传参升序排列
				$scope.homeEndListConfig.sort = "started_at";
				getHomeEndList()

			}else if($scope.endList.startTimeSort === 'sorting_asc'){

				$scope.endList.startTimeSort = "sorting_desc";
				$scope.homeEndListConfig.sort = "-started_at";
				getHomeEndList()

			}else if($scope.endList.startTimeSort === 'sorting_desc'){

				$scope.endList.startTimeSort = "sorting_asc";

				$scope.homeEndListConfig.sort = "started_at";
				getHomeEndList()

			}
		};
		var changeHomeEndEndTimeSort = function (endList) {

			//清除其他header样式
			$scope.endList.startTimeSort = "";
			$scope.endList.processSort = "";
			$scope.endList.nodeSort = "";
			$scope.endList.executorSort = "";
			$scope.endList.terminatorSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.endList.endTimeSort){
				$scope.endList.endTimeSort = "sorting_asc" ;
				$scope.homeEndListConfig.sort = "started_at";
				getHomeEndList()
			}else if($scope.endList.endTimeSort === 'sorting_asc'){
				$scope.endList.endTimeSort = "sorting_desc";
				$scope.homeEndListConfig.sort = "-started_at";
				getHomeEndList()
			}else if($scope.endList.endTimeSort === 'sorting_desc'){
				$scope.endList.endTimeSort= "sorting_asc";
				$scope.homeEndListConfig.sort = "started_at";
				getHomeEndList()
			}
		};
		var changeHomeEndProcessSort = function (endList) {

			//清除其他header样式
			$scope.endList.startTimeSort = "";
			$scope.endList.endTimeSort = "";
			$scope.endList.nodeSort = "";
			$scope.endList.executorSort = "";
			$scope.endList.terminatorSort = "";

			if(!!!$scope.endList.processSort){
				$scope.endList.processSort = "sorting_asc" ;
				$scope.homeEndListConfig.sort = "id";
				getHomeEndList()
			}else if($scope.endList.processSort === 'sorting_asc'){
				$scope.endList.processSort = "sorting_desc";
				$scope.homeEndListConfig.sort = "-id";
				getHomeEndList()
			}else if($scope.endList.processSort === 'sorting_desc'){
				$scope.endList.processSort = "sorting_asc";
				$scope.homeEndListConfig.sort = "id";
				getHomeEndList()
			}
		};
		var changeHomeEndNodeSort = function (endList) {
			//清除其他header样式
			$scope.endList.startTimeSort = "";
			$scope.endList.endTimeSort = "";
			$scope.endList.processSort = "";
			$scope.endList.executorSort = "";
			$scope.endList.terminatorSort = "";

			if(!!!$scope.endList.nodeSort){
				$scope.endList.nodeSort = "sorting_asc" ;
				$scope.homeEndListConfig.sort = "id";
				getHomeEndList()
			}else if($scope.endList.nodeSort === 'sorting_asc'){
				$scope.endList.nodeSort = "sorting_desc";
				$scope.homeEndListConfig.sort = "-id";
				getHomeEndList()
			}else if($scope.endList.nodeSort === 'sorting_desc'){
				$scope.endList.nodeSort = "sorting_asc";
				$scope.homeEndListConfig.sort = "id";
				getHomeEndList()
			}
		};
		var changeHomeEndExecutorSort = function (endList) {
			//清除其他header样式
			$scope.endList.startTimeSort = "";
			$scope.endList.endTimeSort = "";
			$scope.endList.processSort = "";
			$scope.endList.nodeSort = "";
			$scope.endList.terminatorSort = "";

			if(!!!$scope.endList.executorSort){
				$scope.endList.executorSort = "sorting_asc" ;
				$scope.homeEndListConfig.sort = "id";
				getHomeEndList()
			}else if($scope.endList.executorSort === 'sorting_asc'){
				$scope.endList.executorSort = "sorting_desc";
				$scope.homeEndListConfig.sort = "-id";
				getHomeEndList()
			}else if($scope.endList.executorSort === 'sorting_desc'){
				$scope.endList.executorSort = "sorting_asc";
				$scope.homeEndListConfig.sort = "id";
				getHomeEndList()
			}
		};

		var bindListener = function() {

			$scope.changeHomeEndStartTimeSort = changeHomeEndStartTimeSort;
			$scope.changeHomeEndEndTimeSort = changeHomeEndEndTimeSort;
			//$scope.changeHomeEndProcessSort = changeHomeEndProcessSort;
			//$scope.changeHomeEndNodeSort = changeHomeEndNodeSort;
			//$scope.changeHomeEndExecutorSort = changeHomeEndExecutorSort;

			$scope.toManagerProcess = toManagerProcess;
		};

		var initPlugins = function() {

			$scope.breadcrumbsData = breadcrumbsData;

			//执行(分页)的配置
			$scope.homeEndListConfig = homeEndListConfig;
			//已结束
			getHomeEndList = function () {
				HomeServ.getEndRun(homeEndListConfig)
					.then(function(data) {
						if(data.status === 200){
							$scope.endRun = data;
							$scope.endRun.result = $scope.endRun.results.data;
							$scope.endRun.result.map(function(item){
								var started_at = DateParseServ.dateParse(item.started_at);
								var ended_at = DateParseServ.dateParse(item.ended_at)
								var model = StatusMapServ.getStatusData(item.result_status);
								item.result_status = model.text.substring(0,4);
								item.started_at = started_at;
								item.ended_at = ended_at;
							})
							$scope.$broadcast("toList",{
								endRun: data.results.data,
								count: data.results.count,
								limit:  $scope.homeEndListConfig.limit,
								page: $scope.homeEndListConfig.page || data.results.page
							})
						}
					});
			};
			$timeout(function () {
				getHomeEndList()
			},1000);
				$scope.$on('toPage',function (evt,data) {
				$scope.homeEndListConfig.page = data.page;
				getHomeEndList()
			});
		};

		var init = function() {
			bindListener();
			initPlugins()
		};

		init();
	}]);
