/**
 * Created by Administrator on 2017/4/12.
 */
angular.module('HTRD_AutoConsole.A')
	.controller('HomeSuccessListCtrl', ['$scope', '$state', '$window','HomeServ','StatusMapServ','DateParseServ','$timeout',function($scope, $state, $window,HomeServ,StatusMapServ,DateParseServ,$timeout) {
		console.log('HomeSuccessListCtrl');

		//面包屑
	    	var breadcrumbsData = [
		      {
		        'iconClass': 'icon-home',
		        'text': '首页',
		        'link': 'console.base.home.index'
		      },
		      {
		        'text': '执行成功'
		      }
	    	];

		var homeSuccessListConfig = {
			limit: 20,
			page: 1
		};

		var toManagerProcess = function (item) {
			$state.go('console.base.manager.historyProcess',{
				id: item.id,
				processId: item.pipeline
			});
		};

		var getSuccessList;
		//执行成功的排序
		$scope.successList = {};
		$scope.successList.endTimeSort = "sorting_desc";
		var changeHomesuccessListStartTimeSort = function (successList) {

			//清除其他header样式
			$scope.successList.endTimeSort = "";
			$scope.successList.processSort = "";
			$scope.successList.nodeSort = "";
			$scope.successList.executorSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.successList.startTimeSort){
				$scope.successList.startTimeSort = "sorting_asc";
				$scope.homeSuccessListConfig.sort = "started_at";
				getSuccessList()

			}else if($scope.successList.startTimeSort === 'sorting_asc'){

				$scope.successList.startTimeSort = "sorting_desc";
				$scope.homeSuccessListConfig.sort = "-started_at";
				getSuccessList()

			}else if($scope.successList.startTimeSort === 'sorting_desc'){

				$scope.successList.startTimeSort = "sorting_asc";
				$scope.homeSuccessListConfig.sort = "started_at";
				getSuccessList()

			}
		};
		var changeHomesuccessListEndTimeSort = function (successList) {

			//清除其他header样式
			$scope.successList.startTimeSort = "";
			$scope.successList.processSort = "";
			$scope.successList.nodeSort = "";
			$scope.successList.executorSort = "";
			$scope.successList.terminatorSort = "";

			//默认为空 点击按top排序
			if(!!!$scope.successList.endTimeSort){
				$scope.successList.endTimeSort = "sorting_asc" ;
				$scope.homeSuccessListConfig.sort = "ended_at";
				getSuccessList()
			}else if($scope.successList.endTimeSort === 'sorting_asc'){
				$scope.successList.endTimeSort = "sorting_desc";
				$scope.homeSuccessListConfig.sort = "-ended_at";
				getSuccessList()
			}else if($scope.successList.endTimeSort === 'sorting_desc'){
				$scope.successList.endTimeSort= "sorting_asc";
				$scope.homeSuccessListConfig.sort = "ended_at";
				getSuccessList()
			}
		};
		var changeHomesuccessListProcessSort = function (successList) {

			//清除其他header样式
			$scope.successList.startTimeSort = "";
			$scope.successList.endTimeSort = "";
			$scope.successList.nodeSort = "";
			$scope.successList.executorSort = "";
			$scope.successList.terminatorSort = "";

			if(!!!$scope.successList.processSort){
				$scope.successList.processSort = "sorting_asc" ;
				$scope.homeSuccessListConfig.sort = "id";
				getSuccessList()
			}else if($scope.successList.processSort === 'sorting_asc'){
				$scope.successList.processSort = "sorting_desc";
				$scope.homeSuccessListConfig.sort = "-id";
				getSuccessList()
			}else if($scope.successList.processSort === 'sorting_desc'){
				$scope.successList.processSort = "sorting_asc";
				$scope.homeSuccessListConfig.sort = "id";
				getSuccessList()
			}
		};
		var changeHomesuccessListNodeSort = function (successList) {
			//清除其他header样式
			$scope.successList.startTimeSort = "";
			$scope.successList.endTimeSort = "";
			$scope.successList.processSort = "";
			$scope.successList.executorSort = "";
			$scope.successList.terminatorSort = "";

			if(!!!$scope.successList.nodeSort){
				$scope.successList.nodeSort = "sorting_asc" ;
				$scope.homeSuccessListConfig.sort = "id";
				getSuccessList()
			}else if($scope.successList.nodeSort === 'sorting_asc'){
				$scope.successList.nodeSort = "sorting_desc";
				$scope.homeSuccessListConfig.sort = "-id";
				getSuccessList()
			}else if($scope.successList.nodeSort === 'sorting_desc'){
				$scope.successList.nodeSort = "sorting_asc";
				$scope.homeSuccessListConfig.sort = "id";
				getSuccessList()
			}
		};
		var changeHomesuccessListExecutorSort = function (successList) {
			//清除其他header样式
			$scope.successList.startTimeSort = "";
			$scope.successList.endTimeSort = "";
			$scope.successList.processSort = "";
			$scope.successList.nodeSort = "";
			$scope.successList.terminatorSort = "";

			if(!!!$scope.successList.executorSort){
				$scope.successList.executorSort = "sorting_asc" ;
				$scope.homeSuccessListConfig.sort = "id";
				getSuccessList()
			}else if($scope.successList.executorSort === 'sorting_asc'){
				$scope.successList.executorSort = "sorting_desc";
				$scope.homeSuccessListConfig.sort = "-id";
				getSuccessList()
			}else if($scope.successList.executorSort === 'sorting_desc'){
				$scope.successList.executorSort = "sorting_asc";
				$scope.homeSuccessListConfig.sort = "id";
				getSuccessList()
			}
		};

		var bindListener = function() {

			$scope.changeHomesuccessListStartTimeSort = changeHomesuccessListStartTimeSort;
			$scope.changeHomesuccessListEndTimeSort = changeHomesuccessListEndTimeSort;
			//$scope.changeHomesuccessListProcessSort = changeHomesuccessListProcessSort;
			//$scope.changeHomesuccessListNodeSort = changeHomesuccessListNodeSort;
			//$scope.changeHomesuccessListExecutorSort = changeHomesuccessListExecutorSort;
			$scope.toManagerProcess = toManagerProcess
		};

		var initPlugins = function() {
			$scope.breadcrumbsData = breadcrumbsData;
			//执行(分页)的配置
			$scope.homeSuccessListConfig = homeSuccessListConfig;

			getSuccessList = function () {
				
				HomeServ.getSuccessRun(homeSuccessListConfig)
					.then(function(data) {
						if(data.status === 200){
							$scope.successRun = data;
							$scope.successRun.result = $scope.successRun.results.data;
							$scope.successRun.result.map(function(item){

								var started_at = DateParseServ.dateParse(item.started_at);
								var ended_at = DateParseServ.dateParse(item.ended_at)
								var model = StatusMapServ.getStatusData(item.result_status);
								item.started_at = started_at;
								item.ended_at = ended_at;
								item.result_status = model.text;
							})
							$scope.$broadcast("toList",{
								successRun: data.results.data,
								count: data.results.count,
								limit:  $scope.homeSuccessListConfig.limit,
								page: $scope.homeSuccessListConfig.page || data.results.page
							})
						}
					});
			};
			$timeout(function () {
				getSuccessList()
			},1000);
			$scope.$on('toPage',function (evt,data) {
				$scope.homeSuccessListConfig.page = data.page;
				getSuccessList()
			});
		};

		var init = function() {
			bindListener();
			initPlugins()
		};

		init();
	}]);
