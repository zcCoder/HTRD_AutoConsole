angular.module('HTRD_AutoConsole.A')
.controller('HeaderCtrl', ['$scope', '$state', '$window','MessagePanelServ','NotificationServ','SocketServ', 'DialogServ','$timeout',
  function($scope, $state,$window,MessagePanelServ,NotificationServ,SocketServ,DialogServ,$timeout) {

  //消息中心
    var getMessage = function(){
      MessagePanelServ.getManagerInfo()
      .then(function (data) {

          if(data.status === 200){

              $scope.messageData =  data.results;
              $scope.messageCount = $scope.messageData.length;

              // 消息count显示与隐藏
              if ($scope.messageCount === 0) {
                $scope.isShowMessageCount = false;
              } else if($scope.messageCount > 99){
                $scope.messageCount = 99+"+";
              }

              // 數量
              $scope.$broadcast("messageCount", {
                messageCount: $scope.messageCount
              });

          };
      });
  };

    /*點擊查看、忽略及messagePanelState隱藏*/
	  $scope.$on("countReduce", function (evt,data) {

              MessagePanelServ.getManagerOperation(data)
              .then(function(result){
                if(result.status === 200){
                  $scope.messageCount = data.messageCount;

                    if ($scope.messageCount === 0) {
                      $scope.isShowMessageCount = false;
                      $scope.messagePanelState = false;
                    }
                  }

              })
	   });


    //点击图标让其弹出框显示与隐藏
    var toggleMessagePanel = function() {
      getMessage();
    // 消息count显示与隐藏
        if($scope.messageCount == 0){
          return false;
        }else {
          $scope.messagePanelState = !!!$scope.messagePanelState;
        }
  };

  //点击关闭按钮,弹框
    var toggleQuitLogin = function () {
      strapModal = DialogServ.modal({
        'scope': $scope,
        'templateUrl': 'view/console/modal/quitLogin.html'
      });
      strapModal.$promise.then(strapModal.show);
    };

    //弹出框的确定按钮
    var quitConfirm = function () {
      strapModal.hide();
      $state.go('login');
    };

    var bindListener = function() {
      $scope.toggleMessagePanel = toggleMessagePanel;
      $scope.toggleQuitLogin = toggleQuitLogin;
      $scope.quitConfirm = quitConfirm
    };

    var initPlugins = function() {
      $scope.messagePanelState = false;
      $scope.isShowMessageCount = true;

      $scope.$on("clearMsg",function (evt,data) {

        MessagePanelServ.getManagerClean()
          .then(function(result){
            if(result.status === 200){
              $scope.isShowMessageCount = false;
              $scope.messagePanelState = data.messagePanelState;
              $scope.messageCount= data.messageCount;

              //列表清空
              $scope.messageData = data.messageData;
            }
          })
      });

      $scope.$on("toQuit",function (evt,data) {
        $scope.quitLoginState = data.quitLoginState;
      })
    };

    var callback = function(data){
      console.log('callback'+"---"+data);
      $state.go("console.base.manager.process",{id: data.pipeline})
    };

    //浏览器消息通知
    SocketServ.initSocket(0);

    SocketServ.onMessage(function (data) {

      if(data){
          $scope.isShowMessageCount = true;
          $scope.messageCount += 1;
      };

      NotificationServ.init(data,callback);

    });

    var init = function() {
      bindListener();
      initPlugins();
      getMessage();
    };

  init();
}]);
