angular.module('HTRD_AutoConsole.A').controller('LeftCtrl', ['$scope', '$state', '$window','$timeout', function($scope, $state, $window,$timeout) {


  var menuList = [
    {
      title: '首页',
      url: '',
      icon: 'icon-system',
      childItems: [
        {
          title: '系统首页',
          url: 'console.base.home.index',
          icon: 'icon-system-index'
        }
      ]
    },
    {
      title: '流程管理',
      url: '',
      icon: 'icon-manager',
      childItems: [
        {
          title: '流程列表',
          url: 'console.base.manager.list',
          icon: 'icon-process'
        },

        {
          title: '流程结果查询',
          url: 'console.base.manager.result',
          icon: 'icon-result'
        },
        {
          title: '告警查询',
          url: 'console.base.manager.warn',
          icon: 'icon-warn'
        }
      ]
    },
    {
      title: '流程模板',
      url: '',
      icon: 'icon-template',
      childItems: [
        {
          title: '流程模板',
          url: 'console.base.template',
          icon: 'icon-template-index'
        }
      ]
    },
    // {
    //   title: '操作日志',
    //   url: '',
    //   icon: 'icon-log',
    //   childItems: [
    //     {
    //       title: '操作日志',
    //       url: 'console.base.log',
    //       icon: 'icon-log-index'
    //     }
    //   ]
    // },

    {
      title: '流程调度',
      url: '',
      icon: 'icon-control',
      childItems: [
        {
          title: '流程调度',
          url: 'console.base.manager.controlList',
          icon: 'icon-control-index'
        }
      ]
    },
    {
      title: '假日方案',
      url: '',
      icon: 'icon-holiday',
      childItems: [
        {
          title: '假日方案',
          url: 'console.base.manager.holidayList',
          icon: 'icon-holiday-index'
        }
      ]
    }
  ];

  var routerChange = function(item) {

    if (!!item.url) {
      $state.go(item.url);
    };
  };

  var parseDOM = function() {

  };

  var bindListener = function() {
    $scope.routerChange = routerChange;
  };

  var initPlugins = function() {
    $scope.menuList = menuList;

    // $timeout(function () {
      var documentHeight = $(document).height();
      $(".page-sidebar").css("height",documentHeight);
    // }, 100);
    // onresize
  };

  var init = function() {
    parseDOM();
    bindListener();
    initPlugins();
  };

  init();
}]);
