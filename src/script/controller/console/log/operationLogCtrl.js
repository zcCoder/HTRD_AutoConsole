angular.module('HTRD_AutoConsole.A').controller('OperationLogCtrl', ['$scope', '$state', '$window', 'OperationLogServ', function ($scope, $state, $window, OperationLogServ) {

  //面包屑
  var breadcrumbsData = [
    {
      'iconClass': 'icon-info',
      'text': '用户操作日志',
      'link': 'console.base.log'
    }
  ];

  var bindListener = function () {

  };

  var initPlugins = function () {
    $scope.query = '';
    $scope.fromDate = '';
    $scope.untilDate = '';
    $scope.breadcrumbsData = breadcrumbsData;
    $scope.operatorOptions = [
      {
        title: '全部',
        id: 0
      }
    ];
    $scope.operatorSelected = $scope.operatorOptions[0].title;

    OperationLogServ.getLog()
      .then(function(result) {
        if (result.stateCode === 200) {
          $scope.logList = result.data.list;
        }
      });
    OperationLogServ.getUser()
      .then(function(result) {
        if (result.stateCode === 200) {
          $scope.operatorOptions = $scope.operatorOptions.concat(result.data.list);
          $scope.operatorSelected = $scope.operatorOptions[0].title;
        }
      });
  };

  var init = function () {
    bindListener();
    initPlugins();
  };

  init();
}]);
