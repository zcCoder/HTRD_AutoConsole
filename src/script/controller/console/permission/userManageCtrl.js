/**
 * Created by Administrator on 2017/3/25.
 */
angular.module('HTRD_AutoConsole.A').controller('UserManageCtrl', ['$scope', '$state', '$window',
  'UserManagementServ', 'DateParseServ','DialogServ','AlertServ','$timeout',  function($scope, $state, $window,UserManagementServ,DateParseServ,DialogServ,AlertServ,$timeout) {
  console.log('UserManageCtrl');

  $scope.ctrlScope = $scope;

  var userConfig = {
        page: 1
    };

   var crumbsConfig = {
      index: {
            title: '用户权限',
            icon: 'icon-user'
        },
      items: [
        {
          title: '用户管理',
          icon: 'fa fa-angle-right',
          url: 'console.base.permission.user'
        },
      ]
    };

    var getUserList = function(userConfig){

      UserManagementServ.getUserManagement(userConfig)
          .then(function(data) {
              if(data.status === 200){
                  $scope.userList = data.results.data;
                  $scope.userList.map(function(item){
                    item.is_active == true ? item.is_active = "启用" : item.is_active = "禁用";
                    item.last_login = DateParseServ.dateParse(item.last_login);
                  });

                  $scope.$broadcast("toList",{
                    count: data.results.count,
                    page: $scope.userConfig.page || data.results.page
                });
             }
          });
     } ;

     var getEditList = function(id){
        UserManagementServ.editUserManagement(id);
     };

    var userCallback = function (item) {
      userConfig.result_status = item.id;
    };

    var query = function(){
      userConfig.search = $scope.search;
      getUserList(userConfig);
      $scope.search = ""
    };

    var toAddUser = function(){
      $state.go('console.base.permission.addUser');
    };

    var edit = function(item){
      $state.go("console.base.permission.addUser", {
        id: item.id
      });
    };

    var changePassward = function(item){
      $scope.id = item.id;
      strapModal = DialogServ.modal({
          'scope': $scope,
          'templateUrl': 'view/console/modal/changePassward.html'
        });
      strapModal.$promise.then(strapModal.show);
    };

    //密码验证
    var pswBlur = function(){
        console.log($scope.ctrlScope.password);
        if(!!!$scope.pswReg.test($scope.ctrlScope.password) || !!!$scope.ctrlScope.password){
          $scope.showPsw = false;
      }else{
          $scope.showPsw = true;
      };
    };

    var confirmPswBlur = function(){
      if($scope.ctrlScope.password !== $scope.ctrlScope.confirmPassword){
          $scope.isShow = true;
      }else{
          $scope.isShow = false;
      }
    };

    var confirm = function () {
      if($scope.pswReg.test($scope.ctrlScope.password) && $scope.ctrlScope.password === $scope.ctrlScope.confirmPassword){
        //发送请求c/users/1/password_change
        var param = {
          id:$scope.id,
          password:$scope.ctrlScope.password,
          passwordConfirm:$scope.ctrlScope.confirmPassword
        };
        UserManagementServ.getChangePassword(param)
          .then(function(data){
            if(data.status === 200){
                strapModal.hide();
                AlertServ.showSuccess('修改密码成功')
            }
          });
      }else{
          strapModal.hide();
          AlertServ.showSuccess('修改密码失败')
      }
    };

    var bindListener = function() {
      $scope.userCallback = userCallback;
      //添加用户
      $scope.toAddUser = toAddUser;
      //查询
      $scope.query = query;
      //编辑
      $scope.edit = edit;
      //点击修改密码弹窗
      $scope.changePassward = changePassward;
      //弹出的确定按钮
      $scope.confirm = confirm;
      //修改密码失去焦点
      $scope.pswBlur = pswBlur;
      //确认密码
      $scope.confirmPswBlur =confirmPswBlur;
    };

    var initPlugins = function() {
      //面包屑
      $scope.crumbsConfig = crumbsConfig;
      //传参
      $scope.userConfig = userConfig;
      //密码验证
      $scope.showPsw = true;
      //确认输入
      $scope.isShow = false;
      $scope.pswReg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![!#$%^&*]+$)[\da-zA-Z_]{6,20}$/;
      //下拉列表默认值
      $scope.userText = "全部状态";
      $scope.search = "";

      //下拉列表的状态
      $scope.listStatus = [
         {
          id: -1,
          title: '全部状态'
        },
        {
          id: 1,
          title: '启用'
        },
        {
          id: 0,
          title: '禁用'
        }
      ] ;

       $timeout(function(){
          getUserList();
       },1000);

       $scope.$on('toPage',function (evt,data) {
          $scope.userConfig.page = data.page;
          getUserList()
        });
    };

    var init = function() {
      bindListener();
      initPlugins();
    };

    init();

}]);
