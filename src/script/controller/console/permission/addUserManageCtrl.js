angular.module('HTRD_AutoConsole.A')
	.controller('AddUserManageCtrl', ['$scope', '$stateParams', '$state','$window','addUserManagementServ','AlertServ', 'DialogServ','$timeout',  function($scope, $stateParams, $state,$window, addUserManagementServ,AlertServ,DialogServ,$timeout) {

		var create_crumbsConfig = {

			index: {
			          title: '用户权限',
			          icon: 'icon-user'
        			},
			items: [
			  {
			    title: '用户管理',
			    icon: 'fa fa-angle-right',
			    url: 'console.base.permission.user'
			  },

			  {
			    title: '添加用户',
			    icon: 'fa fa-angle-right'
			  }
			]
		  };

		  var edit_crumbsConfig = {
		   	index: {
	          title: '用户权限',
	          icon: 'icon-user'
  			},

				items: [
				  {
				    title: '用户管理',
				    icon: 'fa fa-angle-right',
				    url: 'console.base.permission.user'
				  },

				  {
				    title: '编辑用户',
				    icon: 'fa fa-angle-right'
				  }
				]
		  };

		var user = {};

		//组的全部list
		var getGroupAllList = function(){
			addUserManagementServ.getGroupList()
			.then(function(data) {
				if(data.status === 200){
					$scope.groupList = data.results;
					$scope.queryGroup = [ ];
				}
			});
		};

		//编辑
		var getEditUserInfo = function(config){
			addUserManagementServ.getEditUserInfo(config)
		};

		//保存
		var getSaveUserInfo = function(config){
			console.log(config);
			addUserManagementServ.getSaveUserInfo(config)
		};

		//編輯
		var getEditUserInfo = function(config){
			addUserManagementServ.getSaveUserInfo(config)
		};

		//用戶信息
		var getUserInfo = function(id){
			addUserManagementServ.getUserInfo(id)
			.then(function(data) {

				if (data.status === 200) {
					$scope.user = data.results;
					$scope.queryGroup = $scope.user.groupList;
				};
			});
		};

		//密码验证
		var pswBlur = function(){
			if($scope.user.password){

				if (!!!$scope.pswReg.test(user.password)) {

					$('.password').css('display','block');

				}else{

					$('.password').css('display','none');
				}

			}else{

					$('.password').css('display','none');
			}

		};

		//验证密码
		var confirmPswBlur = function(){

			if($scope.password){
				if($scope.user.password !== $scope.password){
					$('.confirmPassword').css('display','block');
				}else{
					$('.confirmPassword').css('display','none');
				}
			}else{
				$('.confirmPassword').css('display','none');
			}

		};

		//验证邮箱
		var emailBlur = function(){
			if (!!!$scope.emailReg.test(user.email)) {

					$('.email').css('display','block');

				}else{

					$('.email').css('display','none');
				}
		};


		//权限设置
		 var select = function(type){
		 	type === 1 ? $scope.user.is_active = true : $scope.user.is_active = false;
		 };

		 //点击保存
		var save = function(){

			if($scope.pswReg.test(user.password) && $scope.emailReg.test(user.email)){
				AlertServ.showSuccess('保存成功');

				if($stateParams.id){
					getEditUserInfo($scope.user)
				}else{
					getSaveUserInfo($scope.user)
				};
			};
		};

		//点击放弃
		var giveUp = function () {
			strapModal = DialogServ.modal({
				'scope': $scope,
				'templateUrl': 'view/console/modal/giveUpSave.html'
			});
			strapModal.$promise.then(strapModal.show);
		};

		//弹出框的确定按钮
		var confirm = function () {
			strapModal.hide();
			$state.go('console.base.userPermission.addUser');
		};

		//保存并添加
		var saveAdd = function(){
			if(!!$stateParams.id){
				getEditUserInfo($scope.user)
			}else{
				getSaveUserInfo($scope.user)
			};
			$state.go('console.base.userPermission.addUser');
		}

		var bindListener = function() {
			$scope.save = save;
			$scope.giveUp = giveUp;
			$scope.saveAdd =saveAdd;
			$scope.confirm = confirm;
			$scope.select = select;
			$scope.pswBlur = pswBlur;
			$scope.confirmPswBlur = confirmPswBlur;
			$scope.emailBlur = emailBlur;
		};

		var initPlugins = function() {
			$scope.edit_crumbsConfig = edit_crumbsConfig;
			$scope.create_crumbsConfig = create_crumbsConfig;
			$scope.title = '选择组';
			$scope.user = user;
			$scope.pswReg = /^[a-zA-Z\d\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,\-.\<=>?@\[\]^_`{|}~]{6,20}$/;
			$scope.emailReg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

			//判断添加用户与编辑
			if (!!$stateParams.id) {
				var id = $stateParams.id;
				$scope.isHide = true;
				getGroupAllList();
				getEditUserInfo(id);
			} else {
				getGroupAllList();
			};

			$scope.$on("selectValue",function(evt,data){

				data.selectData = data.selectData.map(function(item){

					if(typeof item === 'object'){
						item =  item.id;
					}
					return item;
				});
				 $scope.user.groupList=data.selectData;
			});
		};

		var init = function() {
			bindListener();
			initPlugins();
		};

		init();

	}]);
