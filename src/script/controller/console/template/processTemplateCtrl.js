angular.module('HTRD_AutoConsole.A')
  .controller('ProcessTemplateCtrl', ['$scope', '$state', '$window', 'ManagerWarnServ', 'DateParseServ','$timeout',function ($scope, $state, $window, ManagerWarnServ,DateParseServ,$timeout) {

     //面包屑
     $scope.crumbsConfig = {
       index: {
         title: '流程模板',
         icon: 'htrd-icon-template-sm'
       }
     };

     $scope.data = [
       {
         id: 40,
         title: '开市流程模板',
         count: 10
       },
       {
         id: 41,
         title: '闭市流程模板',
         count: 10
       },
       {
         id: 39,
         title: '巡检流程模板',
         count: 8
       }
     ];

     var processEdit = function (parma) {
       $state.go('console.base.manager.edit',{
         id: parma.id
       })
     }

    var bindListener = function () {
      $scope.processEdit = processEdit;
    };


    var initPlugins = function () {

    };

    var init = function () {
      bindListener();
      initPlugins();
    };

    init();
}])
