angular.module('HTRD_AutoConsole.A').controller('ManagerHistoryProcessCtrl', ['$scope', '$state', '$window', '$stateParams', 'ManagerProcessQuneeServ',
function($scope, $state, $window, $stateParams, ManagerProcessQuneeServ) {

  var getManagerProcessQuneeInfo = function() {
    console.log($stateParams);
    ManagerProcessQuneeServ.getManagerProcessQuneeInfo({
      id: $stateParams.processId
    })
    .then(function(ret) {

      if (ret.status === 200) {
        $scope.processName = ret.results.name + ' - 历史执行记录';

        $scope.crumbsConfig = {
          index: {
            title: '流程管理',
            icon: 'icon-home'
          },
          items: [
            {
              title: '流程列表',
              icon: 'fa fa-angle-right',
              url: 'console.base.manager.list'
            },

            {
              title: $scope.processName,
              icon: 'fa fa-angle-right'
            }
          ]
        }
      };
    });
  };

  var bindListener = function() {
    $scope.getManagerProcessQuneeInfo = getManagerProcessQuneeInfo;
  };

  var initPlugins = function() {
    $scope.getManagerProcessQuneeInfo();
  };

  var init = function() {
    bindListener();
    initPlugins();
  };

  init();
}]);
