angular.module('HTRD_AutoConsole.A').controller('ManagerReviewCtrl', ['$scope', '$state', '$timeout', 'ManagerReviewServ', 'ManagerProcessTreeServ', function($scope, $state, $timeout, ManagerReviewServ, ManagerProcessTreeServ) {

  var pageConfig = {
    limit: 2,
    page: 1
  };

  // tree点击回调
  var treeCallBack = function(data) {
    console.log(data);
  };
  
  // 改变排序方式(类型/升降)
  var changeOrder = function(type) {
    if (type === 'ignore') {
      return;
    }
    if (type === $scope.orderType) {
      $scope.isUp = !$scope.isUp;
      return;
    }
    $scope.isUp = true;
    $scope.orderType = type;
  };
  
  var bindListener = function () {

  };
  
  var initPlugins = function () {
    $scope.pageConfig = pageConfig;
    //发送请求获取全部列表数据
    $scope.query = '';
    // 排序类型
    $scope.orderType = 'name';
    // 是否升序
    $scope.isUp = true;
    // 改变排序方式(类型/升降)
    $scope.changeOrder = changeOrder;
    // 面包屑数据
    $scope.breadcrumbsData = ManagerReviewServ.breadcrumbsData;
    // 表格头
    $scope.headList = ManagerReviewServ.headList;
    // 状态解析
    $scope.stateParse = ManagerReviewServ.stateParse;
    // 递归设置tree
    $scope.setTreeData = ManagerProcessTreeServ.setTreeData;
    // 获取流程tree
    ManagerProcessTreeServ.getProcessTree({
        format: 'json'
      })
      .then(function(result) {
        $scope.treeData = result.results.map($scope.setTreeData);
      });
    // tree选中回调
    $scope.treeCallBack = treeCallBack;
    // 获取审核列表
    ManagerReviewServ.getReviewList({
        format: 'json'
      }).then(function(res) {
        if (res.status === 200) {
          $scope.reviewList = res.results.data;
          $scope.total = res.results.count;
          // page.html未加载,事件传播无效
          $timeout(function() {
            $scope.$broadcast("toList", {
              //每页显示多少
              limit: $scope.pageConfig.limit,
              //当前第几页
              page: $scope.pageConfig.page,
              //共有多少条
              count: $scope.total
            });
          }, 100);
        }
      });
  };

  var init = function () {
    bindListener();
    initPlugins();
  };

  init();

}]);