angular.module('HTRD_AutoConsole.A').controller('ManagerHolidayListCtrl', ['$scope', '$state', '$window', '$timeout', 'ManagerHolidayServ', 'AlertServ', 'DialogServ',
function($scope, $state, $window, $timeout, ManagerHolidayServ, AlertServ, DialogServ) {
  var starpModal;

  // 获取假日方案列表
  var getHolidayList = function() {
    ManagerHolidayServ.getHolidayList($scope.pageConfig)
    .then(function(ret) {
       console.log(ret);  //获取当前 页面 列表数据

      if (ret.status === 200) {
        $scope.holidayList = ret.results.data;
        $scope.pageConfig.page = ret.results.page;

        $scope.$broadcast("toList", {
          page: $scope.pageConfig.page,
          count: ret.results.count
        });

      } else {
        $scope.holidayList = [];
      };
    });
  };

  // 新建假日方案
  var addHoliday = function() {
    console.log('addHoliday');
    $state.go('console.base.manager.holidayInfo');
  };

  // 编辑假日方案
  var editHoliday = function(item) {
    console.log('editHoliday');
    //console.log(item.id);
    $state.go('console.base.manager.holidayInfo', {
      id: item.id
    });
  };

  // 删除单个假日方案
  var delHoliday = function(item) {
    console.log('delHoliday');
    $scope.holidayId = item.id;
    $scope.holidayContent = item.title;
    starpModal = DialogServ.modal({
      'scope': $scope,
      'backdrop': 'static',
      'title': '删除假日方案',
      'templateUrl': 'view/console/modal/holidayConfirmDelete.html'
    });
  };

  var submitDelHoliday = function() {
    console.log('submitDelHoliday');
    //console.log($scope.holidayId);
    starpModal.hide();
    AlertServ.showSuccess('删除成功');
  };

  var bindListener = function() {
    $scope.getHolidayList = getHolidayList;
    $scope.addHoliday = addHoliday;
    $scope.editHoliday = editHoliday;
    $scope.delHoliday = delHoliday;
    $scope.submitDelHoliday = submitDelHoliday;
  };

  var initPlugins = function() {
    $scope.getHolidayList();

    // 面包屑
    $scope.crumbsConfig = {
      index: {
        title: '假日方案',
        icon: 'htrd-icon-holiday-sm'
      }
    };

    $scope.pageConfig = {};

    $scope.$on('toPage', function(evt, data) {
      $scope.pageConfig.page = data.page;
      $scope.getHolidayList();
    });
  };

  var init = function() {
    bindListener();
    initPlugins();
  };

  init();
}]);
