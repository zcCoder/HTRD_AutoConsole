angular.module('HTRD_AutoConsole.A').controller('ManagerResultCtrl', ['$scope', '$state', '$window', '$timeout', 'ManagerResultServ', 'StatusMapServ', 'DateParseServ',function($scope, $state, $window, $timeout, ManagerResultServ, StatusMapServ,DateParseServ) {

    var config = {
      page: 1
    };

    //面包屑
     $scope.crumbsConfig = {
          index: {
            title: '流程管理',
            icon: 'htrd-icon-process-sm'
          },
          items: [
            {
              title: '流程结果查询',
              icon: 'fa fa-angle-right',
              url: 'console.base.manager.result'
            }
          ]
    };

    var search = function() {
      if($scope.isClick){
        return;
      }
      $scope.config.page = 1;
      $scope.searchQuery = $scope.query;
      $scope.searchStatus = $scope.statusId;
      $scope.getResultList();
    };

    // 更改结果下拉选项
    var resultCallBack = function(item) {
      console.log('item'+item)
      $scope.statusId = item.id;
    };
    var changeOrder = function(type) {
      if (type === 'ignore') {
        return;
      }
      if (type === $scope.orderType) {
        $scope.isUp = !$scope.isUp;
        $scope.getResultList();
        return;
      }
      $scope.isUp = true;
      $scope.orderType = type;
      $scope.getResultList();
    };

    var toManagerProcess = function(item){
      console.log(item);
      $state.go("console.base.manager.historyProcess",{id: item.id, processId: item.pipeline});
    };


    var getResultList = function() {
      var params = {
        'min_date': $scope.config.min_date,
        'max_date': $scope.config.max_date,
        'page': $scope.config.page,
        'sort': ($scope.isUp ? '' : '-') + $scope.orderType,
        'group': $scope.group
      };

      if ($scope.searchQuery) {
        params.search = $scope.searchQuery;
      }
      if ($scope.searchStatus) {
        params.result_status = $scope.searchStatus;
      }

      ManagerResultServ.getResult(params)
        .then(function(res) {
          if (res.status === 200) {
            $scope.processResult = res.results.data;
            $scope.total = res.results.count;

            console.log($scope.processResult)

            $scope.processResult.map(function(item) {
              //console.log(item.started_at+"---"+item.ended_at)
              var started_at = DateParseServ.dateParse(item.started_at);
              item.started_at = started_at;
              if(item.ended_at){
                var ended_at = DateParseServ.dateParse(item.ended_at);
                item.ended_at = ended_at;
              }
            });

            // page.html未加载,事件传播无效
            $timeout(function() {
              $scope.$broadcast("toList", {
                //当前第几页
                page: $scope.config.page,
                //共有多少条
                count: $scope.total
              });
            }, 100);
          }
        });
    };

    var bindListener = function() {
      // 查询结果列表
      $scope.search = search;
      // 结果选择回调
      $scope.resultCallBack = resultCallBack;
      // 结果状态转换为中文
      $scope.resultParse = StatusMapServ.getStatusData;
      // 改变排序方式(类型/升降)
      $scope.changeOrder = changeOrder;
      // 请求流程结果列表
      $scope.getResultList = getResultList;
      //整行点击跳转
      $scope.toManagerProcess = toManagerProcess;
      // 分页跳转,返回选中页码
      $scope.$on('toPage', function(evt, data) {
        $scope.config.page = data.page;
        $scope.getResultList();
      });

      //监听时间变化
      $scope.$on("showBtn",function(evt,data){
        console.log(data)
          $scope.isClick = data.isClick;
          $scope.config.min_date = data.min_date;
          $scope.config.max_date = data.max_date
        })
    };

    var initPlugins = function() {
      $scope.config = config;
      // 搜索字段
      $scope.query = '';

      //时间
      $scope.$on('toDate',function(evt,data){
        $scope.config.min_date = data.min_date;
        $scope.config.max_date = data.max_date;
      });

      // 表格标题数组
      $scope.headList = ManagerResultServ.headList;
      // 结果筛选
      $scope.resultSelected = '全部结果';
      // 结果分类数组
      $scope.resultOptions = ManagerResultServ.resultOptions;
      // 排序类型
      $scope.orderType = 'started_at';

      // 是否升序
      $scope.isUp = false;
      $scope.isClick = false;

      $timeout(function(){
         $scope.getResultList();
      },1000)

    };

    var init = function() {
      bindListener();
      initPlugins();
    };

    init();
  }]);
