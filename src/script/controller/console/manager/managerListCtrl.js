/**
 * Created by Administrator on 2017/3/25.
 */
angular.module('HTRD_AutoConsole.A').controller('ManagerListCtrl', ['$scope', '$state', '$window', '$timeout', 'ManagerListServ', 'ManagerProcessTreeServ', 'DateParseServ',function($scope, $state, $window, $timeout, ManagerListServ, ManagerProcessTreeServ,DateParseServ) {
  var status = {
      0:{
        text:"未执行"
      },
      1:{
        text:"正在检测"
      },
      2:{
        text:"执行中"
      }
  };

  var stateReviewerOptions = [
      {
        'title': '全部状态'
      },

      {
        'id': 0,
        'title': '待审核'
      },
      {
        'id': 1,
        'title': '已通过'
      },
      {
        'id': 2,
        'title': '已拒绝'
      }
    ];

  var search = function() {
    $scope.pageConfig.page = 1;
    $scope.searchQuery = $scope.query;
    $scope.searchStatus = $scope.statusId;
    console.log($scope.searchStatus);
    $scope.getProcessList();
  };

  var toManagerEdit = function(item) {
    if(!$scope.isClick){
      return;
    }
    $state.go('console.base.manager.edit',
      { id: item.id});
  };

//创建
  var toManagerCreat = function(){
    $state.go('console.base.manager.edit');
  };

  var toManagerProcess = function($event, id) {
    if ($event.target.nodeName !== 'A') {
      $state.go('console.base.manager.process', {
        id: id
      });
    }
  };

  var toManagerRevirwer = function(item){
    //流程审核
    //$state.go('console.base.manager.audit',{id:item.id})
  }

  var toManagerSchedule = function(item){
    //流程调度
    //$state.go('console.base.manager.control',{id:item.id})
  }
  var clearTreeCache = function() {
    localStorage.removeItem('jstree');
  };

  var treeCallBack = function(data) {
    console.log(data) 
    //$scope.group = data.node.id;  //侧边的 组 ID
    var childs=data.node.children;
    if(childs.length>0){
      var temp=childs.join(",");
      //console.log(temp);
      $scope.group = temp;

    }else{
      $scope.group = data.node.id
    }
    $scope.getProcessList();
  };







//====================

  /*var changeOrder = function(type) {

    if (type === 'ignore') {
      return;
    }
    if (type === $scope.orderType) {
      $scope.isUp = !$scope.isUp;
      $scope.getProcessList();
      return;
    }
    $scope.isUp = true;
    $scope.orderType = type;
    $scope.getProcessList();
  };*/

  var changeIdOrder = function(){
    $scope.classReviewState = '';
    $scope.classCreatState = '';
    $scope.classIdState = !!! $scope.classIdState;
    $scope.classState = $scope.classIdState;
    $scope.getProcessList()
  };

  var changeCreatOrder = function(){
    $scope.classReviewState = '';
    $scope.classIdState = '';

    if($scope.classCreatState){
      $scope.classCreatState = !!!$scope.classCreatState;
    }else {
      $scope.classCreatState = true;
    };
    console.log(1111);
    $scope.orderType =  'created_at';
    $scope.classState = $scope.classCreatState;
    $scope.getProcessList()
  };

  var changeReviewOrder = function(){
    $scope.classCreatState = '';
    $scope.classIdState = '';
    if($scope.classReviewState){
      $scope.classReviewState = !!!$scope.classReviewState;
    }else {
      $scope.classReviewState = true;
    };
    $scope.orderType =  'reviewed_at';
    $scope.classState = $scope.classReviewState;
    $scope.getProcessList();
  }

  var changeState = function(item) {
    //console.log(item)
    $scope.statusId = item.id;
  };

  var changeReviewerState = function(item){
    $scope.reviewerStatus = item.id;
  };

  var getProcessList = function() {
    var params = {
      'page': $scope.pageConfig.page,
      'sort': ($scope.classState ? '' : '-') + $scope.orderType,
      'group': $scope.group,     // [1,2,3].join(',')  // 1,2,3       // 侧边的 组 ID -> 无组 子集ID  2 7 8 9 11 12
      //'group': 8,           // 侧边的 组 ID -> 无组 子集ID  2 7 8 9 11 12
      'lock_on':$scope.searchStatus,  // 流程状态 ID 0-2
      'status':$scope.reviewerStatus  // 审核状态 ID 0-2
    };

    if ($scope.searchQuery) {
      params.search = $scope.searchQuery;
    };

    ManagerListServ.getProcessList(params)
    .then(function(res) {
      console.log("获取流程列表",res);
      if (res.status === 200) {
        $scope.processList = res.results.data;
        $scope.total = res.results.count;

        $scope.processList.map(function(item){

          item.created_at = DateParseServ.dateParse(item.created_at);
          item.reviewed_at = DateParseServ.dateParse(item.reviewed_at);
          if(item.lock_on !== 0 || item.status === 1){
            $scope.isClick = false;
          }else {
            $scope.isClick = true;
          };
          var model = $scope.status[item.lock_on];
          item.lock_on = model.text;
          item.status = $scope.stateParse(item.status);
        })
        // page.html未加载,事件传播无效
        $timeout(function() {
          $scope.$broadcast("toList", {
            //当前第几页
            page: $scope.pageConfig.page,
            //共有多少条
            count: $scope.total
          });
        }, 100);
      }
    });

  };



  var bindListener = function() {
    // 查询流程列表
    $scope.search = search;
    // 跳转到编辑页
    $scope.toManagerEdit = toManagerEdit;
    // 跳转到详情页
    $scope.toManagerProcess = toManagerProcess;
    // 清除tree的localstorage
    $scope.clearTreeCache = clearTreeCache;
    // 递归设置tree
    $scope.setTreeData = ManagerProcessTreeServ.setTreeData;
    // tree点击回调
    $scope.treeCallBack = treeCallBack;
    // 改变排序方式(类型/升降)
    $scope.changeIdOrder = changeIdOrder;
    $scope.changeCreatOrder = changeCreatOrder;
    $scope.changeReviewOrder = changeReviewOrder;
    // 更改下拉状态
    $scope.changeState = changeState;
    $scope.changeReviewerState = changeReviewerState;
    // 获取流程列表
    $scope.getProcessList = getProcessList;
    //创建
    $scope.toManagerCreat = toManagerCreat;
    //跳转审核页
    $scope.toManagerRevirwer = toManagerRevirwer;
    //跳转流程调度
    $scope.toManagerSchedule = toManagerSchedule;

    // 分页跳转,返回选中页码
    $scope.$on('toPage', function(evt, data) {
      $scope.pageConfig.page = data.page;
      $scope.getProcessList();
    });
  };

  var initPlugins = function() {
    $window.isEdit = false;
    $scope.pageConfig = {
      page: 1
    };

    $scope.clearTreeCache();
    //获取树的数据
    ManagerProcessTreeServ.getProcessTree()
    .then(function(result) {
      $scope.treeData = result.results.map($scope.setTreeData);
      console.log('$scope.treeData', $scope.treeData);
    });

    //发送请求获取全部列表数据
    $scope.query = '';
    // 排序类型
    $scope.orderType = 'id';
    // 是否升序
    $scope.classState = false;
    $scope.classIdState = false;
    $scope.classReviewState = '';
    $scope.classCreatState = '';

    // 面包屑数据
    $scope.breadcrumbsData = ManagerListServ.breadcrumbsData;
    // 表格标题列表
    $scope.headList = ManagerListServ.headList;
    // 下拉状态选项列表
    $scope.stateOptions = ManagerListServ.stateOptions;
    console.log($scope.stateOptions);
    // 当前状态
    $scope.stateSelected = '全部状态';
    $scope.reviewerSelected = '全部状态';
    // 状态解析(0 => 未审核)
    $scope.stateParse = ManagerListServ.stateParse;
    // $scope.getProcessList();
    $scope.status = status;
    $scope.isClick = true;
    $scope.stateReviewerOptions = stateReviewerOptions;
  };

  var init = function() {
    bindListener();
    initPlugins();
  };

  init();

}]);
