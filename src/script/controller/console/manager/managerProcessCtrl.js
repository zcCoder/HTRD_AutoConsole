 angular.module('HTRD_AutoConsole.A').controller('ManagerProcessCtrl', ['$scope', '$state', '$window', '$stateParams', 'ManagerProcessTreeServ', 'ManagerProcessQuneeServ',
function($scope, $state, $window, $stateParams, ManagerProcessTreeServ, ManagerProcessQuneeServ) {

  var getManagerProcessQuneeInfo = function() {
    ManagerProcessQuneeServ.getManagerProcessQuneeInfo({
      id: $stateParams.id
    })
    .then(function(ret) {
      console.log("ret",ret);    //详情页中获取 跳转前的 group || id

      if (ret.status === 200) {
        $scope.processName = ret.results.name;

        $scope.crumbsConfig = {
          index: {
            title: '流程管理',
            icon: 'htrd-icon-process-sm'
          },
          items: [
            {
              title: '流程列表',
              icon: 'fa fa-angle-right',
              url: 'console.base.manager.list'
            },

            {
              title: $scope.processName,
              icon: 'fa fa-angle-right'
            }
          ]
        }
      };
    });
  };

  var bindListener = function() {
    $scope.getManagerProcessQuneeInfo = getManagerProcessQuneeInfo;
  };

  var initPlugins = function() {
    $scope.getManagerProcessQuneeInfo();
  };

  var init = function() {
    bindListener();
    initPlugins();
  };

  init();
}]);
