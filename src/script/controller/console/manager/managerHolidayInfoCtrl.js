angular.module('HTRD_AutoConsole.A').controller('ManagerHolidayInfoCtrl', ['$scope', '$state', '$window', '$timeout', '$stateParams', 'ManagerHolidayServ', 'AlertServ', 'DialogServ',
function($scope, $state, $window, $timeout, $stateParams, ManagerHolidayServ, AlertServ, DialogServ) {
  var starpModal;
  var fullYear;

  var getCalendarDayList = function() {
    ManagerHolidayServ.getHolidayInfo({
      id: $stateParams.id
    })
    .then(function(ret) {
      console.log(ret);


      if (ret.status === 200) {
        $scope.hilidayInfo = ret.results;
        JSON.parse(ret.results.days).map(function(item) {
          console.log(arguments)
          $scope.series[item.month - 1][item.monthToWeek - 1][item.dayToWeek].active = true;
        });
        $scope.hilidayInfo.calendarDayList = JSON.parse($scope.hilidayInfo.days);
        $scope.dateCycleText = ret.results.year;
        console.log('$scope.dateCycleText', $scope.dateCycleText);
      } else {

      };
    });
    // var calendarDayList = [{"text":"12","month":"1","day":"12","monthToWeek":2,"dayToWeek":"4","$$hashKey":"object:298","active":true}];
    // console.log('calendarDayList', calendarDayList);
    //
    // // 读取之前选中的日期
    // calendarDayList.map(function(item) {
    //   $scope.series[item.month - 1][item.monthToWeek - 1][item.dayToWeek].active = true;
    // });
    //
    // $scope.hilidayInfo.calendarDayList = calendarDayList;
  };

  // 获得所选年份的日期列表
  var getDateList = function(year) {
    var start_time = year + '-01-01';
    var end_time = year + '-12-31';
    var bd = new Date(start_time),be = new Date(end_time);
    var bd_time = bd.getTime(), be_time = be.getTime(),time_diff = be_time - bd_time;
    var d_arr = [];

    for(var i = 0; i <= time_diff; i += 86400000){
      var ds = new Date(bd_time+i);
      d_arr.push(year + '-' + (ds.getMonth()+1) + '-' + ds.getDate());
    };
    return d_arr;
  };

  // 计算当前日期在本月份的周数
  var getWeekOfMonth = function(weekStart) {
    var weekStartArr = weekStart.split('-');
    var date = new Date(weekStartArr[0], weekStartArr[1] - 1, weekStartArr[2]); // 注意：JS 中月的取值范围为 0~11
    var weekStart = 0;
    var dayOfWeek = date.getDay();
    var day = date.getDate();
    return Math.ceil((day - dayOfWeek - 1) / 7) + ((dayOfWeek >= weekStart) ? 1 : 0);
  };

  // 获得日期的坐标
  var getDatePosition = function(date) {

    // 第几月
    var month = moment(date).format("M");

    // 今年第几周
    var week = moment(date).format("w");

    // 本月第几天
    var day = moment(date).format("DD");

    // 是本周的星期几
    var dayToWeek = moment(date).format('d')

    // 本月第几周 今年第几周 - ((月份 - 1) * 4);
    var monthToWeek = 0;

    monthToWeek = getWeekOfMonth(date);

    var seriesDay = $scope.series[month - 1][monthToWeek - 1][dayToWeek];
    seriesDay.text = day;
    seriesDay.month = month;
    seriesDay.day = day;
    seriesDay.monthToWeek = monthToWeek;
    seriesDay.dayToWeek = dayToWeek;

    if (dayToWeek === '0' || dayToWeek === '6') {
      seriesDay.holiday = true;
    };
  };

  var submitSwitchYear = function() {
    console.log('submitSwitchYear');
    $scope.hilidayInfo.year = $scope.changeYear;
    $scope.parseCalendar($scope.hilidayInfo.year);
    starpModal.hide();
  };

  var changeDateCycle = function(item) {
    $scope.changeYear = item;
    starpModal = DialogServ.modal({
      'scope': $scope,
      'backdrop': 'static',
      'title': '提示',
      'templateUrl': 'view/console/modal/holidayConfirmSwitchYear.html'
    });
  };

  var parseCalendar = function(year) {
    $scope.series = [];

    var monthList = [];

    // 生成所有月
    for (var m = 0;m < 12;m++) {

      var weekList = [];

      // 生成所有周
      for (var w = 0;w < 6;w++) {

        var dayList = [];
        var maxd = 7;

        if (w === 5) {
          maxd = 2;
        };

        // 生成每周的所有天
        for (var d = 0;d < maxd;d++) {

          // 设置默认值
          var day = {
            text: '-'
          };
          dayList.push(day);
        };

        weekList.push(dayList);
      };

      monthList.push(weekList);
    };

    $scope.series = monthList;

    var dateList = $scope.getDateList(year);
    dateList.map(function(item) {
      getDatePosition(item);
    });
  };

  var toggleDay = function(item) {

    if (!!!item.holiday && item.text !== '-') {
      item.active = !!!item.active;
    };
    console.log(item);
  };


  var addHoliday = function() {
    $scope.hilidayInfo.calendarDayList = [];

    $scope.series.map(function(data) {
      data.map(function(items) {
        items.map(function(item) {
          if (item.active) {
            $scope.hilidayInfo.calendarDayList.push(item);
          };
        });
      })
    });

    $scope.hilidayInfo.days = angular.toJson($scope.hilidayInfo.calendarDayList);
    delete $scope.hilidayInfo.calendarDayList;

    if ($stateParams.id) {
      $scope.hilidayInfo.id = $stateParams.id;
      ManagerHolidayServ.editHolidayInfo($scope.hilidayInfo)
      .then(function(ret) {
        if (ret.status === 200) {
          AlertServ.showSuccess();
          $state.go('console.base.manager.holidayList');
        } else {
          AlertServ.showError(ret.msg);
        };
      });
    } else {
      ManagerHolidayServ.addHolidayInfo($scope.hilidayInfo)
      .then(function(ret) {

        console.log("是创建");
        console.log(ret);

        if (ret.status === 200) {
          AlertServ.showSuccess();
          $state.go('console.base.manager.holidayList');
        } else {
          AlertServ.showError(ret.msg);
        };
      });
    };
  };

  var bindListener = function() {
    $scope.changeDateCycle = changeDateCycle;
    $scope.getDateList = getDateList;
    $scope.getDatePosition = getDatePosition;
    $scope.parseCalendar = parseCalendar;
    $scope.toggleDay = toggleDay;
    $scope.addHoliday = addHoliday;
    $scope.getCalendarDayList = getCalendarDayList;
    $scope.submitSwitchYear = submitSwitchYear;
  };

  var initPlugins = function() {
    $scope.dateCycleText = '2017';
    $scope.dateCycleId = 0;

    $scope.title = !!$stateParams.id ? '编辑假日方案' : '新建假日方案';
    $scope.hilidayInfo = {};
    $scope.dateCycleList = [];

    for (var i = 0;i < 10;i++) {
      $scope.dateCycleList.push(i + 2017);
    };
    console.log('$scope.dateCycleList', $scope.dateCycleList);

    $scope.table = {
      xAxis: [
        {
          index: 0,
          text: '日'
        },

        {
          index: 1,
          text: '一'
        },

        {
          index: 2,
          text: '二'
        },

        {
          index: 3,
          text: '三'
        },

        {
          index: 4,
          text: '四'
        },

        {
          index: 5,
          text: '五'
        },

        {
          index: 6,
          text: '六'
        },

        {
          index: 0,
          text: '日'
        },

        {
          index: 1,
          text: '一'
        },

        {
          index: 2,
          text: '二'
        },

        {
          index: 3,
          text: '三'
        },

        {
          index: 4,
          text: '四'
        },

        {
          index: 5,
          text: '五'
        },

        {
          index: 6,
          text: '六'
        },

        {
          index: 0,
          text: '日'
        },

        {
          index: 1,
          text: '一'
        },

        {
          index: 2,
          text: '二'
        },

        {
          index: 3,
          text: '三'
        },

        {
          index: 4,
          text: '四'
        },

        {
          index: 5,
          text: '五'
        },

        {
          index: 6,
          text: '六'
        },

        {
          index: 0,
          text: '日'
        },

        {
          index: 1,
          text: '一'
        },

        {
          index: 2,
          text: '二'
        },

        {
          index: 3,
          text: '三'
        },

        {
          index: 4,
          text: '四'
        },

        {
          index: 5,
          text: '五'
        },

        {
          index: 6,
          text: '六'
        },

        {
          index: 0,
          text: '日'
        },

        {
          index: 1,
          text: '一'
        },

        {
          index: 2,
          text: '二'
        },

        {
          index: 3,
          text: '三'
        },

        {
          index: 4,
          text: '四'
        },

        {
          index: 5,
          text: '五'
        },

        {
          index: 6,
          text: '六'
        },
        {
          index: 0,
          text: '日'
        },

        {
          index: 1,
          text: '一'
        }
      ],
      yAxis: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
    };

    // 面包屑
    $scope.crumbsConfig = {
      index: {
        title: '假日管理',
        icon: 'htrd-icon-holiday-sm'
      },

      items: [
        {
          title: '假日计划',
          icon: 'fa fa-angle-right'
        },

        {
          title: $scope.title,
          icon: 'fa fa-angle-right'
        }
      ]
    };
    fullYear = new Date().getFullYear();
    $scope.hilidayInfo.year = fullYear;
    $scope.dateCycleText = fullYear;

    $scope.parseCalendar(fullYear);

    if ($stateParams.id) {
      $scope.getCalendarDayList();
    };
  };

  var init = function() {
    bindListener();
    initPlugins();
  };

  init();

  var arr = ['关机', '重启'];
}]);
