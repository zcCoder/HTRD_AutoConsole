angular.module('HTRD_AutoConsole.A').controller('ManagerProcessEditCtrl',['$scope', '$state', '$stateParams','$window',
function($scope, $state, $stateParams,$window) {

    var crumbsEdit = {
      index: {
            title: '运维管理',
            icon: 'icon-puzzle'
        },
      items: [
        {
          title: '流程列表',
          icon: 'fa fa-angle-right',
          url: 'console.base.manager.list'
        },
        {
          title: '编辑流程',
           icon: 'fa fa-angle-right',
          url: 'console.base.manager.edit'
        }
      ]
    };

    var crumbsCreat= {
      index: {
            title: '运维管理',
            icon: 'icon-puzzle'
        },
      items: [
        {
          title: '流程列表',
          icon: 'fa fa-angle-right',
          url: 'console.base.manager.list'
        },
        {
          title: '创建流程',
          icon: 'fa fa-angle-right',
          url: 'console.base.manager.edit'
        }
      ]
    };

    var crumbsTemplate= {
      index: {
            title: '流程模板',
            icon: 'htrd-icon-template-sm'
        },
      items: [
        {
          title: '编辑流程',
           icon: 'fa fa-angle-right',
          url: 'console.base.manager.edit'
        }
      ]
    };

    // $("body, html").on("click",function(e){
    //   if($(e.target).hasClass("btn htrd-c-button qunee-check edit")){
    //     $scope.processInfoState = !!!$scope.processInfoState;
    //     $scope.$broadcast('qunee.process.edit', {
    //       processInfoState: $scope.processInfoState
    //     });
    //   }else{
    //     $scope.$apply(function(){
    //        $scope.processInfoState = false;
    //        $scope.$broadcast('qunee.process.edit', {
    //          processInfoState: $scope.processInfoState
    //        });
    //     })
    //   }
    // });
    //


  var bindListener = function() {

  };

  var initPlugins = function() {
    $scope.crumbsTemplate = crumbsTemplate;
    $scope.crumbsEdit = crumbsEdit;
    $scope.crumbsCreat = crumbsCreat;
  };

  //判断创建与编辑
    if (!!$stateParams.id) {

      if($stateParams.id == 39 || $stateParams.id == 40 || $stateParams.id == 41){
        $scope.templateShow = true;
        $scope.editShow = $scope.creatShow = false;
      }else{
        $scope.editShow = true;
        $scope.creatShow = $scope.templateShow = false;
      }

    } else {
      $scope.creatShow = true;
      $scope.editShow = $scope.templateShow = false;
    };

  var init = function() {
    bindListener();
    initPlugins();
  };

  init();
}]);
