angular.module('HTRD_AutoConsole.A')
  .controller('ManagerWarnCtrl', ['$scope', '$state', '$window', 'ManagerWarnServ', 'DateParseServ','$timeout',function ($scope, $state, $window, ManagerWarnServ,DateParseServ,$timeout) {

    var getWarnManager;
    var warnConfig = {
      page: 1
    };

     //面包屑
    $scope.crumbsConfig = {
        index: {
          title: '流程管理',
          icon: 'htrd-icon-process-sm'
        },

        items: [
          {
            title: '流程告警查询',
            icon: 'fa fa-angle-right',
            url: 'console.base.manager.warn'
          }
        ]
    };

    //返回告警级别对应的class
    var statusMap = {
      1:{
        className: "default",
        text:"检测异常"
      },
      2:{
        className: 'error',
        text:"执行超时"
      },
      3:{
        className: 'timeout',
        text:"执行错误"
      }
    };

    var getWarnManager = function(warnConfig){
       /*发送请求获取数据*/
      ManagerWarnServ.getResult(warnConfig)
        .then(function(result) {

          if (result.status === 200) {
            $scope.processResult = result.results.data;

            $scope.processResult.map(function(item) {
              var started_at = DateParseServ.dateParse(item.started_at);
              var ended_at = DateParseServ.dateParse(item.ended_at);
              var model = statusMap[item.level];
              item.started_at = started_at;
              item.ended_at = ended_at;
              item.color = model.className;
              item.level = model.text;
            });

            /*分页*/
            $scope.$broadcast("toList", {
                    //当前第几页
                    page: $scope.warnConfig.page,
                    //共有多少条
                    count: result.results.count
              });
            }
        });
      };

    /*查询*/
    var query = function(){
      if($scope.isClick){
        return;
      }
      warnConfig.search = $scope.search;
      //  console.log('开始时间'+$scope.warnConfig.min_date+'--------'+'结束时间'+$scope.warnConfig.max_date)
      getWarnManager(warnConfig)
    };


    /*时间排序*/
    var changeSort = function(){
      $scope.classState = !!!$scope.classState
        if($scope.classState === true){
          $scope.warnConfig.sort= 'started_at',
          getWarnManager($scope.warnConfig)
        }else{
          $scope.warnConfig.sort= '-started_at',
          getWarnManager($scope.warnConfig)
        }
    };

    var bindListener = function () {
      $scope.query = query;
      $scope.changeSort = changeSort;
    };

    var initPlugins = function () {
      $scope.warnConfig = warnConfig;
      $scope.isClick = false;
      $scope.classState = false;

      //时间
      $scope.$on('toDate',function(evt,data){
        $scope.warnConfig.min_date = data.min_date;
        $scope.warnConfig.max_date = data.max_date;
      });

        /*page.html未加载,事件传播无效*/
        $timeout(function(){
          getWarnManager(warnConfig)
        },100);

       /*分页跳转,返回选中页码*/
        $scope.$on('toPage',function (evt,data) {
          $scope.warnConfig.page = data.page;
          getWarnManager(warnConfig)
        });

        //监听时间变化
        $scope.$on("showBtn",function(evt,data){
          $scope.isClick = data.isClick;
          $scope.warnConfig.min_date = data.min_date;
          $scope.warnConfig.max_date = data.max_date
        })

    };

    var init = function () {
      bindListener();
      initPlugins();
    };

    init();
}])
/*.filter('indexOf', [function() {
  var indexOfFilter = function(objArr, attr, text) {
    objArr = objArr || [];
    return objArr.filter(function(item, index) {
      if (item[attr].indexOf(text) > -1) {
        return true;
      }
    });
  };
  return indexOfFilter;
}])
.filter('ltTime', [function() {
  var ltTimeFilter = function(objArr, attr, number) {
    objArr = objArr || [];
    return objArr.filter(function(item, index) {
      if (!number) {
        return true;
      }
      // 结束日期时间指定为当天最后一毫秒
      // 小于指定范围
      if (item[attr] <= (new Date(number).getTime() + 1000 * 60 * 60 * 24 - 1)) {
        return true;
      }
    });
  };
  return ltTimeFilter;
}])
.filter('gtTime', [function () {
  var gtTimeFilter = function (objArr, attr, number) {
    objArr = objArr || [];
    return objArr.filter(function (item, index) {
      if (!number) {
        return true;
      }
      // 大于指定数值
      if (item[attr] >= new Date(number).getTime()) {
        return true;
      }
    });
  };
  return gtTimeFilter;
}]);*/
