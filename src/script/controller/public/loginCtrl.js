angular.module('HTRD_AutoConsole.A').controller('LoginCtrl', ['$scope', '$state', '$window', 'LoginServ', 'DialogServ'
, function($scope, $state, $window, LoginServ, DialogServ) {
     var strapModal;

     // focus 和 blur方法
    //  var userfocus = function () {
    //       // 使用属性 控制 ng-class 的样式
    //       $scope.changeCheckoutConfig.userNameFocus = true;
    //       $scope.changeIcon.userNameIcon = true
    //  };
    //
    //  var userblur = function () {
    //       $scope.changeCheckoutConfig.userNameFocus = false;
    //       $scope.changeIcon.userNameIcon = false
    //  };
    //
    //  var pswfocus = function () {
    //       $scope.changeCheckoutConfig.passWordFocus = true;
    //       $scope.changeIcon.passWordIcon = true
    //  };
    //
    //  var pswblur = function () {
    //       $scope.changeCheckoutConfig.passWordFocus = false;
    //       $scope.changeIcon.passWordIcon = false
    //  };

     //登录按钮的点击事件
     var checkUserLoginFrom = function () {
        if(!!!$scope.userName){
          $scope.pswErr = $scope.pswNull = false;
          $scope.userNull = true;
          return;
        };

        if(!!!$scope.passWord){
          $scope.pswErr = $scope.userNull = false;
          $scope.pswNull = true;
          return;
        };

        var config = {
             userName : $scope.userName,
             passWord : $scope.passWord
        };

        if(!!$scope.reg.test(config.passWord) && !!$scope.userName){
             LoginServ.checkUserLoginFrom(config)
               .then(function(result){
                    if(result.stasus !== "200"){
                         alert("验证失败")
                    }else{
                    	alert("登录成功")
                         $state.go('console.base.home.index');
                    }
                    var token = result.token;
                    $window.localStorage.setItem('token',token);

               })
        }else{
          $scope.userNull = $scope.pswNull = false;
          $scope.pswErr = true;
        }
     };

     var parseDOM = function() {

     };

     var bindListener = function() {
        $scope.checkUserLoginFrom = checkUserLoginFrom;
        // $scope.userfocus = userfocus;
        // $scope.userblur = userblur;
        // $scope.pswfocus = pswfocus;
        // $scope.pswblur = pswblur;

     };

     var initPlugins = function() {
        $scope.pswErr = false;
        $scope.userNull = false;
        $scope.pswpswNull= false;
        var height = document.documentElement.clientHeight ;
        $('body').height(height);

        // 保存登录表单选中状态
        $scope.changeCheckoutConfig = { };

        // 保存icon图标选中状态
        $scope.changeIcon = { };

        // 初始化密码正则
        $scope.reg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![!#$%^&*]+$)[\da-zA-Z_@/./+/-/_]{6,20}$/;
     };

     var init = function() {
          parseDOM();
          bindListener();
          initPlugins();
     };

     init();

}]);
