angular.module("HTRD_AutoConsole.A").factory('LoginServ', ['BaseServ', function(BaseServ) {
  var that = {};

  // 登录系统
  var checkUserLoginFrom = function(params) {
    return BaseServ.query({
      Serv: true,
      method: 'POST',
      params: params,
      //method: 'GET',
      url: 'login',
      
    });
  };

  that.checkUserLoginFrom = checkUserLoginFrom;
  return that;
}])
