/**
 * Created by Administrator on 2017/4/16.
 */
angular.module('HTRD_AutoConsole.A')
	.factory('MessagePanelServ', ['BaseServ', function(BaseServ) {
		var that = {};

		var getManagerInfo = function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				url: '/notice/'
			});
			return promise;
		};

		var getManagerOperation = function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				url: '/notice/{id}/status/'
			});
			return promise;
		};

		var getManagerClean = function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				url: '/notice/clean/'
			});
			return promise;
		};

		that.getManagerInfo = getManagerInfo;
		that.getManagerOperation = getManagerOperation;
		that.getManagerClean = getManagerClean;
		return that;
	}]);
