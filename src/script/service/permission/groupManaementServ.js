angular.module('HTRD_AutoConsole.A')
  .factory('GroupManagementServ', ['BaseServ', function (BaseServ) {
    var that = {};

    var getGroupManagement = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/GroupManagement.json'
      });
      return promise;
    };

    that.getGroupManagement = getGroupManagement;
    return that;
  }]);