angular.module('HTRD_AutoConsole.A')
  .factory('addUserManagementServ', ['BaseServ', function (BaseServ) {
    var that = {};

    var getGroupList = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/UserGroupsAllList.json'
      });
      return promise;
    };

    var getUserInfo = function (params) {
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'GET',
        url: '/users/{id}/'
      });
      return promise;
    };

    // 添加用户保存
    var saveUserInfo = function (params) {
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'POST',
        url: '/users/'
      });
      return promise;
    };

    //编辑用户保存
    var editUserInfo = function (params) {
      console.log(params)
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'PUT',
        url: '/users/{id}/'
        /*url: 'script/json/AddUserManagement.json'*/
      });
      return promise;
    };

    that.getGroupList = getGroupList;
    that.getUserInfo = getUserInfo;
    that.saveUserInfo = saveUserInfo;
    that.editUserInfo = editUserInfo
    return that;
  }]);
