angular.module('HTRD_AutoConsole.A')
  .factory('addGroupManagementServ', ['BaseServ', function (BaseServ) {
    var that = {};

    var getUserList = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/UserGroupsAllList.json'
      });
      return promise;
    };

    var getPermissionList = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/UserPermissionAllList.json'
      });
      return promise;
    };

    var getGroupInfo = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/AddUserManagement.json'
        //url: '/users/{id}/'
      });
      return promise;
    };

    //添加用户保存
    var getSaveGroupInfo = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'POST',
        url: 'script/json/AddUserManagement.json'
      });
      return promise;
    };

//编辑用户保存
    var getEditGroupInfo = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'PUT',
        url: 'script/json/AddUserManagement.json'
      });
      return promise;
    };

    that.getPermissionList = getPermissionList;
    that.getUserList = getUserList;
    that.getGroupInfo = getGroupInfo;
    that.getSaveGroupInfo = getSaveGroupInfo;
    that.getEditGroupInfo = getEditGroupInfo
    return that;
  }]);