angular.module('HTRD_AutoConsole.A')
  .factory('UserManagementServ', ['BaseServ', function (BaseServ) {
    var that = {};

    //得到用户列表
    var getUserManagement = function (params) {
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'GET',
        url: '/users/'
        /*url: 'script/json/UserManagement.json'*/
      });
      return promise;
    };

    //修改密码
    var getChangePassword= function (params) {
      console.log(params);
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'POST',
        url: '/users/{id}/password_change/'
      });
      return promise;
    };

    that.getUserManagement = getUserManagement;
    that.getChangePassword = getChangePassword;
    return that;
  }]);