angular.module('HTRD_AutoConsole.A')
  .factory('OperationLogServ', ['BaseServ', function (BaseServ) {
    var that = {};

    var getLog = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/OperationLog.json'
      });
      return promise;
    };
    var getUser = function(params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/OperationUser.json'
      });
      return promise;
    };

    that.getLog = getLog;
    that.getUser = getUser;

    return that;
  }]);
