angular.module('HTRD_AutoConsole.A')
  .factory('processEditServ', ['BaseServ', function (BaseServ) {
    var that = {};

    var saveProcessInfo = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: ''
      });
      return promise;
    };

    var saveNodeInfo = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: ''
      });
      return promise;
    };
    
    var getMachineList = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/ManagerTreeData.json'
      });
      return promise;
    };

    var getScriptList = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/ManagerTreeData.json'
      });
      return promise;
    };

    var getUserList = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: ' '
      });
      return promise;
    };

    var getProcessList = function(params){
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/ManagerTreeData.json'
      });
      return promise;
    };

    var getNodeInfo = function (params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: ' '
      });
      return promise;
    };

    that.getMachineList = getMachineList;
    that.getScriptList = getScriptList;
    that.getUserList = getUserList;
    that.getProcessList = getProcessList;
    that.getNodeInfo = getNodeInfo;
    that.saveProcessInfo = saveProcessInfo;
    that.saveNodeInfo = saveNodeInfo;
    return that;
  }]);
