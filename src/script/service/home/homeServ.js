/**
 * Created by Administrator on 2017/4/7.
 */
angular.module('HTRD_AutoConsole.A')
	.factory('HomeServ', ['BaseServ', function(BaseServ) {
		var that = {};
 
		//今日待执行
		var getWaitRun = function(params) {
			var promise = BaseServ.query({
				Serv: false,
				param: params,
				method: 'GET',
				url: 'script/json/SystemWait.json'
			});
			return promise;
		};

		//执行中
		var getRuning = function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				url: '/exec/running/'
			});
			return promise;
		};

		//执行告警
		var getWarnRun = function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				/*url: 'script/json/SystemWarn.json'*/
				url: '/alarm/today/'
			});
			return promise;
		};

		//已结束
		var getEndRun= function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				url: '/exec/completed/'
			});
			return promise;
		};

		//执行成功
		var getSuccessRun = function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				url: '/exec/succeed/'
			});
			return promise;
		};

		//手工接管
		var getHandOver = function(params) {
			var promise = BaseServ.query({
				Serv: true,
				param: params,
				method: 'GET',
				url: '/exec/tack_over/'
			});
			return promise;
		};


		that.getWaitRun = getWaitRun;
		that.getRuning = getRuning;
		that.getWarnRun = getWarnRun;
		that.getEndRun = getEndRun;
		that.getSuccessRun = getSuccessRun;
		that.getHandOver = getHandOver;
		return that;
	}]);
