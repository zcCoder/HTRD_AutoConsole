angular.module('HTRD_AutoConsole.A')
  .factory('UserInfoServ', ['BaseServ', function(BaseServ) {
    var that = {};

    var getUserInfo = function(params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/userInfo.json'
      });
      return promise;
    };

    var getNewUserInfo = function(params) {
      var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'PUT',
        url: 'script/json/userInfo.json'
      });
      return promise;
    };

    that.getUserInfo = getUserInfo;
    that.getNewUserInfo = getNewUserInfo;
    return that;
  }]);
