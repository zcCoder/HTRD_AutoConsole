angular.module('HTRD_AutoConsole.A').factory('ManagerReviewServ', ['BaseServ', function(BaseServ) {
  var that = {};

  var breadcrumbsData = [
    {
      'iconClass': 'icon-home',
      'text': '流程管理',
      'link': 'console.base.manager.list'
    },
    {
      'text': '流程审核'
    }
  ];
  var headList = [
    {
      'title': '序号',
      'type': 'ignore'
    },
    {
      'title': '流程名称',
      'type': 'name'
    },
    {
      'title': '分类',
      'type': 'ignore'
    },
    {
      'title': '节点数',
      'type': 'node_count'
    },
    {
      'title': '创建人',
      'type': 'creator_name'
    },
    {
      'title': '修改人',
      'type': 'modifier_name'
    },
    {
      'title': '编辑时间',
      'type': 'editTime'
    },
    {
      'title': '审核人',
      'type': 'reviewer_name'
    },
    {
      'title': '审批时间',
      'type': 'reviewerTime'
    },
    {
      'title': '审批状态',
      'type': 'ignore'
    },
    {
      'title': '操作',
      'type': 'ignore'
    }
  ];
  var stateParse = {
    pending: '待审批',
    passed: '已通过',
    rejected: '已拒绝'
  };
  var getReviewList = function(params) {
    var promise = BaseServ.query({
      Serv: false,
      param: params,
      method: 'GET',
      url: 'script/json/ManagerReviewList.json'
    });
    return promise;
  };

  that.breadcrumbsData = breadcrumbsData;
  that.headList = headList;
  that.stateParse = stateParse;
  that.getReviewList = getReviewList;

  return that;
}]);