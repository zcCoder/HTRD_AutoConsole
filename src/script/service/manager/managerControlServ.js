angular.module('HTRD_AutoConsole.A')
.factory('ManagerControlServ', ['BaseServ', function(BaseServ) {
  var that = {};

  var getManagerControlList = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      url: '/cron/'
    });
    return promise;
  };

  var getManagerControlInfo = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      url: '/cron/{id}/'
    });
    return promise;
  };

  var addManagerControlInfo = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'POST',
      url: '/cron/'
    });
    return promise;
  };

  var editManagerControlInfo = function(params){
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'PUT',
      url: '/cron/{id}/'
    });
    return promise;
  };

/*  var putEditManagerControlInfo = function(params,id){
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'PUT',
      url: '/cron/'+id+'/'
    });
    return promise;
  };*/



  var delManagerControlInfo = function() {
    var promise = BaseServ.query({
      Serv: false,
      param: params,
      method: 'GET',
      url: 'script/json/ManagerProcessList.json'
    });
    return promise;
  };


  that.getManagerControlList = getManagerControlList;
  that.getManagerControlInfo = getManagerControlInfo;
  that.addManagerControlInfo = addManagerControlInfo;
  that.editManagerControlInfo = editManagerControlInfo;
  //that.putEditManagerControlInfo = putEditManagerControlInfo;
  that.delManagerControlInfo = delManagerControlInfo;
  return that;
}]);
