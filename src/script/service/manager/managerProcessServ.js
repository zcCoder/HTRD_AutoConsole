angular.module('HTRD_AutoConsole.A')
.factory('ManagerProcessServ', ['BaseServ', function(BaseServ) {
  var that = {};

  var getNodeInfo = function(params) {
    var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'GET',
        // url: 'script/json/ManagerNodeInfo.json'
        url: '/node/{id}/'
    });
    return promise;
  };


  var getNodeChartByCPU = function(params) {
    var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/ManagerNodeChartByCPU.json'
    });
    return promise;
  };

  var getNodeChartByMemory = function(params) {
    var promise = BaseServ.query({
        Serv: false,
        param: params,
        method: 'GET',
        url: 'script/json/ManagerNodeChartByMemory.json'
    });
    return promise;
  };

  that.getNodeInfo = getNodeInfo;
  that.getNodeChartByCPU = getNodeChartByCPU;
  that.getNodeChartByMemory = getNodeChartByMemory;
  return that;
}]);
