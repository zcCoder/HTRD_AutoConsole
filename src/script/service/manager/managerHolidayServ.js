angular.module('HTRD_AutoConsole.A').factory('ManagerHolidayServ', ['BaseServ', function (BaseServ) {
  var that = {};

  var getHolidayList = function (params) {
      var promise = BaseServ.query({
          Serv: true,
          param: params,
          method: 'GET',
          url: '/holiday/'
      });
      return promise;
  };

  var getHolidayInfo = function (params) {
      var promise = BaseServ.query({
          Serv: true,
          param: params,
          method: 'GET',
          url: '/holiday/{id}/'
      });
      return promise;
  };

  var addHolidayInfo = function (params) {
      var promise = BaseServ.query({
          Serv: true,
          param: params,
          method: 'POST',
          url: '/holiday/'
      });
      return promise;
  };

  var editHolidayInfo = function (params) {
      var promise = BaseServ.query({
          Serv: true,
          param: params,
          method: 'PUT',
          url: '/holiday/{id}/'
      });
      return promise;
  };

  var delHolidayInfo = function (params) {
      var promise = BaseServ.query({
          Serv: true,
          param: params,
          method: 'DELETE',
          url: '/holiday/{id}/'
      });
      return promise;
  };

  that.getHolidayList = getHolidayList;
  that.getHolidayInfo = getHolidayInfo;
  that.addHolidayInfo = addHolidayInfo;
  that.editHolidayInfo = editHolidayInfo;
  that.delHolidayInfo = delHolidayInfo;
  return that;
}]);
