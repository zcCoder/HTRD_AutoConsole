angular.module('HTRD_AutoConsole.A')
  .factory('ManagerListServ', ['BaseServ', function(BaseServ) {
    var that = {};

    var getProcessList = function(params) {
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'GET',
        url: '/pipeline/'
      });
      return promise;
    };
    var breadcrumbsData = [
      {
        'iconClass': 'htrd-icon-process-sm',
        'text': '流程管理',
        'link': 'console.base.manager.list'
      },
      {
        'text': '流程列表'
      }
    ];
    /*var headList = [
      {
        'title': '序号',
        'type': 'ignore'
      },
      {
        'title': '流程状态',
        'type': 'ignore'
      },
      {
        'title': '流程名称',
        'type': 'ignore'
      },
      {
        'title': '分类',
        'type': 'ignore'
      },
      {
        'title': '节点数',
        'type': 'ignore'
      },
      {
        'title': '创建人',
        'type': 'ignore'
      },
      {
        'title': '修改人',
        'type': 'ignore'
      },
      {
        'title': '创建时间',
        'type': 'created_at'
      },
      // {
      //   'title': '审核人',
      //   'type': 'reviewer'
      // },
      // {
      //   'title': '审批时间',
      //   'type': 'reviewed_at'
      // },
      // {
      //   'title': '审核状态',
      //   'type': 'ignore'
      // },
      {
        'title': '操作',
        'type': 'ignore'
      }
    ];*/

    var stateOptions = [
      {
        'title': '全部状态'
      },

      {
        'id': 0,
        'title': '未执行'
      },
      {
        'id': 1,
        'title': '正在检测'
      },
      {
        'id': 2,
        'title': '执行中'
      }
    ];
    var stateParse = function(state) {
      var stateObj = {
        0: '待审核',
        1: '已通过',
        2: '已拒绝'
      };
      return stateObj[state];
    };

    that.breadcrumbsData = breadcrumbsData;
    //that.headList = headList;
    that.stateOptions = stateOptions;
    that.stateParse = stateParse;
    that.getProcessList = getProcessList;
    return that;
  }]);
