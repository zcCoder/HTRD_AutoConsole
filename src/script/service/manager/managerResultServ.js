angular.module('HTRD_AutoConsole.A')
  .factory('ManagerResultServ', ['BaseServ', function (BaseServ) {
    var that = {};

    var headList = [
      {
        'title': '序号',
        'type': 'ignore'
      },
      {
        'title': '开始时间',
        'type': 'started_at'
      },
      {
        'title': '结束时间',
        'type': 'ended_at'
      },
      {
        'title': '流程名称',
        'type': 'ignore'
      },
      {
        'title': '分类',
        'type': 'ignore'
      },
      {
        'title': '节点数',
        'type': 'ignore'
      },
      {
        'title': '执行结果',
        'type': 'ignore'
      },
      {
        'title': '备注',
        'type': 'ignore'
      },
      {
        'title': '执行人',
        'type': 'ignore'
      }
    ];
    var resultOptions = [
      {
        'title': '全部结果'
      },
      {
        'id': '9',
        'title': '执行成功'
      },
      {
        'id': '19',
        'title': '被不认可终止'
      },
      {
        'id': '21',
        'title': '手工接管后的执行结果：成功'
      },
      {
        'id': '22',
        'title': '手工接管后的执行结果：失败'
      },
      {
        'id': '20',
        'title': '执行完成但有错误'
      },

      {
        'id': '2',
        'title': '检测异常'
      }
    ];

    var getResult = function (params) {
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'GET',
        url: '/exec/'
      });
      return promise;
    };

    that.headList = headList;
    that.resultOptions = resultOptions;
    that.getResult = getResult;
    return that;
  }]);
