angular.module("HTRD_AutoConsole.A").factory('ManagerProcessQuneeServ', ['BaseServ', function (BaseServ) {
  var that = {};

  // 模拟取node json
  var getNodeJsonTest = function(params) {
    var promise = BaseServ.query({
      Serv: false,
      param: params,
      method: 'GET',
      url: 'script/json/NodeJsonTest.json'
    });
    return promise;
  }

  // 获取当前流程属性
  var getManagerProcessQuneeInfo = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeInfo.json'
      url: '/pipeline/{id}/info/'
    });
    return promise;
  };

  // 获取当前流程节点属性
  var getManagerProcessQuneeNodeInfo = function(params) {
    var promise = BaseServ.query({
      Serv: false,
      param: params,
      method: 'GET',
      url: 'script/json/ManagerProcessQuneeNodeInfo.json'
    });
    return promise;
  };

  // 获取流程日志
  var getManagerProcessQuneeLogList = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeLogList.json'
      url: '/exec/{id}/log/'
    });
    return promise;
  };

  // 获取用户列表
  var getUserList = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/UserList.json'
      url: '/users/'
    });
    return promise;
  };

  // 流程 - 用户提交确认节点
  var submitManagerProcessQuneeUserConfirm = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      url: '/exec/{id}/affirm/'
      // url: 'script/json/ManagerProcessQuneeUserConfirm.json'
    });
    return promise;
  };


  // 流程 - 获取用户所有状态字典
  var getManagerProcessQuneeStateMap = function(params) {
    var promise = BaseServ.query({
      Serv: false,
      param: params,
      method: 'GET',
      url: 'script/json/ManagerProcessQuneeStateMap.json'
    });
    return promise;
  };

  // 获取当前流程状态及节点数据
  var getManagerProcessQuneeStateAndData = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeState.json'
      url: '/pipeline/{id}/'
    });
    return promise;
  };

  // 获取执行中具体状态及节点
  var getManagerProcessQuneeExecState = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeExecState.json'
      // url: '/pipeline/{id}/status/'
      url: '/exec/{id}/'
    });
    return promise;
  };

  // 流程图事件

  // 检测当前流程
  var ManagerProcessQuneeCheck = function (params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeCheck.json'
      url: '/pipeline/{id}/check/'
    });
    return promise;
  };

  // 开始执行当前流程
  var ManagerProcessQuneeStart = function (params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeStart.json'
      url: '/pipeline/{id}/run/'
    });
    return promise;
  };

  // 暂停执行当前流程
  var ManagerProcessQuneePause = function (params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneePause.json'
      url: '/exec/{id}/pause/'
    });
    return promise;
  };

  // 继续执行当前流程
  var ManagerProcessQuneeContinue = function (params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeContinue.json'
      url: '/exec/{id}/goon/'
    });
    return promise;
  };

  // 终止执行当前流程
  var ManagerProcessQuneeQuit = function (params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeQuit.json'
      url: '/exec/{id}/stop/'
    });
    return promise;
  };

  // 重做该节点
  var ManagerProcessQuneeResetNode = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeResetNode.json'
      url: '/exec/{id}/redo/'
    });
    return promise;
  };

  // 手工接管
  var ManagerProcessQuneeManualNozzleNode = function (params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeManualNozzleNode.json'
      url: '/exec/{id}/take/'
    });
    return promise;
  };

  // 获取历史执行流程
  var getManagerProcessQuneeHistoryList = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      // url: 'script/json/ManagerProcessQuneeHistoryList.json'
      url: '/pipeline/{id}/history/'
    });
    return promise;
  };

  that.getNodeJsonTest = getNodeJsonTest;
  that.getManagerProcessQuneeNodeInfo = getManagerProcessQuneeNodeInfo;
  that.ManagerProcessQuneeResetNode = ManagerProcessQuneeResetNode;
  that.getManagerProcessQuneeInfo = getManagerProcessQuneeInfo;
  that.getManagerProcessQuneeLogList = getManagerProcessQuneeLogList;
  that.getUserList = getUserList;
  that.submitManagerProcessQuneeUserConfirm = submitManagerProcessQuneeUserConfirm;
  that.getManagerProcessQuneeStateMap = getManagerProcessQuneeStateMap;
  that.getManagerProcessQuneeExecState = getManagerProcessQuneeExecState;
  that.getManagerProcessQuneeStateAndData = getManagerProcessQuneeStateAndData;
  that.ManagerProcessQuneeCheck = ManagerProcessQuneeCheck;
  that.ManagerProcessQuneeStart = ManagerProcessQuneeStart;
  that.ManagerProcessQuneePause = ManagerProcessQuneePause;
  that.ManagerProcessQuneeContinue = ManagerProcessQuneeContinue;
  that.ManagerProcessQuneeQuit = ManagerProcessQuneeQuit;
  // that.ManagerProcessQuneeOnlyExecuteSelfNode = ManagerProcessQuneeOnlyExecuteSelfNode;
  // that.ManagerProcessQuneeSkipSelfNode = ManagerProcessQuneeSkipSelfNode;
  that.ManagerProcessQuneeManualNozzleNode = ManagerProcessQuneeManualNozzleNode;
  that.getManagerProcessQuneeHistoryList = getManagerProcessQuneeHistoryList;
  return that;
}])
