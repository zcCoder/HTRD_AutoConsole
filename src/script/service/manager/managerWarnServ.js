angular.module('HTRD_AutoConsole.A')
  .factory('ManagerWarnServ', ['BaseServ', function (BaseServ) {
    var that = {};

    var getResult = function (params) {
      var promise = BaseServ.query({
        Serv: true,
        param: params,
        method: 'GET',
        /*url: 'script/json/ManagerProcessWarn.json'*/
        url: '/alarm/'
      });
      return promise;
    };

    that.getResult = getResult;
    return that;
  }]);
