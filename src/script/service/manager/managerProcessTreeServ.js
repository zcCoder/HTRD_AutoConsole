angular.module("HTRD_AutoConsole.A").factory('ManagerProcessTreeServ', ['BaseServ', function(BaseServ) {
  var that = {};
  // 临时记录id,防止重复(正式环境去掉)
  // var id = 1;

  // 获取整个流程树
  var getProcessTree = function(params) {
    // var promise = BaseServ.query({
    //   Serv: false,
    //   param: params,
    //   method: 'GET',
    //   url: 'script/json/ManagerTreeData.json'
    // });
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      url: '/groups/'
    });
    return promise;
  };
  // 获取某个流程节点
  var getProcessTreeById = function(params) {
    var promise = BaseServ.query({
      Serv: true,
      param: params,
      method: 'GET',
      url: '/groups/{id}/'
    });
    return promise;
  };


  // 后端数据返回的name属性递归改为text属性
  // 设置分类disabled，流程icon
  var setTreeData = function(data) {
    var copyData = JSON.parse(JSON.stringify(data));

    // 假id,防止重复(正式环境去掉)
    // copyData.id = id ++;

    copyData.text = copyData.name;
    delete copyData.name;

    // 分类
    if (copyData.children && copyData.children.length) {

      // 设置禁用
      // if (copyData.state) {
      //   copyData.state.disabled = true;
      // } else {
      //   copyData.state = {
      //     'disabled': true
      //   }
      // }

      if (typeof copyData.children[0] === 'number') {
        copyData.li_attr = {
          'notload': 'true'
        };
        copyData.children = [true];
        return copyData;
      }

      // 递归设置
      copyData.children = copyData.children.map(function(item) {
        return setTreeData(item);
      });
    } else {
      // 流程
      // copyData.icon = 'https://www.jstree.com/tree-icon.png';
    }

    return copyData;
  };

  // 获取流程状态列表
  // var getProcessState = function(params) {
  //   var promise = BaseServ.query({
  //     Serv: false,
  //     param: params,
  //     method: 'GET',
  //     url: 'script/json/ManagerProcessState.json'
  //   });
  //   return promise;
  // };

  that.getProcessTree = getProcessTree;
  that.getProcessTreeById = getProcessTreeById;
  // that.getProcessState = getProcessState;
  that.setTreeData = setTreeData;

  return that;
}])
