// 操作本地缓存字典数据
angular.module("HTRD_AutoConsole.A").factory('ParseMapServ', ['$window', function($window) {
  var that = {};

  var setItem = function(key, value, mapName) {
    var map = JSON.parse($window.localStorage.getItem(mapName)) || {};
    map[key] = value;
    $window.localStorage.setItem(mapName, JSON.stringify(map));
  };

  var getItem = function(key, mapName) {
    var map = JSON.parse($window.localStorage.getItem(mapName)) || {};
    return map[key];
  };

  var removeItem = function(key, mapName) {
    var map = JSON.parse($window.localStorage.getItem(mapName)) || {};
    delete map[key];
    $window.localStorage.setItem(mapName, JSON.stringify(map));
  };

  that.setItem = setItem;
  that.getItem = getItem;
  that.removeItem = removeItem;
  return that;
}])
