// dialog服务
angular.module("HTRD_AutoConsole.A").factory('DialogServ', ['$modal', '$alert', '$aside',
    function($modal, $alert, $aside) {
        var tmpObj = {},
            tmpDialogObj,
            // 脚手架
            bootstrap = function(constructor, options) {
                tmpDialogObj = constructor(options);

                tmpObj.list.push(tmpDialogObj);

                return tmpDialogObj;
            },
            // 处理列表
            dealList = function(funcName) {
                for (var i = 0,
                        listLen = tmpObj.list.length,
                        item; i < listLen; i++) {
                    item = tmpObj.list[i];

                    if (item.hasOwnProperty(funcName) && angular.isFunction(item[funcName])) {
                        item[funcName]();

                        // 移除list
                        if (funcName == 'destroy') {
                            // 删除当前项
                            tmpObj.list.splice(i, 1);
                            listLen = tmpObj.list.length;
                            i--;
                        }
                    }
                }
            };

        // list
        tmpObj.list = [];

        tmpObj.modal = function(options) {

          // 设置默认值
          options.animation = options.animation || 'am-fade-and-scale';
          options.backdrop = options.backdrop || 'static';
          return bootstrap($modal, options);
        };

        tmpObj.alert = function(options) {
          return bootstrap($alert, options);
        };

        tmpObj.aside = function(options) {
          return bootstrap($aside, options);
        };

        // hide
        tmpObj.hide = function(callback) {
          dealList('hide');
          callback && callback();
        };
        // destroy
        tmpObj.destroy = function() {
          dealList('destroy');
        };

        return tmpObj;
    }
])
