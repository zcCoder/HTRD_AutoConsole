//浏览器消息通知
angular.module("HTRD_AutoConsole.A").factory('NotificationServ', ['$window', function($window) {

	var that = {};
	var init;

	var selfData;
	var selfCallback;

	// 初始化
	var init = function(data, callback) {
		selfData = data;
		selfCallback = callback;
		console.log('浏览器弹窗')
		var notification = new Notification(data.title + ":", {
			dir: "auto",
			lang: "hi",
			tag: " ",
			icon:'img/logo.png',
			body: data.result_info  + data.created_at
		});

		notification.onclick = function (evt) {
			selfCallback(selfData);
		};
	};


	that.init = init;
	return that;
}]);
