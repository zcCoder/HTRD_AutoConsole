// 格式化时间
angular.module("HTRD_AutoConsole.A").factory('DateParseServ', [function() {
  var result;
  var that = {};

  // '2017-04-28T03:20:06.003405Z' -> '2017-04-28 03:20:06'
  var parseStr = function(str) {

    if (str.indexOf('.') !== -1) {
      str = str.split('.')[0];
    };
    str = str.replace('T', ' ').replace('Z', ' ');
    return str;
  };

  var dateParse = function(date) {

    if (typeof date === 'string') {
      result = parseStr(date);
    }

    else if (Array.isArray(date)) {
      result = [];
      date.map(function(item, index) {
        result.push(parseStr(item));
      });
    };

    return result;
  };

  that.dateParse = dateParse;
  return that;
}])
