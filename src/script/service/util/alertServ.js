// alert
angular.module("HTRD_AutoConsole.A").factory('AlertServ', ['DialogServ', function(DialogServ) {
      var that = {};

      var showAlert = function(title, type) {
        DialogServ.alert({
          'animation': 'am-fade-and-slide-top',
          'placement': 'top',
          'duration': 2,
          'title': title,
          'type': type
        });
      };

      var showSuccess = function(content) {
        showAlert(content || '执行成功', 'success');
      };

      var showWarning = function(content) {
        showAlert(content || '执行警告', 'warning');
      };

      var showError = function(content) {
        showAlert(content || '执行错误', 'danger');
      };

      that.showSuccess = showSuccess;
      that.showWarning = showWarning;
      that.showError = showError;
      return that;
    }
])
