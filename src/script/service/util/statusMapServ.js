// 状态对应文本
angular.module('HTRD_AutoConsole.A').factory('StatusMapServ', [function() {
  var result;

  var that = {};

   var statusMap = {
      2: {
        text: '检测异常'
      },
      19:{
        text: '被不认可终止'
      },
      20:{
        text: '执行完成但有错误'
      },
      21:{
        text: '手工接管后的执行结果：成功'
      },
      22:{
        text: '手工接管后的执行结果：失败'
      },
      6:{
        text: '执行中'
      },
      7:{
        text: '执行失败'
      },
      8:{
        text: '执行超时'
      },
      9:{
        text: '执行成功'
      },
      11:{
        text: '手工终止'
      },
      13:{
        text: '暂停'
      }
  };

  var getStatusData = function(statusIndex) {
    result =  statusMap[statusIndex];
    return result;
  };

  that.getStatusData = getStatusData;
  return that;
}])
