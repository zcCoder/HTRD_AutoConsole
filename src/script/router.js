angular.module('HTRD').config(['$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider',
    function($locationProvider, $stateProvider, $urlRouterProvider, $httpProvider) {
        var BaseRouterUrl = '/';
        // delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        // $httpProvider.defaults.useXDomain = true;
        // $httpProvider.defaults.withCredentials = true;


        $urlRouterProvider.when('/', BaseRouterUrl + 'login')
          .otherwise(BaseRouterUrl + 'login');


        $stateProvider

            // 登录
            .state('login', {
                url: BaseRouterUrl + 'login',
                views: {
                    'consoleBox@': {
                        controller: 'LoginCtrl',
                        templateUrl: 'view/public/login.html'
                    }
                }
            })

            // 后台
            .state('console', {
                abstract: true,
                url: BaseRouterUrl,
                views: {
                    'consoleBox@': {
                        templateUrl: 'view/console/base/index.html'
                    }
                }
            })

            // 后台页面基本结构初始化
            .state('console.base', {
                views: {
                    'header': {
                        controller: 'HeaderCtrl',
                        templateUrl: 'view/console/base/header.html'
                    },
                    'body': {
                        controller: 'BodyCtrl',
                        templateUrl: 'view/console/base/body.html'
                    },
                    'left': {
                        controller: 'LeftCtrl',
                        templateUrl: 'view/console/base/left.html'
                    }
                }
            })

            .state('console.base.home', {
                'url': 'home',
                'views': {
                    'category': {
                        /*'controller': 'HomeCtrl',*/
                        'templateUrl': 'view/console/home/index.html'
                    }
                }
            })

            // 首页
            .state('console.base.home.index', {
                'url': '/index',
                'views': {
                    'content': {
                        'controller': 'HomeCtrl',
                        'templateUrl': 'view/console/home/home.html'
                    }
                }
            })

            /*.state('console.base.home.homelist', {
                'url': '/homelist/:type',
                'views': {
                    'content': {
                        'controller': 'HomeListCtrl',
                        'templateUrl': 'view/console/home/homeList.html'
                    }
                }
            })*/
              .state('console.base.home.waitlist', {
                  'url': '/waitlist',
                  'views': {
                      'content': {
                          'controller': 'HomeWaitListCtrl',
                          'templateUrl': 'view/console/home/homeWaitList.html'
                      }
                  }
              })

              .state('console.base.home.runlist', {
                  'url': '/runlist',
                  'views': {
                      'content': {
                          'controller': 'HomeRunListCtrl',
                          'templateUrl': 'view/console/home/homeRunList.html'
                      }
                  }
              })

              .state('console.base.home.warnlist', {
                  'url': '/warnlist',
                  'views': {
                      'content': {
                          'controller': 'HomeWarnListCtrl',
                          'templateUrl': 'view/console/home/homeWarnList.html'
                      }
                  }
              })

              .state('console.base.home.endlist', {
                  'url': '/endlist',
                  'views': {
                      'content': {
                          'controller': 'HomeEndCtrl',
                          'templateUrl': 'view/console/home/homeEndList.html'
                      }
                  }
              })

              .state('console.base.home.successlist', {
                  'url': '/successlist',
                  'views': {
                      'content': {
                          'controller': 'HomeSuccessListCtrl',
                          'templateUrl': 'view/console/home/homeSuccessList.html'
                      }
                  }
              })

              .state('console.base.home.handlist', {
                  'url': '/handlist',
                  'views': {
                      'content': {
                          'controller': 'HomeHandListCtrl',
                          'templateUrl': 'view/console/home/homeHandList.html'
                      }
                  }
              })


            // 运维管理 - 首页
            .state('console.base.manager', {
                'url': 'manager',
                'views': {
                    'category': {
                        'controller': 'ManagerCtrl',
                        'templateUrl': 'view/console/manager/index.html'
                    }
                }
            })

            //流程列表
            .state('console.base.manager.list', {
                'url': '/list',
                'views': {
                    'content': {
                        'controller': 'ManagerListCtrl',
                        'templateUrl': 'view/console/manager/managerList.html'
                    }
                }
            })

            // 运维管理 - 首页 - 流程列表编辑
            .state("console.base.manager.edit", {
                'url': '/edit/:id',
                'views': {
                    'content': {
                        'controller': 'ManagerProcessEditCtrl',
                        'templateUrl': 'view/console/manager/managerProcessEdit.html'
                    }
                }
            })

            // 运维管理 - 首页 - 流程列表详细
            .state('console.base.manager.process', {
                'url': '/process/:id',
                'views': {
                    'content': {
                        'controller': 'ManagerProcessCtrl',
                        'templateUrl': 'view/console/manager/managerProcess.html'
                    }
                }
            })

            // 运维管理 - 首页 - 历史流程列表详细
            .state('console.base.manager.historyProcess', {
                'url': '/historyProcess/:id/:processId',
                'views': {
                    'content': {
                        'controller': 'ManagerHistoryProcessCtrl',
                        'templateUrl': 'view/console/manager/managerHistoryProcess.html'
                    }
                }
            })

            // 流程结果查询
            .state('console.base.manager.result', {
                'url': '/result',
                'views': {
                    'content': {
                        'controller': 'ManagerResultCtrl',
                        'templateUrl': 'view/console/manager/managerResult.html'
                    }
                },
                breadcrumb: {
                    label: '流程结果查询',
                    parent: 'console.base.manager.list'
                }
            })

            // 流程告警查询
            .state('console.base.manager.warn', {
                'url': '/warn',
                'views': {
                    'content': {
                        'controller': 'ManagerWarnCtrl',
                        'templateUrl': 'view/console/manager/managerWarn.html'
                    }
                }
            })

            // 流程审核列表
            .state('console.base.manager.review', {
                'url': '/review',
                'views': {
                    'content': {
                        'controller': 'ManagerReviewCtrl',
                        'templateUrl': 'view/console/manager/managerReview.html'
                    }
                }
            })

            // 流程调度列表
            .state('console.base.manager.controlList', {
                'url': '/controlList',
                'views': {
                    'content': {
                        'controller': 'ManagerControlListCtrl',
                        'templateUrl': 'view/console/manager/controlList.html'
                    }
                }
            })

            // 节假日列表
            .state('console.base.manager.holidayList', {
                'url': '/holidayList',
                'views': {
                    'content': {
                        'controller': 'ManagerHolidayListCtrl',
                        'templateUrl': 'view/console/manager/holidayList.html'
                    }
                }
            })

            // 假日方案详细
            .state('console.base.manager.holidayInfo', {
                'url': '/holidayInfo/:id',
                'views': {
                    'content': {
                        'controller': 'ManagerHolidayInfoCtrl',
                        'templateUrl': 'view/console/manager/holidayInfo.html'
                    }
                }
            })

            //用户操作日志-首页
            .state('console.base.log', {
                'url': 'log',
                'views': {
                    'category': {
                        'controller': 'OperationLogCtrl',
                        'templateUrl': 'view/console/log/operationLog.html'
                    }
                }
            })

            //  // 用户权限 - 首页
            // .state('console.base.permission', {
            //     'url': 'permission',
            //     'views': {
            //         'category': {
            //             'controller': 'PermissionCtrl',
            //             'templateUrl': 'view/console/permission/index.html'
            //         }
            //     }
            // })
            //
            // //用户权限 - 首页 - 用户管理
            // .state('console.base.permission.user', {
            //     'url': '/user',
            //     'views': {
            //         'content': {
            //             'controller': 'UserManageCtrl',
            //             'templateUrl': 'view/console/permission/usermanage.html'
            //         }
            //     }
            // })
            //
            // // 用户权限 - 首页 - 编辑 / 添加用户
            // .state('console.base.permission.addUser', {
            //     'url': '/addUser/:id',
            //     'views': {
            //         'content': {
            //             'controller': 'AddUserManageCtrl',
            //             'templateUrl': 'view/console/permission/addusermanage.html'
            //         }
            //     }
            // })
            //
            // //用户权限 - 首页 - 组管理
            // .state('console.base.permission.group', {
            //     'url': '/group',
            //     'views': {
            //         'content': {
            //             'controller': 'GroupManageCtrl',
            //             'templateUrl': 'view/console/permission/groupmanage.html'
            //         }
            //     }
            // })
            //
            // // 用户权限 - 首页 - 编辑 / 添加组
            // .state('console.base.permission.addGroup', {
            //     'url': '/addGroup/:id',
            //     'views': {
            //         'content': {
            //             'controller': 'AddGroupManageCtrl',
            //             'templateUrl': 'view/console/permission/addgroupmanage.html'
            //         }
            //     }
            // })
            //
            //  // 个人中心
            // .state('console.base.user', {
            //     'url': 'user',
            //     'views': {
            //         'category': {
            //             'controller': 'UserCtrl',
            //             'templateUrl': 'view/console/user/index.html'
            //         }
            //     }
            // })
            //
            // .state('console.base.user.info', {
            //     'url': '/info',
            //     'views': {
            //         'content': {
            //             'controller': 'UserInfoCtrl',
            //             'templateUrl': 'view/console/user/userInfo.html'
            //         }
            //     }
            // })

            //流程模板
            .state("console.base.template", {
                'url': 'template',
                'views': {
                    'category': {
                        'controller': 'ProcessTemplateCtrl',
                        'templateUrl': 'view/console/template/processTemplate.html'
                    }
                }
            })

            // plugin
            .state('console.base.plugin', {
              'url': 'plugin',
              'views': {
                  'category': {
                      'controller': 'PluginCtrl',
                      'templateUrl': 'view/console/plugin/index.html'
                  }
              }
            })

            // directive
            .state('console.base.plugin.directive', {
              'url': '/directive',
              'views': {
                  'content': {
                      'controller': 'DirectiveCtrl',
                      'templateUrl': 'view/console/plugin/directive.html'
                  }
              }
            })

            // fetcory
            .state('console.base.plugin.fetcory', {
              'url': '/fetcory',
              'views': {
                  'content': {
                      'controller': 'FetcoryCtrl',
                      'templateUrl': 'view/console/plugin/fetcory.html'
                  }
              }
            })
        // http://localhost:8888/src/login
        // 需要加上 html.htm标签内的base路径 !!!!!!!!!
        $locationProvider.html5Mode(true);
}]);
