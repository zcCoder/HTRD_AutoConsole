var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),  //CSS压缩
    concat = require('gulp-concat'),        // 文件合并
    uglify = require('gulp-uglify'),        //js压缩插件
    rename = require('gulp-rename'),        // 重命名
    notify = require('gulp-notify'),        // 重命名
    less = require('gulp-less'),        // 重命名
    plumber = require('gulp-plumber'),      // 解决报错停止
    stripDebug = require('gulp-strip-debug')

gulp.task('default', function() {
  // 将你的默认的任务代码放在这
});

gulp.task('watch', function() {

  // gulp.watch(['src/script/*.js','src/script/*/*.js','src/script/*/*/*.js','src/script/*/*/*/*.js'], ['js-jquery','js-bootstrap','js-angular','js-angular-ui-router','js-vendor','js-controller','js-service','js-directive','js-base']);

  gulp.watch(['src/script/*.js','src/script/*/*.js','src/script/*/*/*.js','src/script/*/*/*/*.js','src/script/*/*/*/*/*.js'],
  ['js-vendor','js-graph','js-controller','js-service','js-directive','js-base']);
  gulp.watch(['src/styles/*/*.less','src/styles/*.less'], ['css-less', 'css-ui-less']);
});

gulp.task('js-base', function() {
  return gulp.src('src/script/*.js')
      .pipe(concat('base.js'))
      // .pipe(stripDebug())
      .pipe(gulp.dest('dest/js'))
});

gulp.task('js-vendor', function() {
  return gulp.src('src/script/vendor/*.js')
      .pipe(gulp.dest('dest/js'))
});

gulp.task('js-controller', function() {
  return gulp.src(['src/script/controller/*/*.js','src/script/controller/*/*/*.js'])
      .pipe(concat('controller.js'))
      // .pipe(stripDebug())
      // .pipe(gulp_remove_logging('controller.js'))
      .pipe(gulp.dest('dest/js'))
});

gulp.task('js-service', function() {
  return gulp.src('src/script/service/*/*.js')
      .pipe(concat('service.js'))
      // .pipe(stripDebug())
      // .pipe(gulp_remove_logging('service.js'))
      .pipe(gulp.dest('dest/js'))
});

gulp.task('js-directive', function() {
  return gulp.src(['src/script/directive/*.js', 'src/script/directive/*/*.js'])
      .pipe(concat('directive.js'))
      // .pipe(stripDebug())
      // .pipe(gulp_remove_logging('directive.js'))
      .pipe(gulp.dest('dest/js'))
});

gulp.task('js-graph', function() {
  return gulp.src(['src/script/vendor/graph/*.js','src/script/vendor/graph/*/*.js','src/script/vendor/graph/*/*/*.js'])
      // .pipe(concat('graph.editor.js'))
      // .pipe(stripDebug())
      .pipe(gulp.dest('dest/js/graph'))
});

gulp.task('css-less', function() {
  return gulp.src('src/styles/*/*.less')
      .pipe(plumber())
      .pipe(less())
      .pipe(gulp.dest('src/styles/'))
      .pipe(notify({ message: 'less2css task ok' }));
});

gulp.task('css-ui-less', function() {
  return gulp.src('src/styles/*.less')
      .pipe(plumber())
      .pipe(less())
      .pipe(gulp.dest('src/styles/'))
      .pipe(notify({ message: 'less2css task ok' }));
});
